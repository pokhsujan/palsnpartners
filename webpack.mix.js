const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.sass('resources/assets/sass/admin.scss', 'public/css')
    .autoload({
        jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"]
    })
    .js(['resources/assets/js/admin.js','resources/assets/js/media-manager.js','resources/assets/js/app.custom.js' ], 'public/js')
    .copy('./node_modules/ckeditor/config.js', 'public/js/ckeditor/config.js')
    .copy('./node_modules/ckeditor/styles.js', 'public/js/ckeditor/styles.js')
    .copy('./node_modules/ckeditor/contents.css', 'public/js/ckeditor/contents.css')
    .copyDirectory('./node_modules/ckeditor/skins', 'public/js/ckeditor/skins')
    .copyDirectory('./node_modules/ckeditor/lang', 'public/js/ckeditor/lang')
    .copyDirectory('./node_modules/ckeditor/plugins', 'public/js/ckeditor/plugins');
mix.styles(['resources/assets/css/admin-style.css'], 'public/css/admin-style.css');



mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.js(['resources/assets/js/app.js', 'resources/assets/js/front.custom.js'], 'public/js');