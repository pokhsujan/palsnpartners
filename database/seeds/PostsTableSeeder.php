<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>

<p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>

<p>https://youtu.be/xLCn88bfW1o</p>

<p><strong>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</strong></p>

<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.</p>

<blockquote>
<p>You will never be happy if you continue to search for what happiness consists of. You will never live if you are looking for the meaning of life.</p>

<p>Albert Camus</p>
</blockquote>

<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.&nbsp;Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>

<h2>Rhoncus ut, imperdiet a, venenatis vitae, justo.</h2>

<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit.&nbsp;Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>

<p>Rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer cidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>';
        $faker = Faker\Factory::create();
        $categories = \App\Category::orderBy('id', 'asc')->take(rand(1,3))->pluck('id')->toArray();
        $media = \App\Media::create(['user_id' => 2, 'filename'=>'default.jpg','original_name'=>$faker->word,'upload_directory'=>'/images/temp','mime'=>'image/jpg', 'type'=>'image','caption'=>$faker->sentence,'description'=>$faker->sentence]);
        for($i=0; $i<=30; $i++){
            $created_dt = \Carbon\Carbon::now();
            $post = \App\Post::create([
                'title'=> $faker->sentence(6, true),
                'subtitle' => $faker->sentence(5, true),
                'content' => $content,
                'user_id' => 2,
                'publish_on'=> $created_dt,
                'featured_image' => $media->id,
                'is_breaking_news' => 1,
                'is_special_news' =>  1,
                'is_featured_news' => 1,
            ]);
            $post->categories()->sync($categories);
        }
    }
}
