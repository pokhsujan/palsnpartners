<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profile = [
            'full_name' => 'Administrator', 'nickname' => 'Gold News Admin'
        ];

        $profile2 = [
            'full_name' => 'Editor', 'nickname' => 'Gold News Editor'
        ];

        $profile3 = [
            'full_name' => 'Guest', 'nickname' => 'Gold News Guest'
        ];

        $profile4 = [
            'full_name' => 'Developer', 'nickname' => 'Gold News Developer'
        ];
        $profile5 = [
            'full_name' => 'Ad Manager', 'nickname' => 'Ad Manager'
        ];

        $user = \App\User::create([
            'username' => 'admin',
            'email' => 'admin@demo.com',
            'password' => bcrypt('password'),
            'email_verified_at' => '2018-12-12 00:00:00',
            'status' => 1,
        ]);
        $user1 = \App\User::create([
            'username' => 'editor',
            'email' => 'editor@demo.com',
            'email_verified_at' => '2018-12-12 00:00:00',
            'password' => bcrypt('password'),
            'status' => 1,
        ]);

        $user2 = \App\User::create([
            'username' => 'guest',
            'email' => 'guest@demo.com',
            'password' => bcrypt('password'),
            'email_verified_at' => '2018-12-12 00:00:00',
            'status' => 1,
        ]);
        $user3 = \App\User::create([
            'username' => 'developer',
            'email' => 'developer@demo.com',
            'email_verified_at' => '2018-12-12 00:00:00',
            'password' => bcrypt('password'),
            'status' => 1,
        ]);
        $user4 = \App\User::create([
            'username' => 'admanager',
            'email' => 'admanager@demo.com',
            'email_verified_at' => '2018-12-12 00:00:00',
            'password' => bcrypt('password'),
            'status' => 1,
        ]);

        $admin = \App\Role::whereName('admin')->first();
        $editor = \App\Role::whereName('editor')->first();
        $guest = \App\Role::whereName('user')->first();
        $developer = \App\Role::whereName('developer')->first();
        $admanager = \App\Role::whereName('admanager')->first();


        $userprofile = new \App\Profile($profile);
        $userprofile2 = new \App\Profile($profile2);
        $userprofile3 = new \App\Profile($profile3);
        $userprofile4 = new \App\Profile($profile4);
        $userprofile5 = new \App\Profile($profile5);


        $user->profile()->save($userprofile);
        $user1->profile()->save($userprofile2);
        $user2->profile()->save($userprofile3);
        $user3->profile()->save($userprofile4);
        $user4->profile()->save($userprofile5);


        $user->attachRole($admin);
        $user1->attachRole($editor);
        $user2->attachRole($guest);
        $user3->attachRole($developer);
        $user4->attachRole($admanager);
    }
}
