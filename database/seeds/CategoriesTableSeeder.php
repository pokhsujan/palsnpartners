<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Politics',
            'Economics',
            'World',
            'Sports',
            'Literature',
            'Entertainment',
            'Tech',
            'Health',
            'Travel',
            'Interviews',
            'Photos',
            'Videos',
            'Lifestyle',
            'History',
            'Weird',
            'Blog',
        ];

        foreach ($categories as $category) {
            \App\Category::create(
                [
                    'name' => $category
                ]
            );
        }
    }
}
