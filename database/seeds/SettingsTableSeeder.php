<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            "site_name" => "kMag",
            "site_tagline" => "Laravel Blog CMS",
            "logo" => "/images/logo.png",
            "favicon" => '',
            "head_tags" => '',
            "office_address" => "Sachetan Marg, Baneshwor, Kathmandu, Nepal",
            "email" => "im@hsameer.com.np",
            "phone_1" => "9851070747",
            "facebook_url" => "http://facebook.com/",
            "youtube_url" => "http://youtube.com/",
            "twitter_url" => "http://twitter.com/",
            "instagram_url" => '',
            "gplus_url" => "http://plus.google.com/user/",
            "fb_page_id" => '',
            "ia_ad_1" => '',
            "ia_ad_2" => '',
            "footer_copyright" => '',
            "footer_scripts" => '',
            "facebook_login" => false,
            "posts_per_page" => 12,
            "recent_posts_per_page" => 12,
            "enable_comment" => true,
            "comment" => "facebook",
            "enable_rating" => true,
            "enable_top_bar" => true,
            "top_date" => true,
            "twitter_login" => true,
            "google_login" => true,
        ];
        $rules = \App\Setting::getValidationRules();
        $data = $settings;
        $validSettings = array_keys($rules);
        foreach ($data as $key => $val) {
            if (in_array($key, $validSettings)) {
                \App\Setting::add($key, $val, \App\Setting::getDataType($key));
            }
        }
    }
}
