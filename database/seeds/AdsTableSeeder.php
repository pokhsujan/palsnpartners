<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('ads')->delete();
        $faker = Faker\Factory::create();
        $large_ad_media = \App\Media::create(['user_id' => 2, 'filename'=>'ad-large.jpg','original_name'=>'Large Demo Ad','upload_directory'=>'/images/temp','mime'=>'image/jpg', 'type'=>'image','caption'=>$faker->sentence,'description'=>$faker->sentence]);
        foreach (config('site.adpos') as $k => $value) {
             \App\Adspace::insert(['name' => $value, 'ad_position' => $k]);
        }
        $adspace = \App\Ad::get();
        foreach ($adspace as $adspace) {
            $ad = \App\Ad::create([
                'title' => $faker->sentence,
                'ad_type' => 'image_ad',
                'start_date' => \Carbon\Carbon::now()->format('Y-m-d'),
                'end_date' => \Carbon\Carbon::now()->endOfMonth()->format('Y-m-d'),
                'featured_image' => $large_ad_media->id,
                'url' => 'http://lamputer.com',
                'script' => null,
            ]);
            $ad->adspaces()->sync($adspace->id);
        }
    }
}
