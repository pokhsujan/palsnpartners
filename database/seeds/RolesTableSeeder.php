<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new \App\Role();
        $owner->name = 'admin';
        $owner->display_name = 'Admin'; // optional
        $owner->description = 'Have Control to all resources'; // optional
        $owner->save();

        $editor = new \App\Role();
        $editor->name = 'editor';
        $editor->display_name = 'Editor'; // optional
        $editor->description = 'Have Control to Specific Posts resources'; // optional
        $editor->save();

        $moderator = new \App\Role();
        $moderator->name = 'moderator';
        $moderator->display_name = 'Moderator'; // optional
        $moderator->description = 'Have Control to All Posts'; // optional
        $moderator->save();

        $developer = new \App\Role();
        $developer->name = 'developer';
        $developer->display_name = 'Developer'; // optional
        $developer->description = 'Developer who write codes'; // optional
        $developer->save();

        $admanager = new \App\Role();
        $admanager->name = 'admanager';
        $admanager->display_name = 'Ad Manager'; // optional
        $admanager->description = 'Can Manage Ads '; // optional
        $admanager->save();

        $user = new \App\Role();
        $user->name = 'user';
        $user->display_name = 'Default User'; // optional
        $user->description = 'User who can only view'; // optional
        $user->save();
    }
}
