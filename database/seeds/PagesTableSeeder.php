<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $homepage = [
            'id' => 1,
            'title' => 'Your Site Homepage',
            'slug' => 'homepage',
            'content' => 'Homepage Contents.',
        ];
        $metas = [
            'banner_style'=> 'style-1',
            'enable_spotlight'=> true,
            'enable_recent_news'=> true,
            'recent_post_title'=> 'Recent Posts',
            'recent_posts_per_page'=> 20,
        ];
       $page = \App\Page::firstOrCreate($homepage);
       $page->syncMeta($metas);
    }
}
