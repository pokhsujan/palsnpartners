<?php

use Illuminate\Database\Seeder;

class WidgetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('site.widgets') as $widget_key =>$widget){
            \App\Widget::create([
                'title' => $widget,
                'widget_id' => $widget_key,
                'sidebar_type' => 'default',
            ]);
        }
    }
}
