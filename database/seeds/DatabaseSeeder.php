<?php

use Illuminate\Database\Seeder;
use App\Library\Installer\InstalledFileManager;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(InstalledFileManager $installManager)
    {

        if(!$this->alreadyInstalled()){
            $this->call(RolesTableSeeder::class);
            $this->call(PermissionsTableSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(SettingsTableSeeder::class);
            $this->call(WidgetsTableSeeder::class);
            $this->call(CategoriesTableSeeder::class);
            $this->call(PostsTableSeeder::class);
            $this->call(AdsTableSeeder::class);
            $this->call(PagesTableSeeder::class);
        }

    }
    /**
     * If application is already installed.
     *
     * @return bool
     */
    public function alreadyInstalled()
    {
        return file_exists(storage_path('installed'));
    }

}
