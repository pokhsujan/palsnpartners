<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manager = new Permission();
        $manager->name   = 'access-admin';
        $manager->display_name   = 'Access Admin Area';
        $manager->description    = 'can be able to access backend with other permissions';
        $manager->save();

        $managePost = new Permission();
        $managePost->name   = 'manage-posts';
        $managePost->display_name   = 'Manage Posts';
        $managePost->description    = 'user can be able to create, update and delete posts, category and tags';
        $managePost->save();

        $manageMedia = new Permission();
        $manageMedia->name   = 'manage-media';
        $manageMedia->display_name   = 'Manage Media';
        $manageMedia->description    = 'user can be able to create, update and delete media';
        $manageMedia->save();

        $managePage = new Permission();
        $managePage->name   = 'manage-pages';
        $managePage->display_name   = 'Manage Pages';
        $managePage->description    = 'user can be able tocreate, update and delete pages';
        $managePage->save();

        $manageMenus = new Permission();
        $manageMenus->name   = 'manage-menus';
        $manageMenus->display_name   = 'Manage Menus';
        $manageMenus->description    = 'user can be able to edit menus';
        $manageMenus->save();

        $manageAds = new Permission();
        $manageAds->name   = 'manage-ads';
        $manageAds->display_name   = 'Manage Ads';
        $manageAds->description    = 'user can be able to access ads features';
        $manageAds->save();

        $manageWidgets = new Permission();
        $manageWidgets->name   = 'manage-widgets';
        $manageWidgets->display_name   = 'Manage Widgets';
        $manageWidgets->description    = 'user can be able to access Widget features';
        $manageWidgets->save();

        $manageUsers = new Permission();
        $manageUsers->name   = 'manage-users';
        $manageUsers->display_name   = 'Manage Users';
        $manageUsers->description    = 'can be able to create, update and delete users';
        $manageUsers->save();

        $admin = Role::whereName('admin')->first();
        $editor = Role::whereName('editor')->first();
        $moderator = Role::whereName('moderator')->first();
        $developer = Role::whereName('developer')->first();
        $admanager = Role::whereName('admanager')->first();
        $guest = Role::whereName('user')->first();

        $admin->attachPermissions(array($manager, $managePost, $managePage, $manageMedia, $manageMenus,$manageAds, $manageWidgets, $manageUsers));
        $editor->attachPermissions(array($manager, $managePost, $manageMedia));
        $moderator->attachPermissions(array($manager, $managePost, $managePage, $manageMedia, $manageMenus));
        $developer->attachPermissions(array($manager, $managePost, $managePage, $manageMedia, $manageMenus,$manageAds, $manageWidgets, $manageUsers));
        $guest->attachPermissions(array($manager));
        $admanager->attachPermissions(array($manager, $manageMedia, $manageAds));
    }
}
