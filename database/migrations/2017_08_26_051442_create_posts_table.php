<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->unsigned();
            $table->string('slug')->unique();
            $table->text('title');
            $table->text('subtitle')->nullable()->default(null);
            $table->longText('content');
            $table->unsignedInteger('featured_image')->nullable();
            $table->string('status')->default('publish');
            $table->string('format')->default('standard');
            $table->dateTime('publish_on')->nullable();
            $table->boolean('is_breaking_news')->default(0);
            $table->boolean('is_special_news')->default(0);
            $table->boolean('is_featured_news')->default(0);
            $table->integer('view_count')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('featured_image')->references('id')->on('media')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
