<p align="center"><img src="http://www.kumaley.com/images/logo-gold.png"></p>



## About Kumaley Blog

#Features Here

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Installation

<b>Server Requirements</b>
https://laravel.com/docs/5.7/installation#server-requirements

1. cd kumaley
2. composer update
3. php artisan migrate
4. php artisan db:seed
5. visit yourdomain.com

Default
login path = yoursite.com/login

##Login With Roles
http://your-site.com/login

###admin
username: admin@demo.com<br>
password: password

###editor
username: editor@demo.com<br>
password: password

###guest
username: guest@demo.com<br>
password: password

###moderator
username: moderator@demo.com<br>
password: password

###developer
username: developer@demo.com<br>
password: password
