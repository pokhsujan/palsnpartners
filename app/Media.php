<?php

namespace App;

use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;

class Media extends Model
{

    use Uploadable;
    protected $fillable = ['filename', 'user_id', 'original_name', 'upload_directory', 'mime', 'type', 'caption', 'description'];
    protected $appends = array('preview_url', 'media_url', 'image_size');


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getPreviewUrlAttribute()
    {
        if ($this->type == 'image') {
            if (in_array($this->mime, ['image/jpeg','image/jpg'])) {
                return route('images.show', ['small', $this->filename]);
            }
            return \Storage::url($this->upload_directory . '/' . $this->filename);
        } else {
            return asset('/images/media/' . explode('.', $this->filename)[1] . '.svg');
        }
    }

    public function getMediaUrlAttribute()
    {
        return \Storage::url($this->upload_directory . '/' . $this->filename);
    }
}
