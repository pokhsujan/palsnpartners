<?php
namespace App\ViewComposers;
use App\Post;
use Carbon\Carbon;
use Illuminate\View\View;

class WidgetsComposer
{

    private $missed_posts;
    public function __construct(Post $post)
    {
        $this->post= $post;
    }

    public function compose(View $view)
    {
        $one_month = Carbon::now()->subDays(100);
        $missed_posts = $this->missed_posts = $this->post->with('media')->where('created_at','>=', $one_month)->inRandomOrder()->take(3)->get();
        $view->with('missed_news', $missed_posts);
    }
}
