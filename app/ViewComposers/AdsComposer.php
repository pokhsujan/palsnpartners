<?php
namespace App\ViewComposers;
use App\Ad;
use App\Adspace;
use Illuminate\View\View;

class AdsComposer
{
    private $ads;
    public function compose(View $view)
    {
        if(!$this->ads){
            $this->ads = Ad::with('media', 'adspaces')->runningAds()->get();
        }
        $this->adspaces = Adspace::has('ads')->get();
        $view->with('ads', $this->ads);
        $view->with('adspaces', $this->adspaces);
    }
}