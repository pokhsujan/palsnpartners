<?php
namespace App\ViewComposers;
use App\Adspace;
use App\Post;
use App\Widget;
use Illuminate\View\View;

class SidebarComposer
{

    public function __construct(Post $post, Widget $widget, Adspace $adspace)
    {
        $this->post= $post;
        $this->widget= $widget;
        $this->adspace= $adspace;
    }

    public function compose(View $view)
    {
        $ad_positions = $this->adspace->whereNotIn('ad_position', array_keys(config('site.adpos')))->pluck('ad_position')->toArray();
        $widgets = $this->widget->orderBy('id', 'ASC')->get()->groupBy('sidebar_type');
        $featured_news = $this->post->published()->with(['media','user.profile'])->latest()->where('is_featured_news', true)->take(5)->get();
        $latest_posts = $this->post->published()->with(['media','user.profile'])->latest()->take(5)->get();
        $popular_posts = $this->post->published()->with(['media','user.profile'])->orderBy('view_count', 'DESC')->take(5)->get();
        $view->with('ad_positions', $ad_positions);
        $view->with('widgets', $widgets);
        $view->with('featured_posts', $featured_news);
        $view->with('latest_posts', $latest_posts);
        $view->with('popular_posts', $popular_posts);
    }
}
