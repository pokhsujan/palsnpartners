<?php

namespace App\ViewComposers;

use App\Menu;
use Illuminate\View\View;

class MenusComposer
{
    private $navmenu;

    public function compose(View $view)
    {
        if (!$this->navmenu) {
            $this->navmenu = \Cache::remember('navmenu', 60, function () {
                return Menu::whereNull('parent_id')->get()->keyBy('menu_type');
            });
        }
        $view->with('navmenu', $this->navmenu);
    }
}