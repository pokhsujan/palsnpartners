<?php
namespace App\ViewComposers;
use App\Post;
use App\Widget;
use Illuminate\View\View;

class SearchComposer
{

    public function __construct(Post $post, Widget $widget)
    {
        $this->post= $post;
        $this->widget= $widget;
    }

    public function compose(View $view)
    {
        $recent_news = $this->post->published()->latest()->take(3)->get();
        $view->with('recent_news', $recent_news);
    }
}