<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;

class UserLogEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function onUserLogin($event)
    {
        $user = $event->user;
        $session_id = uniqid();
        session(['login_id' => $session_id]);
        $log_data = [
            'session_id' =>$session_id,
            'ip_address' => $this->request->getClientIp(),
            'user_agent' => $this->request->userAgent(),
            'login_at' => Carbon::now(),
        ];
        $log = new \App\Userlog($log_data);
        $user->log()->save($log);
    }

    public function onUserLogout($event)
    {
        $user = $event->user;
        $userlog = $user->log->where('session_id','==',session()->get('login_id'))->first();
        if($userlog){
            $userlog->logout_at = $userlog->freshTimestamp();
            $userlog->save();
        }
    }

    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserLogEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserLogEventSubscriber@onUserLogout'
        );
    }
}
