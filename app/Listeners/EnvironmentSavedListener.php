<?php

namespace App\Listeners;

use App\Events\Installer\EnvironmentSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Artisan;

class EnvironmentSavedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EnvironmentSaved $environmentSaved)
    {
        $this->environmentSaved = $environmentSaved;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Artisan::call('optimize');
    }
}
