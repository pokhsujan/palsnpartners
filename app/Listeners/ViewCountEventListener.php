<?php

namespace App\Listeners;

use App\Events\ViewCount;
use Illuminate\Session\Store;

class ViewCountEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $session;
    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle the event.
     *
     * @param  PostViewCount  $event
     * @return void
     */
    public function handle(ViewCount $event)
    {
        $model= $event->model;
        if ( ! $this->isPostViewed($model))
        {
            $model->increment('view_count');
            $model->view_count += 1;
            $this->storeViewTime($model);
        }

        $models = $this->getViewedPosts();
        if ( ! is_null($models))
        {
            $models = $this->cleanExpiredViews($models);
            $this->storePostView($models);
        }
    }

    private function storePostView($models)
    {
        // Push the post id onto the viewed_posts session array.
        $this->session->put('viewed', $models);
    }

    public function storeViewTime($model)
    {
        // First make a key that we can use to store the timestamp
        // in the session. Laravel allows us to use a nested key
        // so that we can set the post id key on the viewed_posts
        // array.
        $class = strtolower (preg_replace('/\\\\/','-',get_class($model) ));
        $key = 'viewed.' . $class .$model->id;

        // Then set that key on the session and set its value
        // to the current timestamp.
        $this->session->put($key, time());
    }


    private function isPostViewed($model)
    {
        // Get all the viewed posts from the session. If no
        // entry in the session exists, default to an
        // empty array.
        $viewed = $this->session->get('viewed', []);
        // Check if the post id exists as a key in the array.
        $class = strtolower (preg_replace('/\\\\/','-',get_class($model) ));
        return array_key_exists($class.$model->id, $viewed);
    }

    private function getViewedPosts()
    {
        // Get all the viewed posts from the session. If no
        // entry in the session exists, default to null.
        return $this->session->get('viewed', null);
    }

    private function cleanExpiredViews($models)
    {
        $time = time();

        // Let the views expire after one hour.
        $throttleTime = 3600;

        // Filter through the post array. The argument passed to the
        // function will be the value from the array, which is the
        // timestamp in our case.
        return array_filter($models, function ($timestamp) use ($time, $throttleTime)
        {
            // If the view timestamp + the throttle time is
            // still after the current timestamp the view
            // has not expired yet, so we want to keep it.
            return($timestamp + $throttleTime) > $time;
        });
    }
}
