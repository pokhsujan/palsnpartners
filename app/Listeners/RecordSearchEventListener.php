<?php

namespace App\Listeners;


use App\Events\RecordSearch;

class RecordSearchEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(RecordSearch $searchrecord)
    {
        $this->searchrecord = $searchrecord;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(strlen($event->query) >= 5 && $event->post_count > 0){
            $searchrecord = $this->searchrecord->where('query', 'like', "%{$event->query}%")->first();
            if($searchrecord){
                if($searchrecord->post_count < $event->post_count){
                    $searchrecord->query = $event->query;
                    $searchrecord->post_count = $event->post_count;
                }
                $searchrecord->search_count +=1;
                $searchrecord->save();
            }else{
                $this->searchrecord->create([
                    'query'=>$event->query,
                    'post_count' => $event->post_count,
                    'search_count' => 1
                ]);
            }
        }
    }
}
