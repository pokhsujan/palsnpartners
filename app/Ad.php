<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable=['title','start_date','end_date','featured_image','ad_type','script','url'];
    protected $dates = ['start_date','end_date'];

    public static function boot()
    {
        parent::boot();
    }

    public function media()
    {
        return $this->hasOne(Media::class,'id','featured_image');
    }

    public function adspaces()
    {
        return $this->belongsToMany(Adspace::class, 'ad_adspace');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = Carbon::createFromFormat('Y-m-d',$value);
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }


    public function scopeRunningAds($query)
    {
        $query->where([
            ['start_date','<=',Carbon::today()],
            ['end_date','>=',Carbon::today()]
        ]);
    }

    public function scopeExpiredAds($query)
    {
        $query->where('end_date','<',Carbon::today());

    }

    public function scopeUpcomingAds($query)
    {
        $query->where('start_date','>',Carbon::today());
    }
}
