<?php
if (!function_exists('setting')) {
    function setting($key = null, $default = null)
    {
        $setting = app('setting');

        if (is_null($key)) {
            return $setting;
        }
        return $setting->get($key, $default);
    }
}

if (!function_exists('menusRecursive')) {
    function menusRecursive($menus)
    {
        foreach ($menus as $menu) { ?>
            <li class="dd-item" data-id="<?= $menu->id; ?>" data-display_name="<?= $menu->display_name; ?>"
                data-menu_link="<?= $menu->menu_link; ?>" data-menu_type="<?= $menu->menu_type; ?>">
                <div class="dd-handle"><?= $menu->display_name; ?></div>
                <span class="menu-addons pull-right"><a class="button-edit text-primary"><i class="fa fa-edit"></i></a>
                    <a
                            class="button-delete text-danger""><i class="fa fa-trash"></i></a></span>
                <?php if (!($menu->children->isEmpty())) { ?>
                    <ol class="dd-list"><?php menusRecursive($menu->children); ?></ol> <?php } ?>
            </li>
        <?php }
    }
}


if (!function_exists('classActivePath')) {
    function classActivePath($path, $active = 'current-menu')
    {
        return call_user_func_array('Request::is', (array)$path) ? 'class="' . $active . '"' : '';
    }
}


if (!function_exists('nav_menu')) {
// Menu Helper Functions
    function nav_menu($menus, $menu_type)
    {
        $output = '';
        if ($menus->has($menu_type)) {
            $queried_menu = $menus[$menu_type];
            $output .= buildMenu($queried_menu->children);
        }
        return $output;

    }
}

if (!function_exists('buildMenu')) {
    function buildMenu($menus)
    {
        $dc = '';
        foreach ($menus as $menu) {

            if (!$menu->children->isEmpty()) {
                $dc = ' class="menu-item-has-children"';
            } ?>
            <li<?php echo $dc; ?>>
                <a href="<?php echo $menu->menu_link; ?>"><?php echo $menu->display_name; ?></a>
                <?php if (!$menu->children->isEmpty()) { ?>
                    <ul class="sub-menu">
                        <?php $dc = buildMenu($menu->children); ?>
                    </ul>
                <?php } ?>
            </li>
        <?php }
    }
}

if (!function_exists('nl2p')) {
    function nl2p($string)
    {
        $arr = explode("\n", $string);
        $out = '';

        for ($i = 0; $i < count($arr); $i++) {
            if (strlen(trim($arr[$i])) > 0)
                if ($arr[$i] != '') {
                    $out .= '<p>' . trim($arr[$i]) . '</p>';
                }
        }
        return $out;
    }
}
if (!function_exists('toP')) {
    function toP($text)
    {
        $paragraphs = preg_split("/[\n]{2,}/", $text);
        foreach ($paragraphs as $key => $p) {
            $paragraphs[$key] = "<p>" . str_replace("<br>", "\n", $paragraphs[$key]) . "</p>";
        }
        $text = implode("", $paragraphs);

        return $text;
    }
}
if (!function_exists('bytesToHuman')) {
    function bytesToHuman($bytes)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }
        return round($bytes, 2) . ' ' . $units[$i];
    }
}
if (!function_exists('imageDimention')) {
    function imageDimention($file, $admin = false)
    {
        list($width, $height) = getimagesize($file);
        if ($admin) {
            return $width . ' X ' . $height . ' px';
        }
        return 'width="' . $width . '" height="' . $height . '"';
    }
}
if (!function_exists('youtubeid')) {
    function youtubeid($url)
    {
        var_dump($url);
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    }
}

if (!function_exists('parse_youtube_url')) {
    function parse_youtube_url($url, $return = 'embed', $width = '', $height = '', $rel = 0)
    {
        $urls = parse_url($url);

        //url is http://youtu.be/abcd
        if ($urls['host'] == 'youtu.be') {
            $id = ltrim($urls['path'], '/');
        } //url is http://www.youtube.com/embed/abcd
        else if (strpos($urls['path'], 'embed') == 1) {
            $id = end(explode('/', $urls['path']));
        } //url is xxxx only
        else if (strpos($url, '/') === false) {
            $id = $url;
        }
        //http://www.youtube.com/watch?feature=player_embedded&v=ML2KAaR26Pk
        //url is http://www.youtube.com/watch?v=ML2KAaR26Pk
        else {
            parse_str($urls['query']);
            $id = $v;
            if (!empty($feature)) {
                $id = end(explode('v=', $urls['query']));
            }
        }
        //return embed iframe
        if ($return == 'embed') {
            return '<iframe width="' . ($width ? $width : 560) . '" height="' . ($height ? $height : 349) . '" src="http://www.youtube.com/embed/' . $id . '?rel=' . $rel . '" frameborder="0" allowfullscreen></iframe>';
        } //return normal thumb
        else if ($return == 'thumb') {
            return 'http://i1.ytimg.com/vi/' . $id . '/default.jpg';
        } //return hqthumb
        else if ($return == 'hqthumb') {
            return 'http://i1.ytimg.com/vi/' . $id . '/hqdefault.jpg';
        } // else return id
        else {
            return $id;
        }
    }
}
if (!function_exists('prefix_insert_after_paragraph')) {
    function prefix_insert_after_paragraph($insertion, $paragraph_id, $content)
    {
        $closing_p = '</p>';
        $paragraphs = explode($closing_p, $content);
        foreach ($paragraphs as $index => $paragraph) {
            // Only add closing tag to non-empty paragraphs
            if (trim($paragraph)) {
                // Adding closing markup now, rather than at implode, means insertion
                // is outside of the paragraph markup, and not just inside of it.
                $paragraphs[$index] .= $closing_p;
            }
            // + 1 allows for considering the first paragraph as #1, not #0.
            if ($paragraph_id == $index + 1) {
                $paragraphs[$index] .= $insertion;
            }
        }
        return implode('', $paragraphs);
    }
}

if (!function_exists('get_style')) {
    function get_style($style)
    {
        switch ($style) {
            case 'default':
                return 'app.css';
                break;
            case 'red':
                return 'app-red.css';
                break;
            case 'blue':
                return 'app-blue.css';
                break;
            case 'green':
                return 'app-green.css';
                break;
            default:
                return 'app-red.css';
        }
    }
}
if (!function_exists('FileSizeConvert')) {
    function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
}
if (!function_exists('FileSizeConvert')) {

    function realFileSize($path)
    {
        if (!file_exists($path))
            return false;

        $size = filesize($path);

        if (!($file = fopen($path, 'rb')))
            return false;

        if ($size >= 0) {//Check if it really is a small file (< 2 GB)
            if (fseek($file, 0, SEEK_END) === 0) {//It really is a small file
                fclose($file);
                return $size;
            }
        }

        //Quickly jump the first 2 GB with fseek. After that fseek is not working on 32 bit php (it uses int internally)
        $size = PHP_INT_MAX - 1;
        if (fseek($file, PHP_INT_MAX - 1) !== 0) {
            fclose($file);
            return false;
        }

        $length = 1024 * 1024;
        while (!feof($file)) {//Read the file until end
            $read = fread($file, $length);
            $size = bcadd($size, $length);
        }
        $size = bcsub($size, $length);
        $size = bcadd($size, strlen($read));

        fclose($file);
        return $size;
    }
}

if (!function_exists('isActive')) {
    /**
     * Set the active class to the current opened menu.
     *
     * @param  string|array $route
     * @param  string $className
     * @return string
     */
    function isActive($route, $className = 'active')
    {
        if (is_array($route)) {
            return in_array(Route::currentRouteName(), $route) ? $className : '';
        }
        if (Route::currentRouteName() == $route) {
            return $className;
        }
        if (strpos(URL::current(), $route)) return $className;
    }
}

if (!function_exists('browserInfo')) {
    function browserInfo($user_agent)
    {
        $u_agent = $user_agent;
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        $browser_info = array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );

        return $browser_info['name'] . " " . $browser_info['version'] . " on " . $browser_info['platform'];
    }
}

if (!function_exists('featured_image')) {
    function featured_image($post, $size = null, $class = null, $link = false)
    {
        $class = isset($class) ? $class : 'post-media'; ?>
        <figure class="<?php echo $class; ?>">
            <?php if ($link){ ?>
            <a href="<?php echo $post->url; ?>">
                <?php } ?>
                <?php
                if ($post->format) {
                    if ($post->format == 'video') { ?>
                        <img class="img-responsive"
                             src="<?php echo route('images.video', [$size, 'ytb_thumb' => $post->video_image]); ?>"
                             alt="<?php echo $post->title; ?>" <?php if ($size) { ?> height="<?php echo config('site.image_sizes.' . $size . '.height'); ?>"
                            width="<?php echo config('site.image_sizes.' . $size . '.width'); ?>" <?php } ?>/>

                    <?php } elseif ($post->format == 'standard') {
                        if ($post->featured_image && $post->media) {
                            if ($size) { ?>
                                <img class="img-responsive"
                                     src="<?php echo route('images.show', [$size, $post->media->filename]); ?>"
                                     alt="<?php echo $post->title; ?>"
                                     height="<?php echo config('site.image_sizes.' . $size . '.height'); ?>"
                                     width="<?php echo config('site.image_sizes.' . $size . '.width'); ?>"/>
                            <?php } else { ?>
                                <img src="<?php echo url($post->media->media_url); ?>"
                                     alt="<?php echo $post->title; ?>"/>
                            <?php }
                        }
                    }
                } elseif ($post->featured_image && $post->media) {
                    if ($size) { ?>
                        <img class="img-responsive"
                             src="<?php echo route('images.show', [$size, $post->media->filename]); ?>"
                             alt="<?php echo $post->title; ?>"
                             height="<?php echo config('site.image_sizes.' . $size . '.height'); ?>"
                             width="<?php echo config('site.image_sizes.' . $size . '.width'); ?>"/>
                    <?php } else { ?>
                        <img src="<?php echo url($post->media->media_url); ?>" alt="<?php echo $post->title; ?>"/>
                    <?php }
                } else { ?>
                    <img src="<?php echo asset('default.jpg'); ?>" alt=""/>
                <?php }
                ?>
                <?php if ($link){ ?>
            </a>
        <?php } ?>
        </figure>
        <?php
    }

}
if (!function_exists('post_meta')) {
    function post_meta($post)
    {
        if (setting('post_date') || setting('post_author')) { ?>
            <div class="post-meta">
                <?php
                if (setting('post_date')) { ?>
                    <span class="post-date"><?php echo $post->published_date; ?></span>
                <?php }
                if (setting('post_author')) { ?>
                    <a href="<?php echo route('user.posts', $post->user->username); ?>"
                       class="author"><?php echo $post->user->profile->full_name ?></a>
                    <?php
                } ?>
            </div>
            <?php
        }
    }
}


?>
