<?php

namespace App;

use App\Traits\SyncMailchimp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Subscriber extends Model
{
    use Notifiable;
    protected $fillable =['email','full_name', 'subscribed'];


}
