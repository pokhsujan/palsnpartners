<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Adspace extends Model
{
    protected $fillable=['name', 'description'];
    use Sluggable;
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'ad_position' => [
                'source' => 'name'
            ]
        ];
    }

    public function ads()
    {
        return $this->belongsToMany(Ad::class, 'ad_adspace')->runningAds();
    }
}
