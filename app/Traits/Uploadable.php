<?php
namespace App\Traits;

use App\Media;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
trait Uploadable
{
    private $image_ext = ['jpg', 'jpeg', 'png', 'gif', 'ico', 'svg','bmp'];
    private $audio_ext = ['mp3', 'ogg', 'mpga'];
    private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['docx', 'doc', 'pdf', 'odt', 'txt'];
    public function upload( $form_data )
    {

        $max_size = (int)ini_get('upload_max_filesize') * 1000;
        $all_ext = implode(',', $this->allExtensions());
        $rules = [
            'file' => 'required|file|mimes:' . $all_ext . '|max:' . $max_size
        ];
        $messages = [
            'file.mimes' => 'Uploaded media is not found in supported upload format. Only '.$all_ext.' are allowed.',
            'file.required' => 'Media is required',
            'file.max' => 'Media size is greater than'. bytesToHuman($max_size),
        ];
        $folder = 'uploads';

        $validator = Validator::make($form_data, $rules, $messages);
        if ($validator->fails()) {
            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);
        }
        $media = $form_data['file'];
        $mime = $media->getMimeType();
        $originalName = $media->getClientOriginalName();
        $extension = $media->getClientOriginalExtension();

        $type = $this->getType($extension);
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
        $filename = $this->sanitize($originalNameWithoutExt);
        $allowed_filename = $this->createUniqueFilename( $filename, $extension, $folder);
        try{
            $this->imageOriginal($media, $allowed_filename, $folder);
            $sessionMedia = new Media;
            $sessionMedia->filename = $allowed_filename;
            $sessionMedia->original_name = $originalNameWithoutExt;
            $sessionMedia->upload_directory = $folder;
            $sessionMedia->type = $type;
            $sessionMedia->mime = $mime;
            $sessionMedia->user_id = Auth::id();
            $sessionMedia->save();
            return Response::json([
                'error' => false,
                'code' => 200,
                'message' => 'Media Successfully Uploaded',
                'uploadedMedia' => $sessionMedia,
            ], 200);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createUniqueFilename( $filename, $extension, $path)
    {
        $mediacheck = Media::whereFilename($filename.''.$extension)->first();
        if($mediacheck){
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return str_slug($filename) . '-' . $imageToken . '.' . $extension;
        }
        $full_media_path = $path .'/'. $filename . '.' . $extension;
        if ( Storage::exists( $full_media_path ) )
        {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return str_slug($filename) . '-' . $imageToken . '.' . $extension;
        }
        return str_slug($filename) . '.' . $extension;
    }


    public function imageOriginal( $photo, $filename, $path)
    {
        return Storage::put($path.'/'.$filename, file_get_contents($photo), 'public');
//        return Storage::putFileAs($path, $photo, $filename);
    }

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function remove($media_id)
    {
        $sessionMediaImage = Media::findorFail($media_id);
        $sessionMediaImage->delete();
        Storage::delete($sessionMediaImage->upload_directory .'/' .$sessionMediaImage->filename);
        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }

    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    /**
     * Get type by extension
     * @param  string $ext Specific extension
     * @return string      Type
     */
    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }
    }

    /**
     * Get all extensions
     * @return array Extensions of all file types
     */
    private function allExtensions()
    {
        return array_merge($this->image_ext, $this->audio_ext, $this->video_ext, $this->document_ext);
    }

}
