<?php namespace App\Traits\Rating;

use App\Rating;
use App\User;
use Illuminate\Support\Facades\Auth;

trait Rateable
{
    /**
     * This model has many ratings.
     *
     * @return Rating
     */
    public function ratings()
    {
        return $this->morphMany(Rating::class, 'rateable');
    }
    /**
     *
     * @return mix
     */
    public function avgRating()
    {
        return number_format($this->ratings()->avg('rating'));
    }
    /**
     *
     * @return mix
     */
    public function sumRating()
    {
        return $this->ratings()->sum('rating');
    }
    /**
     * @param $max
     *
     * @return mix
     */
    public function ratingPercent($max = 5)
    {
        $quantity = $this->ratings()->count();
        $total = $this->sumRating();
        return ($quantity * $max) > 0 ? $total / (($quantity * $max) / 100) : 0;
    }
    /**
     *
     * @return mix
     */
    public function countPositive()
    {
        return $this->ratings()->where('rating', '>', '0')->count();
    }
    /**
     *
     * @return mix
     */
    public function countNegative()
    {
        $quantity = $this->ratings()->where('rating', '<', '0')->count();
        return ("-$quantity");
    }
    /**
     *
     * @return mix
     */
    public function authUserRating()
    {
        $rating = $this->ratings()->whereUserId(auth()->user()->id)->first();
        if($rating){
            return $rating->rating;
        }
        return null;
    }

    /**
     * @param $data
     * @param Model      $author
     * @param Model|null $parent
     *
     * @return static
     */
    public function rating($data, User $user)
    {
        return (new Rating())->createRating($this, $data, $user);
    }
    /**
     * @param $data
     * @param Model      $author
     * @param Model|null $parent
     *
     * @return static
     */

    public function ratingUnique($data, User $user)
    {
        return (new Rating())->createUniqueRating($this, $data, $user);
    }

    /**
     * @param $id
     * @param $data
     * @param Model|null $parent
     *
     * @return mixed
     */
    public function updateRating($id, $data, Model $parent = null)
    {
        return (new Rating())->updateRating($id, $data);
    }
    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRating($id)
    {
        return (new Rating())->deleteRating($id);
    }
    public function getAvgRatingAttribute()
    {
        return $this->avgRating();
    }
    public function getratingPercentAttribute()
    {
        return $this->ratingPercent();
    }
    public function getSumRatingAttribute()
    {
        return $this->sumRating();
    }
    public function getCountPositiveAttribute()
    {
        return $this->countPositive();
    }
    public function getCountNegativeAttribute()
    {
        return $this->countNegative();
    }
    public function getAuthUserRatingAttribute()
    {
        return $this->authUserRating();
    }
}
