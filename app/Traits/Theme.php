<?php
namespace App\Traits;


trait Theme
{
    public function theme()
    {
        return config('site.theme', 'kMag');
    }
}