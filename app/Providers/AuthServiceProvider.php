<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Post' => 'App\Policies\PostPolicy',
        'App\Media' => 'App\Policies\MediaPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('delete-comment', function ($user, $comment) {
            return $user->hasRole(['admin','developer','moderator']) || $user->id == $comment->commenter_id;
        });
        // if the current user is the user that posted the comment
        // then the current user can edit the comment.
        Gate::define('edit-comment', function ($user, $comment) {
            return $user->hasRole(['admin','developer','moderator']) || $user->id == $comment->commenter_id;
        });
        // The user can only reply to other peoples comments and
        // not to his own comments.
        Gate::define('reply-to-comment', function ($user, $comment) {
            return  $user->id != $comment->commenter_id;
        });
    }
}
