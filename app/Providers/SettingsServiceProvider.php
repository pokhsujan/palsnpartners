<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Factory $cache, Setting $settings)
    {
//        $settings = $cache->remember('settings', 60, function() use ($settings)
//        {
//            return $settings->pluck('value', 'key')->all();
//        });
//        config()->set('settings', $settings);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('setting', Setting::class);
    }
}
