<?php

namespace App\Providers;

use App\Events\Installer\EnvironmentSaved;
use App\Events\MailchimpSubscribe;
use App\Listeners\EnvironmentSavedListener;
use App\Listeners\SubscribeToMailchimp;
use Illuminate\Support\Facades\Event;
use App\Events\RecordSearch;
use App\Events\ViewCount;
use App\Listeners\RecordSearchEventListener;
use App\Listeners\UserLogEventSubscriber;
use App\Listeners\ViewCountEventListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    protected $subscribe = [
        UserLogEventSubscriber::class,
    ];
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ViewCount::class => [
            ViewCountEventListener::class,
        ],
        RecordSearch::class=>[
            RecordSearchEventListener::class,
        ],
        EnvironmentSaved::class=>[
            EnvironmentSavedListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
