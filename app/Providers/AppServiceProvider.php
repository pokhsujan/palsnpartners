<?php

namespace App\Providers;

use App\ViewComposers\AdsComposer;
use App\ViewComposers\MenusComposer;
use App\ViewComposers\SearchComposer;
use App\ViewComposers\SidebarComposer;
use App\ViewComposers\WidgetsComposer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
//        $this->loadViewsFrom('', 'theme');
        View::composer(
            ['themes.kMag.menus.top-menu','themes.kMag.menus.main-menu','themes.kMag.menus.footer-menu'], MenusComposer::class
        );
        View::composer(['themes.kMag.sidebar'], SidebarComposer::class);
        View::composer(
            ['themes.kMag.partials.module-missed-posts'], WidgetsComposer::class
        );
        View::composer(
            ['themes.kMag.partials.module-search-box'], SearchComposer::class
        );
        View::composer(
            ['themes.kMag.partials.ads'], AdsComposer::class
        );
        Blade::component('themes.kMag.partials.comments.comments', 'comment');
        Blade::component('themes.kMag.partials.ratings.rating', 'rating');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MenusComposer::class);
        $this->app->singleton(SidebarComposer::class);
        $this->app->singleton(WidgetsComposer::class);
        $this->app->singleton(SearchComposer::class);
        $this->app->singleton(AdsComposer::class);
    }

    public function registerNested()
    {

    }
}
