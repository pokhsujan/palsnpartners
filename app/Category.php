<?php

namespace App;

use Baum\Node;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Plank\Metable\Metable;


class Category extends Node
{
    use Metable;
    use Sluggable;

    protected $fillable = ['name','slug','description','parent_id', 'lft', 'rgt', 'depth'];
    protected $table = 'categories';
    protected $parentColumn = 'parent_id';
    protected $leftColumnName = 'lft';
    protected $rightColumnName = 'rgt';
    protected $depthColumn = 'depth';
    protected $guarded = ['parent_id', 'lft', 'rgt', 'depth'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_category');
    }

    public function getPosts($categoryIds, $numPosts = 5)
    {
        $posts = [];
        $rawQuery = "SELECT c.id as category_id, substring_index(group_concat(p.id ORDER BY p.publish_on DESC), ',', " . $numPosts . ") post_ids FROM ". \DB::getTablePrefix() . "categories c 
                JOIN ". \DB::getTablePrefix() . "post_category pc on c.id = pc.category_id
                JOIN ". \DB::getTablePrefix() . "posts p on pc.post_id = p.id
                where( p.status = 'publish' and p.publish_on < '" . Carbon::now()->toDateTimeString() . "') 
                and category_id in (" . implode(', ', $categoryIds) . ")
                group by c.id";
        $postIds = collect(\DB::select(\DB::raw($rawQuery)))->pluck('post_ids', 'category_id');
        $postIds = $postIds->each(function ($ids) use (&$allIds) {
            $allIds[] = explode(',', $ids);
        });
        $allIds = array_flatten($allIds);
        $dbPosts = Post::with('media','user.profile')->whereIn('id', $allIds)->latest()->get();
        foreach ($postIds as $cat => $postIds) {
            $posts[$cat] = $dbPosts->whereIn('id', explode(',', $postIds));
        }
        return $posts;
    }

}
