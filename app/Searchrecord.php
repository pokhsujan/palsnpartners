<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Searchrecord extends Model
{
    protected $fillable = ['query', 'post_count', 'search_count'];
}
