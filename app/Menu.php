<?php

namespace App;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;

class Menu extends Node
{
    protected $fillable =['display_name', 'menu_type', 'menu_link', 'menu_style', 'parent_id', 'lft', 'rgt', 'depth'];


    protected $parentColumn = 'parent_id';
    protected $leftColumnName = 'lft';
    protected $rightColumnName = 'rgt';
    protected $depthColumn = 'depth';
    protected $guarded = ['parent_id', 'lft', 'rgt', 'depth'];
    protected $scoped = ['menu_type'];
}
