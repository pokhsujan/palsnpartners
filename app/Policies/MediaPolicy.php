<?php

namespace App\Policies;

use App\Media;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MediaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function view(User $user, Media $media){
        return true;
    }

    public function create(User $user){
        return $user->id > 0;
    }

    public function update(User $user, Media $media){
        if ($user->hasRole(['admin','moderator','developer'])) {
            return true;
        }
        // Check if user is the post author
        if ($user->id == $media->user_id) {
            return true;
        }

        return false;
    }
    public function delete(User $user,  Media $media){
        if ($user->hasRole(['admin','moderator','developer'])) {
            return true;
        }
        // Check if user is the post author
        if ($user->id == $media->user_id) {
            return true;
        }
        return false;
    }
}
