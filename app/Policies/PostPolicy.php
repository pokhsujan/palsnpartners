<?php

namespace App\Policies;

use App\Post;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function view(User $user, Post $post){
        return true;
    }

    public function create(User $user){
        return $user->id > 0;
    }
    public function update(User $user, Post $post){
        if ($user->hasRole(['admin','moderator','developer'])) {
            return true;
        }
        // Check if user is the post author
        if ($user->id == $post->user_id) {
            return true;
        }

        return false;
    }
    public function delete(User $user, Post $post){
        if ($user->hasRole(['admin','moderator','developer'])) {
            return true;
        }
        // Check if user is the post author
        if ($user->id == $post->user_id) {
            return true;
        }
        return false;
    }
}
