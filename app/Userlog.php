<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userlog extends Model
{
    public $timestamps = false;
    protected $table = 'user_logs';
    protected $fillable = ['session_id','ip_address','user_agent','login_at','logout_at'];
    protected $dates = ['login_at','logout_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
