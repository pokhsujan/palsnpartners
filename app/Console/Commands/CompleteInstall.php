<?php

namespace App\Console\Commands;

use App\Library\Installer\InstalledFileManager;
use Illuminate\Console\Command;

class CompleteInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'complete:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application Successfully Installed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(InstalledFileManager $installManager)
    {
        $installManager->update();
    }
}
