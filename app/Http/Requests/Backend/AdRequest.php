<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class AdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'=>'required',
            'ad_type'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'featured_image'=>'required_if:ad_type,==,image_ad',
            'url'=>'required_if:ad_type,==,image_ad',
            'script'=>'required_if:ad_type,==,script_ad',
        ];
    }
}
