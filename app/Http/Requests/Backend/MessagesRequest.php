<?php

namespace App\Http\Controllers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'full_name' => 'required|max:255',
                    'email' => 'required|email',
                    'phone' => 'required',
                    'country' => 'required',
                    'find_from' => 'required',
                    'message' => 'required',
                    'g-recaptcha-response' => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {

            }
            default:break;
        }
    }
}
