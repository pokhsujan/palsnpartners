<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    protected $permission;
    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function index()
    {
        $permissions = $this->permission->paginate();
        return view('backend.permissions.index',compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'display_name'=>'required|string|max:255',
        ]); ;
        try{
            $this->permission->create($request->only(['name', 'display_name','description']));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('permissions.index')->with('success','Permission Successfully Added!');
    }


    public function edit($id)
    {
        $permission = $this->permission->findorFail($id);
        return view('backend.permissions.edit', compact('permission'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'display_name'=>'required|string|max:255',
        ]);
        try{
            $permission = $this->permission->findorFail($id);
            $permission->update($request->only(['display_name','description']));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('permissions.index')->with('success','Permission Successfully Updated!');
    }

}
