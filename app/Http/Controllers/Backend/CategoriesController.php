<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Str;
use App\Category;

class CategoriesController extends Controller
{
    protected $category;

    public function __construct(Category $category){
        $this->category = $category;
    }

    public function index(){
        $categories = $this->category->whereNull('parent_id')->with('children')->paginate();
        $allCategories = $this->category->pluck('name','id')->all();
    	return view('backend.categories.index',compact('categories','allCategories'));
    }

    //storing the newly created category
    public function store(Request $request){
    	$this->validate($request,[
    		'name'=>'required',
            'slug'=>'required|unique:categories'
        ]);
    	$input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? NULL : $input['parent_id'];
        $this->category->create($input);
        return redirect()->back()->with('success','Category added successfully!');
    }

    
    //delete funtion
    public function destroy($id){
    	$category=$this->category->findorFail($id);
    	$category->delete();
    	return redirect()->back()->with('success','Category deleted successfully!');
    }

    public function edit($id){
    	$category=$this->category->find($id);
        $allCategories = $this->category->whereNull('parent_id')->where('id', '<>', $category->id)->pluck('name','id')->all();
        return view('backend.categories.edit',compact('category','allCategories'));
    }

    public function update(Request $request,$id){
    	$this->validate($request,[
    		'name'=>'required',
            'slug'=>'required'
    		]);
 		$category=$this->category->find($id);
        $parent_id = empty($request->input('parent_id')) ? NULL : $request->input('parent_id');
 		$category->update([
 			'name'=>$request->name,
 			'description'=>$request->description,
            'slug'=>$request->slug,
            'parent_id'=>$parent_id,
        ]);
 		return redirect()->route('categories.index')->with('success','Category updated successfully!');
    }
}
