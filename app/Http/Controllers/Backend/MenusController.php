<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Menu;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{
    protected $menu;
    protected $category;
    protected $destination;
    protected $activity;
    protected $package;
    public function __construct(Menu $menu, Category $category, Page $page)
    {
        $this->menu = $menu;
        $this->category = $category;
        $this->page = $page;
    }
    
    public function index()
    {
        return view('backend.menus.list');
    }

    public function edit(Request $request)
    {
        $menu_type = $request->input('menu_type');
        $menus = config('site.menupositions');
        if(!array_key_exists($menu_type,$menus)) return redirect()->back()->with('error',"Invalid Menu Position");
        $menu = $this->menu->whereMenuType($menu_type)->first();
        if(!$menu) $menu = $this->menu->create(['display_name'=>config('site.menupositions.'.$menu_type), 'menu_type'=>$menu_type, 'menu_link'=>'javascript:void(0);']);
        $categories = $this->category->whereNull('parent_id')->with('children')->get();
        $pages = $this->page->all();
        return view('backend.menus.index', compact('categories','menus','pages','menu','menu_type'));
    }

    public function store(Request $request)
    {
        try{
            $menus = $request->input('items');
            $ptype = $request->input('parent_type');
            $type = $request->input('type');
            $menu = $this->menu->whereMenuType($ptype)->first();
            $this->menu->whereMenuType($type)->delete();
            $menu->makeTree($menus);
            \Cache::forget('navmenu');
            return "Menu Saved Successfully!";
        }catch (\Exception $e){
            return $e->getMessage();
        }

    }
}
