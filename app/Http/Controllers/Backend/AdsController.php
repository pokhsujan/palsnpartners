<?php

namespace App\Http\Controllers\Backend;

use App\Adspace;
use App\Http\Requests\Backend\AdRequest;
use App\Http\Controllers\Controller;
use App\Ad;

class AdsController extends Controller
{
    protected $ad;
    protected $adspace;

    public function __construct(Ad $ad, Adspace $adspace){
        $this->ad=$ad;
        $this->adspace=$adspace;
    }

    // Running Ads
    public function index(){
        $advertisements= $this->ad->runningAds()->orderBy('end_date')->paginate(20);
        return view('backend.ads.index',compact('advertisements'));
    }

    // Create Add View
    public function create(){
        $adspaces = $this->adspace->get();
        return view('backend.ads.create', compact('adspaces'));
    }

    //Store function
    public function store(AdRequest $request){
        $advertisement = $this->ad->create($request->all());
        if ($request->has('adspace_id')) {
            $advertisement->adspaces()->sync($request->input('adspace_id'));
        }
        if($request->input('submit') == 'save_continue'){
            return redirect()->route('ads.edit',$advertisement->id);
        }
        return redirect()->route('ads.index')->with('success','Ad Created Successfully');
    }

    //edit page
    public function edit($id){
        $advertisement = $this->ad->findorFail($id);
        $adspaces = $this->adspace->get();
        return view('backend.ads.edit',compact('advertisement','adspaces'));

    }

    //update function
    public function update(AdRequest $request,$id){
        $advertisement = $this->ad->findorFail($id);
        $advertisement->update($request->all());
        if ($request->has('adspace_id')) {
            $advertisement->adspaces()->sync($request->input('adspace_id'));
        }else{
            $advertisement->adspaces()->detach();
        }
        if($request->input('submit') == 'save_continue'){
            return redirect()->route('ads.edit',$advertisement->id);
        }
        return redirect()->route('ads.index')->with('success','Ad Updated Successfully');

    }

    //delte function
    public function destroy($id){
        $advertisement = $this->ad->findorFail($id);
        $advertisement->delete();
        return redirect()->back();
    }

    //upcoming ads
    public function upcoming(){
        $advertisements = $this->ad->upcomingAds()->orderBy('start_date')->paginate();
        return view('backend.ads.upcoming',compact('advertisements'));
    }

    //expired ads
    public function expired(){
        $advertisements=$this->ad->expiredAds()->orderBy('end_date')->paginate();
        return view('backend.ads.expired',compact('advertisements'));
    }
}
