<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use Str;
use DB;
use Session;

class TagsController extends Controller
{
	protected $tag;
	public function __construct(Tag $tag){
		$this->tag=$tag;
	}

	//listing all tags
    public function index(){
    	$tags=$this->tag->paginate(20);
    	return view('backend.tags.index',compact('tags'));
    }

    //storing tags
    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
        ]);
        try{
            $this->tag->create($request->only('slug','name'));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('tags.index')->with('success','Tag Successfully updated!');
    }
    
    //delete fcuntion for tags
    public function destroy($id){
        try{
            $tag=$this->tag->findorFail($id);
            $tag->delete();
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->back()->with('success','Tag Successfully Deleted!');
    }
    //edit page for tags
    public function edit($id){
    	$tag=$this->tag->findOrFail($id);
    	return view('backend.tags.edit',compact('tag'));
    }

    //updating tags
    public function update(Request $request,$id){
    	$this->validate($request,[
            'name' => 'required|string|max:255',
    		]);
        try{
            $tag = $this->tag->findOrFail($id);
            $tag->update($request->only('name'));
        }catch (\Exception $exception){
            dd($exception);
        }
    	return redirect()->route('tags.index')->with('success','Tag Successfully updated!');
    }

    public function tagslist(Request $request)
    {
        $name = $request->input('q');
        $tags = $this->tag->where('name','LIKE','%'.$name.'%')->pluck('name')->toArray(); //[text=>'name',1]
        return response()->json($tags);
    }
}
