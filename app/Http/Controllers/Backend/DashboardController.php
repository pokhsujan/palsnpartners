<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\User;

class DashboardController extends Controller
{

    public function __construct(Category $category, Post $post, User $user){
    	$this->category=$category;
    	$this->post=$post;
    	$this->user=$user;
    }

    public function index(){
        $recentPosts= $this->post->published()->latest('publish_on')->take(5)->get();
        $popularPosts= $this->post->published()->orderBy('view_count', 'DESC')->take(5)->get();
        $users = $this->user->latest('created_at')->take(5)->get();
        return view('backend.dashboard', compact('recentPosts','popularPosts', 'users'));
    }
}
