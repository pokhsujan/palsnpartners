<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Category;
use App\Events\ViewCount;
use App\Http\Controllers\Requests\PostRequest;
use App\Post;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    protected $post;
    protected $category;
    protected $tag;
    protected $user;

    public function __construct(Post $post, Category $category, Tag $tag, Auth $user)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $this->post = $post;
        $this->category = $category;
        $this->tag = $tag;
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $order = $request->input('order')?$request->input('order'):'created_at';
        $order_by = $request->input('orderby')?$request->input('orderby'):'DESC';
        $posts= $this->posts()->published()->orderBy($order, $order_by)->paginate(20);
        $order_by = $order_by == 'asc'?'desc':'asc';
        return view('backend.posts.index', compact('posts','order_by'));
    }

    public function show($slug)
    {
        $post = $this->post->published()->with('media','user.profile','categories')
                           ->whereSlug($slug)
                           ->firstorFail();
        \Event::fire( new ViewCount($post));
        $categories= $post->categories;
        $cat_ids = $categories->pluck('id')->toArray();
        $related_posts = $this->post->published()->with('media')
                                    ->whereHas('categories', function ($q) use ($cat_ids) {
                                        $q->whereIn('categories.id', $cat_ids);
                                    })
                                    ->whereNotIn('id', [$post->id])
                                    ->latest()
                                    ->take(6)
                                    ->get();
        $tags=$post->tags;
        return view('frontend.single',compact('post','categories','tags','related_posts'));
    }

    public function search(Request $request)
    {
        $query = $request->input('s');
        $posts=$this->posts()->whereHas('categories', function ($q) use ($query) {
            $q->where('name', 'LIKE', '%'.$query.'%');
        })->orWhere('title','LIKE','%'.$query.'%')
            ->orWhere('content','LIKE','%'.$query.'%')
            ->paginate(10);
        return view('backend.posts.search', compact('posts','query'));
    }

    public function draft(Request $request)
    {
        $order = $request->input('order')?$request->input('order'):'created_at';
        $order_by = $request->input('orderby')?$request->input('orderby'):'DESC';
        $posts= $this->posts()->draft()->orderBy($order, $order_by)->paginate(20);
        $order_by = $order_by == 'asc'?'desc':'asc';
        return view('backend.posts.draft', compact('posts', 'order_by'));
    }


    public function pending(Request $request)
    {
        $order = $request->input('order')?$request->input('order'):'created_at';
        $order_by = $request->input('orderby')?$request->input('orderby'):'DESC';
        $posts= $this->posts()->pending()->orderBy($order, $order_by)->paginate(20);
        $order_by = $order_by == 'asc'?'desc':'asc';
        return view('backend.posts.pending', compact('posts', 'order_by'));
    }

    public function trash(Request $request)
    {
        $order = $request->input('order')?$request->input('order'):'created_at';
        $order_by = $request->input('orderby')?$request->input('orderby'):'DESC';
        $posts= $this->posts()->onlyTrashed()->orderBy($order, $order_by)->paginate(20);
        $order_by = $order_by == 'asc'?'desc':'asc';
        return view('backend.posts.trash', compact('posts','order_by'));
    }

    public function create()
    {
        $users = \App\User::all()->pluck('fullname','id')->toArray();
        $categories = $this->category->whereNull('parent_id')->with('children')->get();
        return view('backend.posts.create', compact('categories','users'));
    }

    public function store(PostRequest $request)
    {
        $data = $request->all();
        try{
            $published_date = Carbon::parse($request->input('publish_on'));

            $data['user_id'] = auth()->user()->id;
            $data['publish_on'] = $published_date;
            $post = $this->post->create($data);

            if (!empty($data['category_id'])) {
                $post->categories()->sync($data['category_id']);
            }
            if(!empty($data['tags'])) {
                $this->tagsFilter($data['tags'], $post);
            }
            if($request->input('submit') == 'save_continue') {
                return redirect()->route('posts.edit', $post->id)->with('success','Post Created Successfully!!');
            }
            return redirect()->route('posts.index')->with('success','Post Created Successfully!!');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function edit($id)
    {
        $post = $this->post->findOrFail($id);
        event(new ViewCount($post));
        $users = \App\User::all()->pluck('fullname','id')->toArray();
        $this->authorize('update', $post);
        $categories = $this->category->whereNull('parent_id')->with('children')->get();
        return view('backend.posts.edit', compact('post', 'categories','users'));
    }

    public function update(PostRequest $request, $id)
    {
        $data = $request->all();
        $published_date =   Carbon::parse($request->input('publish_on'));
        $data['publish_on'] = $published_date;
        $data['is_breaking_news'] = $request->input('is_breaking_news')?$request->input('is_breaking_news'):0;
        $data['is_special_news'] =  $request->input('is_special_news')?$request->input('is_special_news'):0;
        $data['is_featured_news'] = $request->input('is_featured_news')?$request->input('is_featured_news'):0;
        try{
            $post = $this->post->findOrFail($id);
            $this->authorize('update', $post);      // authorized user for this post (admin,developer and $post->user_id)
            $post->update($data);
            if (!empty($data['category_id'])) {
                $post->categories()->sync($data['category_id']);
            }else{
                $post->categories()->detach();
            }
            if(!empty($data['tags'])){
                $this->tagsFilter($data['tags'], $post);
            }else{
                $post->tags()->detach();
            }
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
        if($request->input('submit') == 'save_continue') return redirect()->route('posts.edit', $post->id)->with('success','Post Updated!!');
        return redirect()->route('posts.index')->with('success','Post updated!!');
    }

    public function destroy($id)
    {
        $post = $this->post->findOrFail($id);
        $this->authorize('delete', $post);      // authorized user for this post (admin,developer and $post->user_id)
        $post->delete();
        return redirect()->back()->with('success','Post Successfully Moved to Trash!!');

    }

    public function emptyTrash(){
        $posts = $this->posts()->onlyTrashed()->get();
        if(count($posts)){
            foreach ($posts as $post){
                $post->forceDelete();
            }
            return redirect()->back()->with('success','Trash Successfully Cleaned');
        }else{
            return redirect()->back()->with('error','Sorry no posts in Trash to Clean');
        }

    }

    public function restoreTrash($id){
        try{
            $post = $this->post->withTrashed()->find($id);
            $this->authorize('update', $post);      // authorized user for this post (admin,developer and $post->user_id)
            $post->restore();
            $post->status = 'draft';
            $post->save();
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('posts.trash')->with('success','Post restored successfully.Please check Drafts to edit the post!! ');
    }

    public function deleteTrash($id){
        try{
            $post = $this->post->withTrashed()->find($id);
            $this->authorize('update', $post);      // authorized user for this post (admin,developer and $post->user_id)
            $post->forceDelete();
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('posts.trash')->with('success','Post permanently deleted.');
    }

    public function info()
    {
        $thisweekdates = collect();
        $labeldate = [];
        $start_date = new Carbon('first day of this month');
        $end_date = new Carbon('last day of this month');
        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $labeldate[]  = $dates = $date->format('Y-m-d');
            $thisweekdates->put( $dates, 0);
        }
        $thisweekposts = $this->post
            ->whereBetween( 'created_at',[Carbon::createFromFormat('Y-m-d',$thisweekdates->keys()->first())->startOfDay(),Carbon::createFromFormat('Y-m-d',$thisweekdates->keys()->last())->endOfDay()])
            ->groupBy( 'date' )
            ->orderBy( 'date' )
            ->get( [
                DB::raw( 'DATE( created_at ) as date' ),
                DB::raw( 'SUM(view_count) as "count"' )
            ] )
            ->pluck( 'count', 'date' )->all();

        $thisweekViewCount = $thisweekdates->merge( $thisweekposts );
        $thisweekcounts = [];
        foreach($thisweekViewCount as $count){
            $thisweekcounts[] = $count;
        }
        return ['daily_post_view_count' =>  $thisweekcounts, 'label'=>$labeldate];
    }

    // create new tags and/or sync existing tags to post
    public function tagsFilter($inputtags, $post)
    {
        $toattach = [];
        $tocreate = [];
        foreach ($inputtags as $req_tag) {
            if ($tag = $this->tag->where('name', $req_tag)->first()) {
                $toattach[] = $tag->id;
            } else {
                $tocreate[] = [
                    'name'=>$req_tag,
                ];
            }
        }
        if (!empty($toattach)) {
            $post->tags()->sync($toattach);
        }
        if (!empty($tocreate)) {
            $post->tags()->createMany($tocreate);
        }
        return false;
    }
    // Check role of auth user and apply post filter admin and developer can only view all users post
    public function posts()
    {
        $user = auth()->user();
        if($user->hasRole(['admin','moderator','developer'])){
            return $this->post;
        }else{
            return $user->posts();
        }
    }

    public function checkAllPosts()
    {
        $allUsers = User::all()->pluck('id')->toArray();
        $posts = $this->post->whereNotIn('user_id', $allUsers)->get();
        foreach ($posts as $post){
            $post->user_id = 1;
            $post->save();
        }
        return 'Posts Successfully Checked';

    }
}
