<?php

namespace App\Http\Controllers\Backend;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function index()
    {
        $comments= $this->comment->latest()->paginate(20);
        return view('backend.comments.index', compact('comments'));
    }

    public function show($id)
    {
        $comment = $this->comment->findOrFail($id);
        return view('backend.comments.show', compact('comment'));
    }

    public function approveComment($id)
    {
        $comment = $this->comment->findOrFail($id);
        $comment->approved = true;
        $comment->save();
        return redirect()->back()->with('success', 'Comment Successfully Approved!!');
    }

    public function suspendComment($id)
    {
        $comment = $this->comment->findOrFail($id);
        $comment->approved = false;
        $comment->save();
        return redirect()->back()->with('success', 'Comment Successfully Banned!!');
    }

    /**
     * Deletes a comment.
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete-comment', $comment);
        $comment->delete();
        return redirect()->route('comments.index')->with('success', 'Comment Successfully Deleted!!');
    }
}
