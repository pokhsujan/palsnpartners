<?php

namespace App\Http\Controllers\Backend;

use App\Adspace;
use App\Http\Controllers\Controller;
use App\Widget;
use Illuminate\Http\Request;

class WidgetsController extends Controller
{
    public function __construct(Widget $widget, Adspace $adspace){
        $this->widget = $widget;
        $this->adspace = $adspace;
    }
    public function index()
    {
        return view('backend.widgets.list');
    }

    public function edit(Request $request)
    {
        $adspaces = $this->adspace->whereNotIn('ad_position', array_keys(config('site.adpos')))->get();
        $widget_type = $request->input('widget_type');
        $widgets_list = config('site.sidebars');
        if(!array_key_exists($widget_type,$widgets_list)) return redirect()->back()->with('error',"Invalid Sidebar");
        $widgets = $this->widget->where('sidebar_type',$widget_type)->orderBy('id', 'ASC')->get();
        return view('backend.widgets.index', compact('widget_type','widgets','adspaces'));
    }

    public function store(Request $request)
    {
        try{
            $widgets = $request->input('items');
            $type = $request->input('type');
            $this->widget->where('sidebar_type',$type)->delete();
            if(!is_null($widgets)){
                $this->widget->insert($widgets);
            }
            return config('site.sidebars.'.$type). " Widget Saved Successfully!";
        }catch (\Exception $e){
            return $e->getMessage();
        }

    }
}
