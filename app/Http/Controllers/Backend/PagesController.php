<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Http\Controllers\Requests\PageRequest;
use App\Widget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use Str;
use DB;

class PagesController extends Controller
{

    protected $page;

    public function __construct(Page $page, Widget $widget)
    {
        $this->page = $page;
        $this->widget = $widget;
    }

    //index page
    public function index()
    {
        $pages = $this->page->latest()->paginate(20);
        return view('backend.pages.index', compact('pages'));
    }

    //create page for page
    public function create()
    {
        return view('backend.pages.create');
    }

    //store new page
    public function store(PageRequest $request)
    {
        $page = $this->page->create($request->all());
        $metas['meta_keywords'] = $request->input('meta_keywords');
        $metas['meta_description'] = $request->input('meta_description');
        $page->syncMeta($metas);
        if ($request->submit == 'save_continue') {
            return redirect()->route('pages.edit', $page->id)->with('success', 'Page Created Successfully');
        }
        return redirect()->route('pages.index')->with('success', 'Page Created Successfully');
    }

    //directing to edit page of pagecontroller
    public function edit($id)
    {
        $page = $this->page->findorFail($id);
        if ($page->template == 'home-page') {
            $categories = Category::has('posts')->pluck('name', 'id')->toArray();
            return view('backend.pages.homepage', compact('categories', 'page'));
        }
        return view('backend.pages.edit', compact('page'));
    }

    //updating the page
    public function update(PageRequest $request, $id)
    {
        $page = $this->page->findorFail($id);
        $page->update($request->all());
        $page->syncMeta($request->except(['_method', '_token', 'title', 'slug', 'subtitle', 'content', 'featured_image', 'template', 'submit']));
        if ($request->submit == 'save_continue') {
            return redirect()->route('pages.edit', $id)->with('success', 'Page Updated Successfully');
        }
        return redirect()->route('pages.index')->with('success', 'Page Updated Successfully');
    }

    public function storeHomepage(Request $request)
    {
        $rules = [
            'title' => 'required',
        ];
        for ($i = 1; $i <= config('site.home_cat_sections_count', 4); $i++) {
            $rules['sec_' . $i . '_ad_position'] = 'required_if:sec_' . $i . '_ad_position, 1';
            $rules['sec_' . $i . '_category_id'] = 'int|nullable|required_if:sec_' . $i . '_enable, 1';
            $rules['sec_' . $i . '_posts_per_page'] = 'int|nullable|required_if:sec_' . $i . '_enable, 1';
        }
        $this->validate($request, $rules);
        $page = $this->page->whereSlug('homepage')->firstOrFail();
        $metas = $request->except(['title', '_token', 'submit']);
        $page->title = $request->input('title');
        $page->save();
        $page->syncMeta($metas);
        if ($request->submit == 'save_continue') {
            return redirect()->route('pages.edit', $page->id)->with('success', 'Page Updated Successfully');
        }
        return redirect()->route('pages.index')->with('success', 'Page Updated Successfully');
    }

    public function destroy($id)
    {
        $page = $this->page->findorFail($id);
        if ($page->slug == 'homepage') {
            return redirect()->back()->with('error', 'Sorry Homepage cannot be deleted.');
        }
        $page->delete();
        return redirect()->back();
    }
}
