<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Message;

class MessagesController extends Controller
{
    protected $message;
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function index()
    {
        $messages= $this->message->latest()->paginate(20);
        return view('backend.messages.index', compact('messages'));
    }

    public function show($id)
    {
        $message = $this->message->findOrFail($id);
        return view('backend.messages.show', compact('message'));
    }
}
