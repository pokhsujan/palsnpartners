<?php

namespace App\Http\Controllers\Backend;

use App\Events\MailchimpSubscribe;
use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubscribersController extends Controller
{

    protected $subscriber;
    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function index()
    {
        $subscribers = $this->subscriber->paginate();
        return view('backend.subscribers.index',compact('subscribers'));
    }

    public function syncMailchimp()
    {
    }

    public function export($type)
    {
        if($type == 'csv'){
            return $this->csvExport();
        }
        return redirect()->back()->with('error', 'No Export Type defined.');
    }

    public function csvExport()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $subscribers = Subscriber::get();
        $columns = array('Name', 'Email', 'Subscribed On');

        $callback = function() use ($subscribers, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($subscribers as $subscriber) {
                fputcsv($file, array($subscriber->full_name, $subscriber->email, $subscriber->created_at));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    public function destroy($id){
        $subscriber=$this->subscriber->findorFail($id);
        $subscriber->delete();
        return redirect()->back()->with('success','Subscriber Successfully Deleted!');
    }


}
