<?php

namespace App\Http\Controllers\Backend;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    protected $permission, $role;
    public function __construct(Permission $permission, Role $role)
    {
        $this->permission = $permission;
        $this->role = $role;
    }

    public function index()
    {
        $permissions = $this->permission->all();
        $roles = $this->role->paginate(20);
        return view('backend.roles.index',compact('roles','permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'display_name'=>'required|string|max:255',
            'permission' =>'required'
        ]); ;
        try{
            $role = $this->role->create($request->only(['name', 'display_name','description']));
            $role->perms()->sync($request->input('permission'));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('roles.index')->with('success','Role Successfully Added!');
    }

    public function edit($id)
    {
        $permissions = $this->permission->all();
        $role = $this->role->findorFail($id);
        return view('backend.roles.edit', compact('role','permissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'display_name'=>'required|string|max:255',
            'permission' =>'required'
        ]);
        try{
            $role = $this->role->findorFail($id);
            $role->update($request->only(['display_name','description']));
            $role->perms()->sync($request->input('permission'));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('roles.index')->with('success','Role Successfully Updated!');
    }

}
