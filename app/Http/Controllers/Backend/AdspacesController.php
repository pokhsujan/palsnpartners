<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Adspace;

class AdSpacesController extends Controller
{
    protected $adspace;

    public function __construct(Adspace $adspace){
        $this->adspace = $adspace;
    }

    //listing all adspaces
    public function index(){
        $adspaces=$this->adspace->orderBy('created_at')->paginate(20);
        return view('backend.adspaces.index',compact('adspaces'));
    }

    //storing adspaces
    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
        ]);
        try{
            $this->adspace->create($request->only('slug', 'name', 'description'));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('adspaces.index')->with('success','Ad Space Successfully updated!');
    }

    //delete fcuntion for adspaces
    public function destroy($id){
        try{
            $adspace=$this->adspace->findorFail($id);
            $adspace->delete();
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->back()->with('success','Ad Space Successfully Deleted!');
    }
    //edit page for adspaces
    public function edit($id){
        $adspace=$this->adspace->findOrFail($id);
        return view('backend.adspaces.edit',compact('adspace'));
    }

    //updating adspaces
    public function update(Request $request,$id){
        $this->validate($request,[
            'name' => 'required|string|max:255',
        ]);
        try{
            $adspace = $this->adspace->findOrFail($id);
            $adspace->update($request->only('name', 'description'));
        }catch (\Exception $exception){
            dd($exception);
        }
        return redirect()->route('adspaces.index')->with('success','Ad Space Successfully updated!');
    } 
}
