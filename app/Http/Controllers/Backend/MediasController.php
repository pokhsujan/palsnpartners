<?php
namespace App\Http\Controllers\Backend;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManager;

class MediasController extends Controller
{
    protected $media;
    private $per_page;
    public function __construct(Media $media)
    {
        $this->media = $media;
        $this->per_page = 21;
    }

    public function index()
    {
        $medias = $this->media()->orderBy('created_at','DESC')->paginate($this->per_page);
        return view('backend.medias.index',compact('medias'));
    }

    public function create()
    {
        return view('backend.medias.create');
    }

    public function store(Request $request)
    {
        $photo = $request->all();
        $response = $this->media->upload($photo);
        return $response;
    }

    public function edit($id)
    {
        $media = $this->media->findorFail($id);
        $returnHtml =  view('backend.medias._modal-edit-partial',compact('media'))->render();
        return response()->json(['success'=>true, 'data'=>$returnHtml]);
    }

    public function update(Request $request, $id)
    {
        if($request->ajax()){
            $media = $this->media->findorFail($id);
            $media->update($request->all());
            return response()->json([
                'success' => true,
                'code'  => 200
            ], 200);
        }
        return redirect(404);

    }

    public function lists(Request $request)
    {
        $num_of_media = $request->input('upload_count')?$request->input('upload_count'):$this->per_page;
        $media_type = $request->input('type');
        $list_type = $request->input('list_type');
        $s = $request->input('s');
        $medias = $this->media();
        if($media_type){
            $medias = $this->media()->whereType($media_type);
        }
        if($s){
            $medias = $medias->where(function($medias) use ($s){
                $medias->where('filename','LIKE','%'.$s.'%');
                    $medias->orWhere('original_name','LIKE','%'.$s.'%');
                    $medias->orWhere('description','LIKE','%'.$s.'%');
                    $medias->orWhere('caption','LIKE','%'.$s.'%');
            });
        }
        $medias = $medias->orderBy('created_at','DESC')->paginate($num_of_media);
        $returnHtml = view('backend.medias._index_list', compact('medias'))->render();

        if($list_type && $list_type == 'create'){
            $returnHtml = view('backend.medias._create_list', compact('medias'))->render();
        }
        return response()->json(['success'=>true, 'totalPages'=>$medias->lastPage(),'currentPage' => $medias->currentPage(), 'data'=>$returnHtml]);

    }

    public function browseImage()
    {
        $type = 'image';
        $medias = $this->media()->where('type','image')->orderBy('created_at','DESC')->paginate($this->per_page);
        return view('backend.medias._editor_modal', compact('medias', 'type'));
    }

    public function browseFiles()
    {
        $type = 'file';
        $medias = $this->media()->where('type','file')->orderBy('created_at','DESC')->paginate($this->per_page);
        return view('backend.medias._editor_modal', compact('medias','type'));
    }

    public function show($id)
    {
        $media = $this->media()->findorFail($id);
        $returnHtml =  view('backend.medias._modal-show-partial',compact('media'))->render();
        return response()->json(['success'=>true, 'data'=>$returnHtml]);
    }

    public function destroy($id)
    {
        $response = $this->media()->remove($id);
        return $response;
    }

    /**
     * Create Icon From Original
     */
    public function imageThumb( $photo, $filename, $path )
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo )->resize(200, null, function ( $constraint) {
            $constraint->aspectRatio();
        })->save( $path.'/'  . $filename );

        return $image;
    }

    public function media()
    {
        $user = auth()->user();
        if($user->hasRole(['admin','moderator','developer'])){
            return $this->media;
        }else{
            return $user->medias();
        }
    }
}
