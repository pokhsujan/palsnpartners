<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Profile;
use App\Role;
use App\Social;
use App\User;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;


class SocialAuthController extends Controller
{
    /**
     **_ Redirect the user to the OAuth Provider.
     * _**
     **_ @return Response
     * _**/

    protected $redirectTo = '/dashboard';

    public function __construct()
    {
        $this->middleware('guest', ['except' => ['doLogout', 'activateAccount']]);
    }

    public function redirectToProvider($provider)
    {
        $providerKey = \Config::get('services.' . $provider);
        if (empty($providerKey))
            return redirect('login')->with('error', 'No such provider');

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $providerUser = Socialite::with($provider)->user();
            $authUser = $this->getOrCreateUser($providerUser, $provider);
            $this->guard()->login($authUser);
            return redirect($this->redirectTo);
        } catch (\Exception $e) {
            return redirect('login')->with('message',$e->getMessage());
        }
    }

    public function getOrCreateUser($providerUser, $provider)
    {
        $socialAccount = Social::whereProvider($provider)->whereSocialId($providerUser->getId())->first();
        if ($socialAccount) {
            return $socialAccount->user;
        } else {
            $socialAccount = new Social([
                'provider' => $providerUser->getId(),
                'social_id' => $provider
            ]);
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
//                Log::info('Enter No User');
                $guest = Role::whereName('user')->first();
                $user_data = [
                    'username' => strtolower(preg_replace('/\s+/', '_', $providerUser->name) . mt_rand(10, 100)),
                    'email' => $providerUser->getEmail(),
                    'password' => null,
                    'status' => 1,
                ];
                $profile_data = [
                    'full_name' => $providerUser->getName(),
                    'nickname' => $providerUser->getNickname() ? $providerUser->getNickname() : $providerUser->getName(),
                ];
                $registered_user = User::create($user_data);
                $profile = new Profile($profile_data);
                $registered_user->profile()->save($profile);;
                $registered_user->attachRole($guest);
                $user = $registered_user;
                //registered User
            }
            if(!$user->hasVerifiedEmail()){
                $user->markEmailAsVerified();
            }
            $socialAccount->user()->associate($user);
            $socialAccount->save();
            return $user;
        }
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
