<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;
use Validator;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class SubscribersController extends Controller
{

    protected $subscriber;

    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function create(Request $request)
    {
        $rules = [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:subscribers',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
            'unique' => 'The :attribute is already subscribed.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->to(url()->previous() . '#subscription-form')
                ->withErrors($validator)
                ->withInput();
        }
       try{
           $subscriber = $this->subscriber->create($request->all());
           if (setting('mailchimp_subscription')=='enable' && config('newsletter.apiKey') && config('newsletter.lists.subscribers.id')):
               Newsletter::subscribeOrUpdate($subscriber->email) ;
               $subscriber->subscribed = true;
               $subscriber->save();
           endif;
       }catch (\Exception $e){
            dd($e);
       }
        return redirect()->to(url()->previous() . '#subscription-form')->with('success', 'You have been successfully subscribed!');

    }

    public function unsubscribe(Request $request)
    {
        $subscriber=$this->subscriber->findorFail($request->email);
        $this->subscriber->sync($subscriber);
        return redirect()->back()->with('success','Email Unsubscribed Successfully!');
    }


}
