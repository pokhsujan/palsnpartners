<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use App\Tag;
use App\User;

class UsersController extends Controller
{
    public function __construct(Post $post, Category $category, Tag $tag, User $user){
        $this->post=$post;
        $this->category=$category;
        $this->user=$user;
        $this->tag=$tag;
        $this->theme = config('site.theme', 'kMag');
    }

    public function show($username){
        $user = $this->user->whereUsername($username)->firstorFail();
        $posts = $user->posts()->with('media')->published()->latest('publish_on')->paginate(setting('posts_per_page', config('site.posts_per_page')));
        return view()->first([
            'themes.'.$this->theme.'.archives',
            'themes.'.$this->theme.'.category',
        ], ['posts'=> $posts, 'user'=>$user,'title' => $user->profile->display_name?$user->profile->display_name:$user->username, 'type' => 'user' ]);
    }
}
