<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\Post;

class PagesController extends Controller
{
    protected $post, $category, $page, $guestpost;

    public function __construct(Post $post, Category $category, Page $page)
    {
        $this->post = $post;
        $this->category = $category;
        $this->page = $page;
        $this->theme = config('site.theme', 'kMag');
    }

    public function index()
    {

    }

    public function show($slug)
    {
        $page = $this->page->whereSlug($slug)->firstOrFail();

        if($page->template == 'home-page'){
            return $this->homepage($slug);
        }
        $template = $page->template?'page-'.$page->template:'page';
        return view('themes.'.$this->theme.'.'.$template, compact('page'));
    }

    public function homepage($slug)
    {
        $page = $this->page->whereSlug($slug)->firstOrFail();
        $catPosts = [];
        $section_cat_ids =[];
        $posts_per_page =[];
        for ($i = 1; $i <= config('site.home_cat_sections_count', 4); $i++) {
            if($page->getMeta('sec_' . $i . '_enable')){
                $section_cat_ids[] = $page->getMeta('sec_' . $i . '_category_id');
                $posts_per_page[] =  $page->getMeta('sec_' . $i . '_posts_per_page');
            }
        }
        $categories = $this->category->whereIn('id',$section_cat_ids)->get();
        if(!$categories->isEmpty()){
            $catPosts = $this->category->getPosts( $categories->pluck('id')->toArray(), max($posts_per_page));
        }
        $breaking_news = $this->post->with('categories', 'media')->whereIsBreakingNews(1)->whereStatus('publish')->latest()->take(6)->get();
        $cover_news = $this->post->whereIsSpecialNews(1)->whereStatus('publish')->latest()->take(5)->get();
        $featured_news = $this->post->whereIsFeaturedNews(1)->whereStatus('publish')->latest()->take(4)->get();
        $recent_news = $this->post->with('media', 'user')->latest()->whereNotIn('id',$breaking_news->pluck('id')->toArray()+$cover_news->pluck('id')->toArray()+$featured_news->pluck('id')->toArray())->take(setting('posts_per_page_home', config('site.posts_per_page')))->get();
        return view('themes.'.$this->theme.'.front-page', compact('page','catPosts','categories','breaking_news','special_news','cover_news','featured_news','recent_news'));

    }

}
