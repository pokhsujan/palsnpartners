<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use App\Tag;
use App\User;

class CategoriesController extends Controller
{
    protected $post,$category,$tag,$user;

    public function __construct(Post $post, Category $category, Tag $tag, User $user){
        $this->post=$post;
        $this->category=$category;
        $this->user=$user;
        $this->tag=$tag;
        $this->theme = config('site.theme', 'kMag');
    }

    public function show($slug){
        $category = $this->category->whereSlug($slug)->firstorFail();
        $topPosts = $category->posts()->with('media', 'user', 'user.profile')->published()->latest('publish_on')->orderBy('view_count','desc')->take(5)->get();
        $exclude = $topPosts->pluck('id')->toArray();
        $posts = $category->posts(function ($q){
            $q->with('media', 'user', 'user.profile');
        })->whereNotIn('posts.id', $exclude)->published()->latest('publish_on')->paginate(setting('posts_per_page', config('site.posts_per_page')));
        return view()->first([
            'themes.'.$this->theme.'.archives',
            'themes.'.$this->theme.'.category',
        ], ['posts'=> $posts, 'topPosts'=>$topPosts, 'title' => $category->name, 'type' => 'category' ]);
    }
}
