<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    protected $post, $page, $category, $tag;

    public function __construct(Post $post, Page $page, Category $category, Tag $tag)
    {
        $this->post = $post;
        $this->page = $page;
        $this->category = $category;
        $this->tag = $tag;
    }

    public function index()
    {

        $post = $this->post->published()->orderBy('updated_at', 'desc')->first();
        $page = $this->page->orderBy('updated_at', 'desc')->first();
        $category = $this->category->orderBy('updated_at', 'desc')->first();
        $tag = $this->tag->orderBy('updated_at', 'desc')->first();
        return response()->view('frontend.sitemaps.index', ['post'=>$post,'page'=>$page,'category'=>$category, 'tag' => $tag])->header('Content-Type', 'text/xml');
    }

    public function show($xml_name)
    {
        switch ($xml_name) {
            case 'post':
                $posts = $this->post->all();
                return response()->view('frontend.sitemaps.posts', compact('posts'))->header('Content-Type', 'text/xml');
                break;
            case 'page':
                $pages = $this->page->all();
                return response()->view('frontend.sitemaps.pages', compact('pages'))->header('Content-Type', 'text/xml');
                break;
            case 'category':
                $categories = $this->category->all();
                return response()->view('frontend.sitemaps.categories', compact('categories'))->header('Content-Type', 'text/xml');
                break;
            case 'category':
                $tags = $this->tag->all();
                return response()->view('frontend.sitemaps.tags', compact('tags'))->header('Content-Type', 'text/xml');
                break;
            default:
                return redirect(404);
        }
    }
}
