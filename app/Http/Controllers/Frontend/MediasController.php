<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class MediasController extends Controller
{

    protected $media;
    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    public function imageSizes($size, $filename)
    {
        $media = $this->media->whereFilename($filename)->firstOrFail();
        switch ($size) {
            case 'large':
            case 'medium':
            case 'small':
            case 'thumbnail':
            case 'preview':
                return Image::cache(function ($image) use ($media, $filename, $size) {
                    $image->make(\Storage::path($media->upload_directory.'/'.$filename));
                }, 1000, true)->fit(config('site.image_sizes.' . $size . '.width'), config('site.image_sizes.' . $size . '.height'), function ($constraint) {
                    $constraint->upsize();
                })->encode('jpg', 80)->response();
                break;
            default:
                abort(404);
                break;
        }
    }

    public function videoImageSizes($size, Request $request)
    {
        $url = $request->input('ytb_thumb');
        switch ($size) {
            case 'full':
                return $url;
                break;
            case 'large':
            case 'medium':
            case 'small':
            case 'thumbnail':
            case 'preview':
                return Image::cache(function ($image) use ($url, $size) {
                    $image->make($url);
                }, 1000, true)->fit(config('site.image_sizes.' . $size . '.width'), config('site.image_sizes.' . $size . '.height'), function ($constraint) {
                    $constraint->upsize();
                })->encode('jpg', 80)->response();
                break;
            default:
                abort(404);
                break;
        }
    }
}
