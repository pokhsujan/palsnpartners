<?php

namespace App\Http\Controllers\Frontend;
use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\Post;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $post;
    protected $category;
    protected $page;
    public function __construct(Post $post, Category $category, Page $page)
    {
        $this->post = $post;
        $this->category = $category;
        $this->page = $page;
        $this->theme = config('site.theme', 'kMag');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->page->whereSlug(\setting('homepage'))->first();
        if($page){
            if($page->template == 'home-page'){
                $catPosts = [];
                $section_cat_ids =[];
                $posts_per_page =[];
                for ($i = 1; $i <= config('site.home_cat_sections_count', 4); $i++) {
                    if($page->getMeta('sec_' . $i . '_enable')){
                        $section_cat_ids[] = $page->getMeta('sec_' . $i . '_category_id');
                        $posts_per_page[] =  $page->getMeta('sec_' . $i . '_posts_per_page');
                    }
                }
                $categories = $this->category->whereIn('id',$section_cat_ids)->get();
                if(!$categories->isEmpty()){
                    $catPosts = $this->category->getPosts( $categories->pluck('id')->toArray(), max($posts_per_page));
                }
                $breaking_news = $this->post->with('categories', 'media')->published()->whereIsBreakingNews(1)->whereStatus('publish')->latest()->take(6)->get();
                $cover_news = $this->post->whereIsSpecialNews(1)->published()->latest()->take(5)->get();
                $featured_news = $this->post->whereIsFeaturedNews(1)->published()->latest()->take(4)->get();
                $recent_news = $this->post->with('media', 'user')->published()->latest()->whereNotIn('id',$breaking_news->pluck('id')->toArray()+$cover_news->pluck('id')->toArray()+$featured_news->pluck('id')->toArray())->take(setting('posts_per_page_home', config('site.posts_per_page')))->get();
                return view('themes.'.$this->theme.'.front-page', compact('page','catPosts','categories','breaking_news','special_news','cover_news','featured_news','recent_news'));
            }
            $template = $page->template?'page-'.$page->template:'page';
            return view('themes.'.$this->theme.'.'.$template, compact('page'));
        }
        $recent_news = $this->post->with('media', 'user')->latest()->take(setting('posts_per_page_home', config('site.posts_per_page')))->get();
        return view('themes.'.$this->theme.'.recent-homepage', compact('recent_news'));
    }

}
