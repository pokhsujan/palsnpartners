<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Category;
use App\Events\ViewCount;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    protected $post, $category, $tag, $user;
    public function __construct(Post $post, Category $category, Tag $tag, User $user)
    {
        $this->post = $post;
        $this->category = $category;
        $this->tag = $tag;
        $this->user = $user;
        $this->theme = config('site.theme', 'kMag');
    }


    public function show($slug)
    {
        $post = $this->post->published()->with('media', 'user.profile', 'categories')
            ->whereSlug($slug)
            ->firstorFail();
        \Event::dispatch(new ViewCount($post));
        $categories = $post->categories;
        $cat_ids = $categories->pluck('id')->toArray();
        $related_posts = $this->post->published()->with('media')
            ->whereHas('categories', function ($q) use ($cat_ids) {
                $q->whereIn('categories.id', $cat_ids);
            })
            ->whereNotIn('id', [$post->id])
            ->latest()
            ->take(6)
            ->get();
        $tags = $post->tags;
        return view('themes.kMag.single', compact('post', 'categories', 'tags', 'related_posts'));
    }

    public function search(Request $request)
    {
        $search = $request->input('s');
        $order = $request->has('order') ? $request->input('order') : 'publish_on';
        $posts = $this->post->published()->whereHas('categories', function ($query) use ($search) {
            $query->where('categories.name', 'like', '%' . $search . '%');
        })->orWhere('title', 'LIKE', '%' . $search . '%')->orderBy($order, 'asc')->paginate(setting('posts_per_page', config('site.posts_per_page')));
        return view()->first([
            'themes.' . $this->theme . '.archives',
            'themes.' . $this->theme . '.category',
        ], ['posts' => $posts, 'search' => $search, 'title' => '', 'type' => 'search']);
    }

    public function ratingStore(Request $request)
    {
        try {
            $this->validate($request, [
                'rateable_type' => 'required|string',
                'rateable_id' => 'required|integer|min:1',
                'rating' => 'required|integer'
            ]);
            $model = $request->rateable_type::findOrFail($request->rateable_id);
            return $model->ratingUnique([
                'rating' => $request->rating,
            ], auth()->user());
        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'response_code' => $e->getCode(), 'errors' => $e->getMessage()]);
        }
    }


    public function feeds()
    {
        $posts = $this->post->published()->latest('publish_on')->orderBy('updated_at', 'desc')->take(20)->get();
        return response()->view('frontend.feed', compact('posts'));
    }

    public function iafeeds()
    {
        $posts = $this->post->published()->with('media')->latest('updated_at')->take(20)->get();
        return response()->view('frontend.iafeed', compact('posts'));
    }

    public function pageSections(Request $request)
    {
        $categories = $this->category->pluck('id', 'name')->toArray();
        return view('themes.kMag.page.partials.category-section', compact('categories'));
    }

}

