<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Message;
use App\Subscriber;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Validator;

class MessagesController extends Controller
{
    use ValidatesRequests;
    protected $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }
    /**
     * Creates a new message.
     */
    public function create(Request $request)
    {
        $rules = [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'message' => 'required',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
        ];
        $this->validate($request, $rules, $messages);
        $this->message->create($request->all());
        return redirect()->to(url()->previous())->with('success', 'Thank You for Contacting us ');
    }

}
