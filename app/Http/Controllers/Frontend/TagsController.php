<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\Tag;
use App\User;
class TagsController extends Controller
{
    protected $post,$category,$user,$tag;
    public function __construct(Post $post, Category $category, Tag $tag, User $user){
        $this->post=$post;
        $this->category=$category;
        $this->user=$user;
        $this->tag=$tag;
        $this->theme = config('site.theme', 'kMag');
    }

    public function show($slug){
        $tag = $this->tag->whereSlug($slug)->firstorFail();
        $topPosts = $tag->posts(function ($q){
            $q->with('media', 'user', 'user.profile');
        })->published()->with('media')->latest()->orderBy('view_count','desc')->take(5)->get();
        $exclude = $topPosts->pluck('id')->toArray();
        $posts = $tag->posts()->published()->with('media')->paginate(setting('posts_per_page', config('site.posts_per_page')));
        if(count($exclude)>3){
            $posts = $tag->posts()->published()->with('media')->whereNotIn('posts.id', $exclude)->latest()->paginate(setting('posts_per_page', config('site.posts_per_page')));
        }
        return view()->first([
            'themes.'.$this->theme.'.archives',
            'themes.'.$this->theme.'.category',
        ], ['posts'=> $posts, 'topPosts'=>$topPosts, 'title' => $tag->name, 'type' => 'tag' ]);
    }
}
