<?php

namespace App\Http\Controllers\Installer;

use App\Events\Installer\EnvironmentSaved;
use App\Events\Installer\LaravelInstallerFinished;
use App\Http\Controllers\Controller;
use App\Library\Installer\EnvironmentManager;
use App\Library\Installer\DatabaseManager;
use App\Library\Installer\FinalInstallManager;
use App\Library\Installer\InstalledFileManager;
use App\Library\Installer\PermissionsChecker;
use App\Library\Installer\RequirementsChecker;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;

class InstallerController extends Controller
{

    protected $EnvironmentManager;
     private $databaseManager;
     protected $permissionChecker;
      protected $requirementsChecker;

    public function __construct(EnvironmentManager $environmentManager, DatabaseManager $databaseManager,PermissionsChecker $permissionChecker,RequirementsChecker $requirementsChecker)
    {
        $this->EnvironmentManager = $environmentManager;
        $this->databaseManager = $databaseManager;
        $this->permissions = $permissionChecker;
        $this->requirements = $requirementsChecker;
    }
    /**
     * Display the installer welcome page.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view('installer.welcome');
    }

    /**
     * Migrate and seed the database.
     *
     * @return \Illuminate\View\View
     */
    public function database()
    {
        $response = $this->databaseManager->migrateAndSeed();
        if($response['status'] == 'error'){
            return redirect()->route('Installer::environmentClassic')->with(['message' => $response]);
        }
        return redirect()->route('Installer::final')->with(['message' => $response]);
    }

    /**
     * Display the Environment page.
     *
     * @return \Illuminate\View\View
     */
    public function environmentClassic()
    {
        $envConfig = $this->EnvironmentManager->getEnvContent();

        return view('installer.environment-classic', compact('envConfig'));
    }

    /**
     * Processes the newly saved environment configuration (Classic).
     *
     * @param Request $input
     * @param Redirector $redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveClassic(Request $input, Redirector $redirect)
    {
        $message = $this->EnvironmentManager->saveFileClassic($input);

        event(new EnvironmentSaved($input));

        return $redirect->route('Installer::environmentClassic')
            ->with(['message' => $message]);
    }


    /**
     * Display the permissions check page.
     *
     * @return \Illuminate\View\View
     */
    public function permissions()
    {
        $permissions = $this->permissions->check(
            config('installer.permissions')
        );

        return view('installer.permissions', compact('permissions'));
    }
    /**
     * Display the requirements page.
     *
     * @return \Illuminate\View\View
     */
    public function requirements()
    {
        $phpSupportInfo = $this->requirements->checkPHPversion(
            config('installer.core.minPhpVersion')
        );
        $requirements = $this->requirements->check(
            config('installer.requirements')
        );

        return view('installer.requirements', compact('requirements', 'phpSupportInfo'));
    }
    /**
     * Update installed file and display finished view.
     *
     * @param InstalledFileManager $fileManager
     * @return \Illuminate\View\View
     */
    public function finish(InstalledFileManager $fileManager, FinalInstallManager $finalInstall, EnvironmentManager $environment)
    {
        $finalMessages = $finalInstall->runFinal();
        $finalStatusMessage = $fileManager->update();
        $finalEnvFile = $environment->getEnvContent();

        event(new LaravelInstallerFinished);

        return view('installer.finished', compact('finalMessages', 'finalStatusMessage', 'finalEnvFile'));
    }

}
