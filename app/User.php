<?php

namespace App;

use App\Traits\Comment\Commenter;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Plank\Metable\Metable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use EntrustUserTrait;
    Use Metable;
    use Commenter;

    const ACTIVE = 1;
    const BANNED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'active', 'password', 'remember_token','email_verified_at'
    ];
    protected $appends = ['fullname'];

    public function profile()
    {
       return $this->hasOne(Profile::class);
    }

    public function log()
    {
        return $this->hasMany(Userlog::class);
    }

    public function social()
    {
        return $this->hasMany(Social::class);
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function medias(){
        return $this->hasMany(Media::class);
    }


    public function owns($post)
    {
        return $this->id == $post->user_id;
    }

    public function getFullnameAttribute()
    {
        if($this->profile())
        {
            return ucfirst($this->profile->display_name?$this->profile->display_name:$this->profile->full_name);
        }
        return ucfirst($this->username);
    }

    /**
     * Returns all comments that this user has made.
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class, 'user_id');
    }
}
