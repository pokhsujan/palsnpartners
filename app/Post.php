<?php

namespace App;

use App\Traits\Comment\Commentable;
use App\Traits\Rating\Rateable as Rateable;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Metable\Metable;

class Post extends Model
{
    protected $fillable=['slug','title','subtitle','content','status','format','featured_image','is_featured_news','is_special_news','is_breaking_news','user_id','publish_on'];
    protected $dates = ['publish_on', 'created_at','updated_at','deleted_at'];
    protected $appends = ['tags_list','published_date','excerpt','url', 'formatted_content', 'related_posts','ia_content','video_url', 'video_image'];
    protected $excerptSize = 32;
    use Metable;
    use SoftDeletes;
    use Sluggable;
    use Commentable;
    use Rateable;
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function boot(){
        parent::boot();
    }

    public function scopePublished($query)
    {
        return $query->where('publish_on', '<=', Carbon::now()->toDateTimeString())->where('status', 'publish');
    }

    public function scopePending($query)
    {
        return $query->where('publish_on', '>=', Carbon::now()->toDateTimeString())->where('status', 'publish');
    }

    public function scopeDraft($query)
    {
        return $query->where('status', 'draft');
    }

    public function scopeBreaking($query)
    {
        return $query->where('is_breaking_news', true);
    }

    public function scopeFeatured($query)
    {
        return $query->where('is_featured_news', true);
    }

    public function scopeSpecial($query)
    {
        return $query->where('is_special_news', true);
    }

    public function scopeVideo($query)
    {
        return $query->where('format', 'video');
    }

    public function user()
    {

        return $this->belongsTo(User::class,'user_id');
    }

    public function media()
    {
        return $this->hasOne(Media::class,'id','featured_image');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'post_category');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag');
    }

    public function getPublishedDateAttribute()
    {
        return $this->publish_on->format(setting('date_format', 'l, d M Y'));
    }

    public function getExcerptAttribute() {
        $content = explode(' ', strip_tags($this->content), $this->excerptSize);
        if (count($content)>=$this->excerptSize) {
            array_pop($content);
            $content = implode(" ",$content).' ...';
        } else {
            $content = implode(" ",$content);
        }
        $content = preg_replace('/<[^>]*>/', '', $content);
        $content = preg_replace('~(?:https?://)?(?:www.)?(?:youtube.com|youtu.be)/(?:watch\?v=)?([^\s]+)~', '', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $content);
        return $content;
    }


    public function setExcerptSize($limit)
    {
        $this->excerptSize = $limit;
        return $this;
    }

    public function getVideoImageAttribute()
    {
        $content = preg_replace('/<[^>]*>/', '', $this->content);
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $content, $video_url);
        if($video_url){
            return 'https://img.youtube.com/vi/'.$video_url[1].'/maxresdefault.jpg';
        }
        return null;
    }

    public function getVideoUrlAttribute()
    {
        $content = preg_replace('/<[^>]*>/', '', $this->content);
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $content, $video_url);
        if($video_url){
            return $video_url[1];
        }
        return null;
    }

    public function hahgetUrlAttribute()
    {
        return route('posts.show',$this->slug);
    }

    public function getformattedContentAttribute()
    {
        $content = $this->content;
        $content = preg_replace(
            "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            "<div class=\"video-wrapper embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" width=\"100 % \" height=\"400\"src=\"//www.youtube.com/embed/$2\" frameborder=\"0\" allowfullscreen></iframe></div>",
            $content);
        return $content;
    }

    public function getRelatedPostsAttribute()
    {
        $categories= $this->categories;
        return $this->whereHas('categories', function ($q) use ($categories) {
            $q->whereIn('categories.id', $categories->pluck('id')->toArray());
        })
            ->whereNotIn('id', [$this->id])
            ->orderByRaw("RAND()")
            ->take(3)
            ->get();
    }

    public function getIaContentAttribute()
    {
        $content = $this->content;
        $IAContent = strip_tags($content,'<img>');
        $IAContent = nl2p($IAContent);
        $pattern = '/(<img([^>]*)>)/i';
        $replacement = '<figure data-feedback="fb:likes, fb:comments">$1</figure>';
        $IAContent = preg_replace( $pattern, $replacement, $IAContent );
        $content = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<figure class=\"op-interactive\"><iframe width=\"100%\" height=\"200\" src=\"//www.youtube.com/embed/$2\"></iframe></figure>",$IAContent);
        $contentWithFixedPTags =  preg_replace_callback('/<p>((?:.(?!p>))*?)(<a[^>]*>)?\s*(<figure[^>]+>)(<\/a>)?(.*?)<\/p>/is', function($matches)
        {
            $image = $matches[2] . $matches[3] . $matches[4];
            $content = trim( $matches[1] . $matches[5] );
            return $image . $content;
        }, $content);

        // On large strings, this regular expression fails to execute, returning NULL
        return is_null($contentWithFixedPTags) ? $content : $contentWithFixedPTags;
    }
}
