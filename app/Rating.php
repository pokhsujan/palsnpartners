<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['rating', 'rateable_id' , 'rateable_type' , 'user_id'];

    /**
     * @return mixed
     */
    public function rateable()
    {
        return $this->morphTo();
    }

    /**
     * Rating belongs to a user.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * @param Model $ratingable
     * @param $data
     * @param Model $author
     *
     * @return static
     */
    public function createRating(Model $rateable, $data, Model $user)
    {
        $rating = new static();
        $rating->fill(array_merge($data, [
            'user_id' => $user->id,
        ]));
        $rateable->ratings()->save($rating);
        return $rating;
    }
    /**
     * @param Model $ratingable
     * @param $data
     * @param Model $author
     *
     * @return static
     */
    public function createUniqueRating(Model $rateable, $data, Model $user)
    {
        $rating = [
            'user_id' => $user->id,
            "rateable_id" => $rateable->id,
            "rateable_type" => get_class($rateable),
        ];
        return Rating::updateOrCreate($rating, $data);
    }
    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function updateRating($id, $data)
    {
        $rating = static::find($id);
        $rating->update($data);
        return $rating;
    }
    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRating($id)
    {
        return static::find($id)->delete();
    }

}
