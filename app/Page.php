<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;

class Page extends Model
{
    protected $fillable=['slug','title','subtitle','content','template','featured_image'];
    use Metable;
    use Sluggable;
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function media()
    {
        return $this->hasOne(Media::class,'id','featured_image');
    }

}
