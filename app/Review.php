<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillables = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
