<?php

return [
    'theme' => 'kMag',
    'site_name' => 'K Mag',
    'posts_per_page' => 12,
    'home_cat_sections_count' => 4,
    'image_sizes' =>[
        'preview'=>['width' => 200, 'height' => 150],
        'thumbnail'=>['width' => 150,'height' => 112],
        'small'=>['width' => 200, 'height' => 135],
        'medium'=>['width' => 400,'height' => 250],
        'large'=>['width' => 700, 'height' => 500],
    ],
    'menupositions' => [
        'top_menu' => 'Top Menu',
        'main_menu' => 'Main Menu',
        'footer_menu1' => 'Footer Menu 1',
        'footer_menu2' => 'Footer Menu 2',
        'footer_menu3' => 'Footer Menu 3',
        'footer_menu4' => 'Footer Menu 4',
    ],
    'page_templates' =>[
        'default' => 'Default',
        'full-width' => 'Full Width',
        'home-page' => 'Home Page',
        'contact' => 'Contact Us',
    ],
    'social_urls' => [
        'facebook' => 'Facebook',
        'youtube' => 'Youtube',
        'twitter' =>'Twitter',
        'instagram' => 'Instagram',
        'gplus' => 'Google Plus'
    ],
    'adpos' => [
        'ad-header'=>'Header Ad (Large)',
        'ad-header-middle' => 'Header Middle (Large)',
        'ad-header-bottom' => 'Header Bottom (Large)',
        'archive-content-top' => 'Archive Title Top  (Large)',
        'archive-content-middle' => 'Archive Content Middle (Large)',
        'archive-content-bottom' => 'Archive Content Bottom (Large)',
        'single-bottom' => 'Single Page Bottom (Large)',
        'single-bottom-1' => 'Single Page Bottom II (Large)',
        'single-bottom-2' => 'Single Page Bottom II (Large)',
        'single-bottom-3' => 'Single Page Bottom III (Large)',
    ],
    'sidebars' => [
        'default' => 'Default Sidebar',
        'homepage' => 'Home Page Sidebar',
        'single' => 'News Detail Sidebar',
        'category' => 'Categories Page Sidebar',
        'tag' => 'Tags Page Sidebar',
        'search' => 'Search Page Sidebar',
        'user' => 'Users Page Sidebar',
    ],
    'widgets'=>[
        'popular_news' => 'Popular Posts',
        'featured_news' => 'Featured Posts',
        'recent_news' => 'Recent Posts',
        'social_box' => 'Social Box Widget',
        'facebook_likebox' => 'Facebook Likebox Widget',
        'subscriber' => 'Email Subscription Widget'
    ],
];
