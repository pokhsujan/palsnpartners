import Dropzone from 'dropzone';
import swal from 'sweetalert';

var ajaxCall = {
    ajaxSearchResults: function () {
        $.ajax({
            type: "get",
            url: "/admin/media/list",
            data: {page: this.page_num, s: this.search_key, type: this.media_type},
            beforeSend: function () {
                // Codebase.blocks('.media-lists', 'state_loading');
            },
            success: function (response) {
                $("#media-list-modal").html(response.data);
                // Codebase.blocks('.media-lists', 'state_normal');
                $('#media-selected-info').html('<div class="block-content text-primary-dark p-20">Once you select the media you will find add button to add media.</div>');
                if (response.totalPages > response.currentPage) {
                    jQuery('.loadmore-images').attr('page-number', response.currentPage);
                    jQuery('.loadmore-images').show();
                } else {
                    jQuery('.loadmore-images').hide();
                }
            }
        });
    }
}
var uiHelperDropzoneUpload = function () {
    var upload_counter = 0;
    var for_modal = $('#media-upload').attr('data-modal');
    var messageDiv = $('#info-message');
    Dropzone.options.mediaUpload = {
        uploadMultiple: false,
        parallelUploads: 20,
        previewsContainer: false,
        acceptedFiles: '.png,.gif,.jpeg,.jpg,.bmp,.svg,.ico,.doc,.docx,.pdf,.xls,.txt,.mp3,.ogg,.mpga,.mp4,.mpeg',
        error: function (file, response) {
            if (response.message) {
                var message = response.message;
            } else {
                var message = response;
            }
            $('.upload-errors').html(message);
            swal("Error!", message, "error");
        },
        success: function (file, done) {
            upload_counter++;
            if (for_modal == 'false') {
                $.ajax({
                    type: "get",
                    url: "/admin/media/list",
                    data: {upload_count: upload_counter, list_type: 'create'},
                    success: function (response) {
                        $("#media-manager").html(response.data);
                        jQuery('#media-manager').on('click', '.edit-attachment', function (e) {
                            e.preventDefault();
                            var link = $(this);
                            var media_url = $(this).attr('data-edit-url');
                            $.ajax({
                                type: "get",
                                url: media_url,
                                success: function (response) {
                                    link.parent().addClass('selected');
                                    $("#media-frame-content").html(response.data);
                                    $("#edit-attachment-frame").modal('show');
                                    $('#edit-attachment-frame').on('hidden.bs.modal', function () {
                                        link.parent().removeClass('selected');
                                    });

                                }
                            });
                        });
                    }
                });

            }
        },
        totaluploadprogress: function (uploadProgress, totalBytes, totalBytesSent) {
            $('#info-load').show();
            $('#info-load').text('Uploading : ' + Math.round(uploadProgress) + ' %');
        },
        queuecomplete: function () {
            $('#info-load').hide();
            if (for_modal == 'true') {
                $('.dad-tabs a[href="#selectImage"]').tab('show');
                $.ajax({
                    type: "get",
                    url: "/admin/media/list",
                    beforeSend: function () {
                        //Codebase.blocks('.media-lists', 'state_loading');
                    },
                    success: function (response) {
                        $('#media-selected-info').html('<div class="block-content text-primary-dark p-20">Once you select the media you will find add button to add media.</div>');
                        $("#media-list-modal").html(response.data);
                        if (response.totalPages > response.currentPage) {
                            $('.loadmore-images').show();
                            $('.loadmore-images').attr('page-number', (response.data.currentPage))
                        } else {
                            $('.loadmore-images').hide();
                            $('.loadmore-images').removeAttr('page-number');
                        }

                        //Codebase.blocks('.media-lists', 'state_normal');
                    }
                });
            }
        }
    }
}
var uiHelperMediaManagerModal = function () {
    jQuery('body').on('click', '.js-media-manager-button', function () {
        var trigger = $(this);
        var insert_input = '#' + trigger.attr('data-input');
        var insert_id = '#' + trigger.attr('data-hidden');
        var insert_preview = '#' + trigger.attr('data-preview');
        $.ajax({
            type: "get",
            url: "/admin/media/list",
            success: function (response) {
                $('#media-selected-info').html('<div class="block-content text-primary-dark p-20">Once you select the media you will find add button to add media.</div>');
                $("#media-list-modal").html(response.data);
                $("#show-attachment-frame").modal('show');
                if (response.currentPage < response.totalPages) {
                    $('#show-attachment-frame').find('.loadmore-images').show();
                    $('#show-attachment-frame').find('.loadmore-images').attr('page-number', (response.currentPage))
                }
                jQuery('#media-selected-info').on('click', '#set-image', function () {
                    var selector = $('#media-selector img');
                    // media attributes

                    var media_id = selector.attr('data-id');
                    var media_original_src = selector.attr('data-original-url');
                    var media_src = selector.attr('src');
                    $(insert_input).val(media_original_src);
                    $(insert_id).val(media_id);
                    $(insert_preview).attr('src', media_src);
                    $("#show-attachment-frame").modal('hide');
                    $(".remove-featured-image").show();
                    insert_input = '';
                    insert_id = '';
                    insert_preview = '';
                });
            }
        });
    });
}
var uiMediaHelperRemoveFeaturedImage = function () {
    jQuery('.remove-featured-image').on('click', function (e) {
        e.preventDefault();
        var trigger = $(this).closest('.form-group').find('a.js-media-manager-button');
        var insert_input = '#' + trigger.attr('data-input');
        var insert_id = '#' + trigger.attr('data-hidden');
        var insert_preview = '#' + trigger.attr('data-preview');
        $(insert_input).val('');
        $(insert_id).val('');
        $(insert_preview).attr('src', '');
        $(this).hide();
    });
}
var uiHelperMediaManagerSearchModal = function () {
    jQuery('.media-search').on('keyup', '[name="s"]', function () {
        var type = $('.media-search').find('select[name="type"]').val();
        var key = $(this).val();
        var q = {
            media_type: type,
            search_key: key,
        };
        ajaxCall.ajaxSearchResults.call(q);
    });
    jQuery('.media-search').on('change', 'select[name="type"]', function () {
        var key = $('.media-search').find('input[name="s"]').val();
        var type = $(this).val();
        $('#load-more').attr('data-type', type);
        var q = {
            media_type: type,
            search_key: key,
        };
        ajaxCall.ajaxSearchResults.call(q);
    });
}
var uiHelperMediaLoadMore = function () {
    var page_num = parseInt(1);
    jQuery('.loadmore-images').on('click', function (e) {
        e.preventDefault();
        var current = $(this);
        if ($(this).attr('page-number')) {
            page_num = $(this).attr('page-number');
        }
        var type = '';
        if ($(this).attr('data-type')) {
            var type = $(this).attr('data-type');
        }
        var key = $('.media-search [name="s"]').val();
        $.ajax({
            type: "get",
            url: "/admin/media/list",
            beforeSend: function () {
                // Codebase.blocks('#media-list-modal', 'state_loading');
            },
            data: {page: (parseInt(page_num) + 1), s: key, type: type},
            success: function (response) {
                $("#media-list-modal").append(response.data);
                if (response.currentPage < response.totalPages) {
                    page_num = response.currentPage;
                    current.attr('page-number', (response.currentPage))
                } else {
                    current.hide();
                    current.removeAttr('page-number');
                }
            }
        });
    });
}
var uiHelperMediaEditModal = function () {
    jQuery('.attachments').on('click', 'li.attachment', function () {
        jQuery('.attachments').find('li.attachment').each(function () {
            $(this).removeClass('selected');
        });
        $(this).addClass('selected');
        var media_url = $(this).attr('data-edit-url');
        $.ajax({
            type: "get",
            url: media_url,
            success: function (response) {
                $("#media-frame-content").html(response.data);
                $("#edit-attachment-frame").modal('show');
            }
        });
    });
};
var uiHelperMediaShowModal = function () {
    jQuery('#media-list-modal').on('click', 'li.attachment', function () {
        jQuery('#media-list-modal').find('li.attachment').each(function () {
            $(this).removeClass('selected');
        });
        $(this).addClass('selected');
        var media_url = $(this).attr('data-show-url');
        $.ajax({
            type: "get",
            url: media_url,
            beforeSend: function () {
                // Codebase.blocks('#media-selected-info', 'state_loading');
            },
            success: function (response) {
                // Codebase.blocks('#media-selected-info', 'state_normal');
                $("#media-selected-info").html(response.data);
            }
        });
    });
}
var uiHelperMediaEdit = function () {
    jQuery("#edit-attachment-frame").on('shown.bs.modal', function () {
        jQuery('body').find('form#attachment-edit').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    jQuery('.settings-save-status').show();
                    setInterval(function () {
                        jQuery('.settings-save-status').hide();
                    }, 3000);
                },
                error: function (xhr, err) {
                    alert('Error');
                }
            });
        });
    });
}
var uiHelperDeleteMedia = function () {
    jQuery("#edit-attachment-frame").on('shown.bs.modal', function () {
        jQuery('body').find('form#attachment-delete').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    $("#edit-attachment-frame").modal('hide');
                    $('.attachments').find('li.attachment.selected').remove();
                    $('body').find('.media-item.selected').remove();
                },
                error: function (xhr, err) {
                    console.log('Error');
                }
            });
        });
    });
}

uiHelperDropzoneUpload();
uiHelperMediaManagerModal();
uiHelperMediaManagerSearchModal();
uiHelperMediaEditModal();
uiHelperMediaLoadMore();
uiHelperMediaShowModal();
uiHelperMediaEdit();
uiHelperDeleteMedia();
uiMediaHelperRemoveFeaturedImage();
