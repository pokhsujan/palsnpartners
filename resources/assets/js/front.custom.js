//==============================================================
// CUSTOM SCRIPTS
// Author: Lamputer Media
// 2018
// ==============================================================
$ = jQuery;
import ScrollMagic from 'scrollmagic';

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 300) {
        $(".scrolltop").addClass("on");
    } else {
        $(".scrolltop").removeClass("on");
    }
});

$(document).ready(function () {
    $(".nav-trigger").on('click', function () {
        $(".nav-trigger").find('.burger').toggleClass('menu-on');
        $('body').toggleClass('openmenu-overlay');
    });

    if ($(window).width() < 992) {
        navScroll();
    }

    function navScroll() {
        $(".primary-nav").mCustomScrollbar({
            theme: "minimal",
            scrollInertia: 200,
            scrollbarPosition: "outside"
        });
    }

    function navScrollDestroy() {
        $(".primary-nav").mCustomScrollbar("destroy");
    }

    $('.news-slides').slick({
        arrows: false,
        dots: true,
        adaptiveHeight: true
    });

    $('.scrolltop').on('click', function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });


    $(".search-trigger").on('click', function () {
        $('body').addClass('opensearch-overlay');
        $('body').addClass('openoverlay');
    });
    $("body").on('click', '.search-overlay .close-trigger', function () {
        $('body').removeClass('opensearch-overlay');
        $('body').removeClass('openoverlay');
    });

    var controller = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
        triggerElement: '.main-content',
        triggerHook: 0.1
    })
        .on('enter', function () {
            $('body').addClass('nav-fixed');
        })
        .on('leave', function () {
            $('body').removeClass('nav-fixed');
        })
        .addTo(controller);

    /**
     * Facebook Page Plugin fully respnsive
     */
    var app = {};
    app.resizeFBPagePlugin = function () {
        //getting parent box width
        var container_width = (Number($('.facebook-page-share').width()) - Number($('.facebook-page-share').css('padding-left').replace("px", ""))).toFixed(0);
        if (!isNaN(container_width)) {
            $(".fb-page").attr("data-width", container_width);
        }
        if (typeof FB !== 'undefined') {
            FB.XFBML.parse();
        }
    }
    //==============================================================
    // Window Resize
    // ==============================================================
    var resizeTimer; // Set resizeTimer to empty so it resets on page load

    function resizeFunction() {
        app.resizeFBPagePlugin();
        if ($(window).width() < 992) {
            navScroll();
        } else {
            navScrollDestroy();
        }
    }

    $(window).resize(function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizeFunction, 250);
    });

});
