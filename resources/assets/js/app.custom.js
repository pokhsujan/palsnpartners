import datetimepicker from 'pc-bootstrap4-datetimepicker';
import swal from 'sweetalert';
import autoComplete from 'js-autocomplete';
$(function () {
  "use strict";
  /**tooltips and popovers**/
  $('[data-toggle="tooltip"]').tooltip();

  $('[data-toggle="popover"]').popover();

  $(".dad-nav-btn").on('click', function () {
    $("#page-aside").toggleClass("aside-folded");
  });

  $(".dad-nav-btn").on('click', function () {
    $(".dad-nav-btn").toggleClass("toggler-folded");
  });
  /**multilevel**/
  $('.dropdown-menu a.dropdown-toggle').on('click', function () {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');


    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
      $('.dropdown-submenu .show').removeClass("show");
    });
    return false;
  });
  /**offcanvas**/
  $('[data-toggle="offcanvas"],.offcanvas-overlay,.dad-nav-mini-btn,.aside-close').on('click', function () {
    $('.offcanvas-wrapper,.aside-icon-o').toggleClass('show');
  });
  $(".right-bar-btn").on('click', function () {
    $(".right-sidebar").toggleClass("visible");
  });
  /**metis menu**/
  $("#metisMenu").metisMenu({
  });
  $("#metisMenu-demo,#metisMenu-demo2").metisMenu({
  });
  /**carousel**/
  $('.interval-1s').carousel({
    interval: 1000
  });
  $('.interval-2s').carousel({
    interval: 2000
  });
  $('.interval-3s').carousel({
    interval: 3000
  });
  $('.interval-4s').carousel({
    interval: 4000
  });
  //slim scroll
  $('.slim-scroll').slimScroll({
    height: '100%'
  });
  $('.app-scroll').slimScroll({
    height: '100%',
    start: 'bottom'
  });
  $('#page-aside').on('click', function (event) {
    // The event won't be propagated up to the document NODE and
    // therefore delegated events won't be fired
    event.stopPropagation();
  });
  $(window).on("resize", function () {
    var t = $(window).width();
    t > 992 && ($("#page-aside").removeClass("show"), $(".sidebar-md, .sidebar-sm").removeClass("show")), t > 768 && $(".sidebar-sm").removeClass("Show");
  });

  $('.media-scroll').slimScroll({
    height: '650px',
    color: '#ffffff'
  });
  $('#media-list-modal').slimScroll({
    height: '650px',
    color: '#ffffff'
  });
  $('.slimscroll').slimScroll({
    height: '300px',
    color: '#ffffff',
    alwaysVisible: true,
    railVisible: true,
    railColor: '#eaeaea',
    railOpacity: 0.3,
  });
  var options = {
    height : '450px',
    filebrowserImageBrowseUrl: '/admin/media/browse/images',
    filebrowserBrowseUrl: '/admin/media/browse/files',
  };
  // Init inline text editor
  if ($('#js-ckeditor-inline:not(.js-ckeditor-inline-enabled)').length) {
    $('#js-ckeditor-inline').attr('contenteditable','true');
    CKEDITOR.inline('js-ckeditor-inline');

    // Add .js-ckeditor-inline-enabled class to tag it as activated
    $('#js-ckeditor-inline').addClass('js-ckeditor-inline-enabled');
  }

  // Init full text editor
  if ($('#js-ckeditor:not(.js-ckeditor-enabled)').length) {
    CKEDITOR.replace('js-ckeditor',options);

    // Add .js-ckeditor-enabled class to tag it as activated
    $('#js-ckeditor').addClass('js-ckeditor-enabled');
  }


  $(".datepicker").datetimepicker({
     format : 'YYYY-MM-DD HH:mm:ss',
      icons: {
        up: "fa fa-angle-up",
        down: "fa fa-angle-down",
        next: 'fa fa-angle-right',
        previous: 'fa fa-angle-left',
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
      },
      sideBySide:false,
      useCurrent : false,
    });
  $('#start-date').datetimepicker({
    format : 'YYYY-MM-DD',
    icons: {
      up: "fa fa-angle-up",
      down: "fa fa-angle-down",
      next: 'fa fa-angle-right',
      previous: 'fa fa-angle-left',
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
    },
    sideBySide:false,
    useCurrent : false,
  });
  $('#end-date').datetimepicker({
    format : 'YYYY-MM-DD',
    icons: {
      up: "fa fa-angle-up",
      down: "fa fa-angle-down",
      next: 'fa fa-angle-right',
      previous: 'fa fa-angle-left',
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
    },
    sideBySide:false,
    useCurrent: false //Important! See issue #1075
  });
  $("#start-date").on("dp.change", function (e) {
    $('#end-date').data("DateTimePicker").minDate(e.date);
  });
  $("#end-date").on("dp.change", function (e) {
    $('#start-date').data("DateTimePicker").maxDate(e.date);
  });

  new autoComplete({
    selector: '.js-autocomplete',
    minChars:1,
    source:function(term,response){
      $.getJSON('/admin/tags/lists',{q:term},function(data){
        response(data);
      });
    },
    onSelect:function(e,term,item){
      tagsInsert(term);
    }
  });

    //add tag
    $('.tags-select').on('click','.tag-add', function () {
      var tag = $('input[name="select_tags"]').val();
      tagsInsert(tag);
    });
    //removetag
    $('.tags-select').on('click','.remove-tag', function () {
      $(this).closest('li').remove();
    });

    $(window).keydown(function(event){
      if( (event.keyCode == 13)) {
        event.preventDefault();
        return false;
      }
    });

  $('.delete-form').on('click', function(e){
    e.preventDefault();
    var form = $(this);
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
     .then((willDelete) => {
       if (willDelete) {
         setTimeout(function () {
           form.submit();
         }, 500);
       } else {
          return false;
       }
     });
  });

  $( "input.form-control" ).keypress(function(e) {
    $(this).closest( ".form-group" ).removeClass('is-invalid');
  });


});

function tagsInsert(tag) {
  if(tag != ''){
    var vals = [];
    $('.tags-select').find('input[name="tags[]"]').each(function () {
      vals.push($(this).val());
    });
    if(vals.indexOf(tag) === -1){
      $('.js-autocomplete').val('');
      $('#post-tags').append('<li><input type="hidden" name="tags[]" value="'+tag+'"/><span class="text-primary-dark"><a href="javascript:void(0);" class="text-danger remove-tag mr-1"><i class="fa fa-times-circle"></i></a>'+tag+'</span></li>');
    }else{
      $('.js-autocomplete').val('');
    }
  }
}


// Menus
var selector1 = '.category-lists';
addtoMenu(selector1);
var selector2= '.page-lists';
addtoMenu(selector2);
var selector3= '.custom-links';
var menuPosition = $('input[name="menu_type"]').val();
function addtoMenu(selector) {
  $(selector).on('click', '.add-to-menu', function (e) {
    $(selector).find("input[name='menus[]']:checked").each(function () {
      $(this).prop('checked', false);
      $('ol#dd-menu-list').append('<li class="dd-item" data-id="" data-display_name="' + $(this).val() + '" data-menu_link="' + $(this).data('url') + '" data-menu_type="' + menuPosition + '">' +
                                  '<div class="dd-handle">' + $(this).val() + '</div>' +
                                  '<span class="menu-addons pull-right"><a class="button-edit text-primary""><i class="fa fa-edit"></i></a><a class="button-delete text-danger""><i class="fa fa-trash"></i></a></span>'+
                                  '</li>');
      $('#menus').nestable();
    });
  });
}
$(selector3).on('click', '.add-to-menu', function (e) {
  var title=$('input[name="menu_title"]').val();
  var url = $('input[name="menu_link"]').val();
  if(title == '' || url == ''){
    $(selector3).find('.form-group').addClass('has_error');
    $(selector3).find('.help-block').remove();
    $(selector3).find('.form-group').append('<span class="invalid-feedback animated fadeInDown"> <strong>Please Enter some Value</strong></span>');
    return 0;
  }
  $(selector3).find('.form-group').removeClass('has_error');
  $(selector3).find('.help-block').remove();
  $('ol#dd-menu-list').append('<li class="dd-item" data-id="" data-display_name="' + title + '" data-menu_link="' +url + '" data-menu_type="' + menuPosition + '">' +
                              '<div class="dd-handle">' + title + '</div>' +
                              '<span class="menu-addons pull-right"><a class="button-edit text-primary""><i class="fa fa-edit"></i></a><a class="button-delete text-danger""><i class="fa fa-trash"></i></a></span>'+
                              '</li>');
  $(selector3).find('input').val('');
  $('#menus').nestable({
    maxDepth:3,
    collapseBtnHTML:false,
    expandBtnHTML:false,
  });
});

$('#menus').nestable({
  maxDepth:3,
  collapseBtnHTML:false,
  expandBtnHTML:false,
});


$('.menu-edit-block').on('click', '.save-menu', function () {
  var menuItems = $('#menus').nestable('serialize');
  var menuPosition = $('input[name="menu_type"]').val();
  var menuParent = $('input[name="menu_parent_type"]').val();
  var token = $('input[name="_token"]').val();
  $.ajax({
    url: "/admin/menus/store",
    data: {items: menuItems,type:menuPosition,parent_type:menuParent, _token: token},
    method: "POST",
    success: function (response) {
      uiHelperAlert('Success', response, 'success');
    },
    error:function (response) {
      console.log(response);
    }
  });
});

// mousedown prevent nestable click
$('.button-delete').on('mousedown', function(event) {
  event.preventDefault();
  return false;
});
$('#dd-menu-list').on('click', '.button-delete', function(){
  var concern_div = $(this).parent().parent();
  concern_div.remove();
});
$('#dd-menu-list').on('click', '.button-edit', function(){
  var concern_div = $(this).closest('li');
  $('#edit-menu-modal').find('#menu-title').val(concern_div.attr('data-display_name'));
  $('#edit-menu-modal').find('#menu-link').val(concern_div.attr('data-menu_link'));
  $('#edit-menu-modal').modal('show');
  concern_div.addClass('menu-editing');
});


$('#edit-menu-modal').on('hidden.bs.modal', function () {
  var menu_name = $('#edit-menu-modal').find('#menu-title').val();
  var menu_link = $('#edit-menu-modal').find('#menu-link').val();
  $('.dd-list').find('.menu-editing').attr('data-display_name', menu_name);
  $('.dd-list').find('.menu-editing').attr('data-menu_link', menu_link);
  $('.dd-list').find('.menu-editing').find('.dd-handle:first').html(menu_name);
  $('.dd-list').find('.menu-editing').removeClass('menu-editing');
});
var selectedType = $('input[type=radio][name=ad_type]:checked', '.gold-type').val();
$('.gold-featured-image').hide();
$('.gold-content').hide();
$('.gold-url').hide();
if (selectedType == 'image_ad') {
    $('.gold-content').hide();
    $('.gold-featured-image').show();
    $('.gold-url').show();
}else if (selectedType == 'script_ad') {
    $('.gold-featured-image').hide();
    $('.gold-url').hide();
    $('.gold-content').show();
}
$('input[type=radio][name=ad_type]').change(function() {
  if (this.value == 'image_ad') {
    $('.gold-content').hide();
    $('.gold-featured-image').show();
    $('.gold-url').show();
  }else if (this.value == 'script_ad') {
    $('.gold-featured-image').hide();
    $('.gold-url').hide();
    $('.gold-content').show();
  }
});


// Widgets

var widget_selector = '.widgets-lists';
addtoWidget(widget_selector);

var widgetPosition = $('input[name="sidebar_type"]').val();
function addtoWidget(widget_selector) {
  $(widget_selector).on('click', '.add-to-sidebar', function (e) {
    $(widget_selector).find("input[name='widgets[]']:checked").each(function () {
      $(this).prop('checked', false);
      $('ol#dd-widget-list').append('<li class="dd-item" data-widget_id="' +  $(this).data('widget_id') + '" data-title="' + $(this).val() + '"  data-sidebar_type="' + widgetPosition + '">' +
                                  '<div class="dd-handle">' + $(this).val() + '</div>' +
                                  '<span class="widget-addons pull-right"><a class="widget-button-edit text-primary"><i class="fa fa-edit"></i></a><a class="button-delete text-danger""><i class="fa fa-trash"></i></a></span>'+
                                  '</li>');
      $('#widgets').nestable({
        maxDepth:1,
        collapseBtnHTML:false,
        expandBtnHTML:false,
      });
    });

  });
}

$('#widgets').nestable({
  maxDepth:1,
  collapseBtnHTML:false,
  expandBtnHTML:false,
});


$('.widget-edit-block').on('click', '.save-widget', function () {
  var widgetItems = $('#widgets').nestable('serialize');
  var widgetPosition = $('input[name="sidebar_type"]').val();
  var token = $('input[name="_token"]').val();
  $.ajax({
    url: "/admin/widgets/store",
    data: {items: widgetItems,type:widgetPosition, _token: token},
    method: "POST",
    success: function (response) {
      uiHelperAlert('Success', response, 'success');
    },
    error:function (response) {
      console.log(response);
    }
  });
});
$('#dd-widget-list').on('click', '.button-delete', function(){
  var concern_div = $(this).parent().parent();
  concern_div.remove();
});

$('#dd-widget-list').on('click', '.widget-button-edit', function(){
  var concern_div = $(this).closest('li');
  $('#edit-widget-modal').find('#widget-title').val(concern_div.attr('data-title'));
  $('#edit-widget-modal').modal('show');
  concern_div.addClass('widget-editing');
});

$('#edit-widget-modal').on('hidden.bs.modal', function () {
  var widget_title = $('#edit-widget-modal').find('#widget-title').val();
  $('.dd-list').find('.widget-editing').attr('data-title', widget_title);
  $('.dd-list').find('.widget-editing').find('.dd-handle:first').html(widget_title);
  $('.dd-list').find('.widget-editing').removeClass('widget-editing');
});


var chartLinesCon  = jQuery('.js-chartjs-lines');
$.get( "/admin/posts/info", function( data ) {
  var thisMonthViews = data.daily_post_view_count;
  var labels = data.label;
  var chartLinesBarsRadarData = {
    labels: labels,
    datasets: [
      {
        label: 'This Month',
        fill: true,
        backgroundColor: 'rgba(66,165,245,.75)',
        borderColor: 'rgba(66,165,245,1)',
        pointBackgroundColor: 'rgba(66,165,245,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(66,165,245,1)',
        data: thisMonthViews
      }
    ]
  };
  if ( chartLinesCon.length ) {
    chartLines = new Chart(chartLinesCon, { type: 'line', data: chartLinesBarsRadarData });
  }
});

$('#search-box-opener').click(function () {
    $('.in-header').find('.search-posts').toggle();
});
