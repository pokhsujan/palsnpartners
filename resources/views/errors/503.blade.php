<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <title>{{ setting('site_name', config('site.site_name')) }} | Maintenance Break</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('css/'.get_style(setting('site_style', 'default'))) }}"/>
</head>
<body>
<section class="error-module">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="error-post">
                    <h1>We’ll be back soon!</h1>
                    <h2>We’re currently down for maintenance..</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>