<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <title>{{ config('site.site_name') }} | 404 Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
</head>
<body>
    <section class="error-module">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="error-post">
                        <h1>400 NOT FOUND</h1>
                        <h1>Oops.. You just found an error page..</h1>
                        <h2>We are sorry but your request contains bad syntax and cannot be fulfilled..</h2>
                        <p><a class="btn btn-secondary" href="{{ url('/') }}">Back to Home page</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
