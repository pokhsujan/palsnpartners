@extends('auth.layout')
@section('title'){{ __('Your Email Address') }} @endsection
@section('content')
    <div class="col-lg-8 ml-auto mr-auto col-md-10 col-12">
        <div class="bg-white overflow-hidden rounded shadow-sm">
            <div class="row no-margin align-items-center">
                <div class="col-lg-12  col-md-12 no-padding bg-light">
                    <div class="p-4 pb-0 flex pt-4 text-center">
                        <a class="" href="{{ url('/') }}">
                            <img src="{{ setting('logo', 'images/logo.png') }}" alt="{{ setting('site_name',config('app.name')) }}">
                        </a>
                        <h3 class="fs20 font300 lineH-1 mt-20">{{ __('auth.verify_email') }}</h3>
                    </div>
                    <div class="p-4">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('auth.fresh_verification_sent') }}
                            </div>
                        @endif
                        {{ __('auth.check_email_inbox') }}
                        {{ __('auth.did_not_receive_email') }}, <a class="text-danger-active" href="{{ route('verification.resend') }}">{{ __('auth.make_another_request') }}</a>.
                    </div>
                </div>
            </div>
        </div>
        <p class="pt-3 no-margin text-center text-muted">
            @if(setting('footer_copyright'))
                {{ setting('footer_copyright') }}
            @else
                © {{ date('Y') }}. {{ setting('site_name', config('app.name')) }}
            @endif
        </p>
    </div>
@endsection
