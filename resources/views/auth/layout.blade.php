<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') - {{ setting('site_name',config('app.name')) }}</title>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin-style.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('styles')
</head>
<body class="bg-light  pace-done">
    <div class="page-wrapper d-flex flex fullscreen align-items-center" id="page-wrapper">
        <div class="container">
            <div class='row align-self-center'>
                @yield('content')
            </div>
        </div>
    </div><!-- app's main wrapper end -->
    <!-- Common plugins -->
    @stack('modal')
    <script> var CKEDITOR_BASEPATH = '/js/ckeditor/'; </script>
    <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
    @stack('scripts')
    <script>
        function uiHelperAlert(title, message, type){
            swal({
                title: title,
                text: message,
                icon: type,
                timer: 1000,
                showConfirmButton:false
            });
        }
    </script>
    @if (session('error'))
        <script>
            var error_message = "{{ session('error') }}";
            uiHelperAlert('Error', error_message, 'error');
        </script>
    @endif
    @if (session('success'))
        <script>
            var success_message = "{{ session('success') }}";
            uiHelperAlert('Success', success_message, 'success');
        </script>
    @endif
    @if (session('status'))
        <script>
            var success_message = "{{ session('status') }}";
            uiHelperAlert('Success', success_message, 'success');
        </script>
    @endif
    @if (session('warning'))
        <script>
            var success_message = "{{ session('warning') }}";
            uiHelperAlert('Warning', success_message, 'warning');
        </script>
    @endif

</body>
</html>