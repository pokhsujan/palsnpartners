@extends('auth.layout')
@section('title'){{ __('auth.reset_password') }} @endsection
@section('content')
    <div class="col-lg-8 ml-auto mr-auto col-md-10 col-12">
        <div class="bg-white overflow-hidden rounded shadow-sm">
            <div class="row no-margin align-items-center">
                <div class="col-lg-12  col-md-12 no-padding bg-light">
                    <div class="p-4 pb-0 flex pt-4 text-center">
                        <a class="logo-text" href="{{ url('/') }}">
                            <img src="{{ setting('logo', 'images/logo.png') }}" alt="{{ setting('site_name',config('app.name')) }}" class="">
                        </a>
                        <h3 class="fs20 font300 lineH-1 mt-20">{{ setting('site_name',config('app.name')) }} {{ __('auth.reset_password') }}</h3>
                    </div>
                    <div class="p-4">
                        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('auth.reset_password') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('site.email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('auth.send_password_reset_link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <p class="pt-3 no-margin text-center text-muted">
            @if(setting('footer_copyright'))
                {{ setting('footer_copyright') }}
            @else
                © {{ date('Y') }}. {{ setting('site_name', config('app.name')) }}
            @endif
        </p>
    </div>
@endsection
