@extends('auth.layout')
@section('title'){{ __('auth.sign_in') }} @endsection
@section('content')
    <div class="col-lg-8 ml-auto mr-auto col-md-10 col-12">
        <div class="bg-white overflow-hidden rounded shadow-sm">
            <div class="row no-margin align-items-center">
                <div class="col-lg-6  col-md-12 no-padding bg-light">
                    <div class="p-4 pb-0 flex pt-4 text-center">
                        <a class="" href="{{ url('/') }}">
                            <img src="{{ setting('logo', 'images/logo.png') }}" alt="{{ setting('site_name',config('app.name')) }}" class="w100">
                        </a>
                        <h3 class="fs20 font300 lineH-1 mt-20">{{ setting('site_name',config('app.name')) }} {{ __('auth.sign_in') }}</h3>
                    </div>
                    <div class="p-4">
                        @include('backend._macros.session_message')
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('auth.sign_in') }}">
                            @csrf
                            <div class="form-group">
                                <input id="email" type="text"
                                       class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" placeholder="{{ __('auth.username_or_email') }}" value="{{ old('email') }}"
                                       required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="password" type="password"
                                       class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" placeholder="{{ __('auth.password') }}" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <label for="check-remember" class="custom-checkbox mb-3 checkbox-dark d-block">
                                <input id="check-remember" type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class="label-helper"> {{ __('auth.remember_me') }}</span>
                            </label>
                            <button type="submit"
                                    class="btn btn-primary btn-block btn-lg btn-icon">{{ __('auth.sign_in') }}</button>
                        </form>
                        <div class="text-center mt-10">
                            Forget your Password? <a href="{{ route('password.request') }}"
                                                     class="text-primary ml-2 b-b d-inline-block">{{ __('auth.reset_from_here') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="p-4 flex">
                        @if(setting('facebook_login') || setting('twitter_login') || setting('google_login'))
                            <div class="text-center mb-20">
                                <div class="title-sep sep-white mb-2">
                                    <span>{{ __('auth.sign_in_with') }}</span>
                                </div>
                                @if(setting('facebook_login') && config('services.facebook.client_id') && config('services.facebook.client_secret') && config('services.facebook.redirect'))
                                    <a href="{{ route( 'social.redirect', ['provider' => 'facebook'] ) }}"
                                       class="btn btn-facebook btn-outline-primary d-block mb-2"><i
                                            class="fa fa-facebook"></i> {{ __('auth.facebook') }}</a>
                                @endif
                                @if(setting('twitter_login') && config('services.twitter.client_id') && config('services.twitter.client_secret') && config('services.twitter.redirect'))
                                    <a href="{{ route( 'social.redirect', ['provider' => 'twitter'] ) }}"
                                       class="btn btn-twitter btn-outline-primary d-block mb-2"><i
                                            class="fa fa-twitter"></i> {{ __('auth.twitter') }}</a>
                                @endif
                                @if(setting('google_login') && config('services.google.client_id') && config('services.google.client_secret') && config('services.google.redirect'))
                                    <a href="{{ route( 'social.redirect', ['provider' => 'google'] ) }}"
                                       class="btn tn-google-plus btn-outline-danger d-block"><i
                                            class="fa fa-google"></i>
                                        {{ __('auth.google') }}</a>
                                @endif
                            </div>
                            <div class="title-sep sep-white mb-2">
                                <span>{{ __('auth.or') }}</span>
                            </div>
                        @endif
                        <div class="text-center">
                            {{ __('auth.dont_have_account') }} <a href="{{ route('register') }}" class="text-primary ml-2 b-b d-inline-block">{{ __('auth.register') }}</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <p class="pt-3 no-margin text-center text-muted">
            @if(setting('footer_copyright'))
                {{ setting('footer_copyright') }}
            @else
                © {{ date('Y') }}. {{ setting('site_name', config('app.name')) }}
            @endif
        </p>
    </div>
@endsection
