@extends('auth.layout')
@section('title'){{ __('auth.register') }} @endsection
@section('content')
    <div class="col-lg-8 ml-auto mr-auto col-md-10 col-12">
        <div class="bg-white overflow-hidden rounded shadow-sm">
            <div class="row no-margin align-items-center">
                <div class="col-lg-12  col-md-12 no-padding bg-light">
                    <div class="p-4 pb-0 flex pt-4 text-center">
                        <a class="logo-text" href="{{ url('/') }}">
                            <img src="{{ setting('logo', 'images/logo.png') }}" alt="{{ setting('site_name',config('app.name')) }}">
                        </a>
                        <h3 class="fs20 font300 lineH-1 mt-20">{{ setting('site_name',config('app.name')) }} {{ __('auth.register') }}</h3>
                    </div>
                    <div class="p-4">
                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                            @csrf
                            <div class="form-group row {{$errors->has('username') ? 'is-invalid':''}}">
                                {!! Html::decode(Form::label('username', __('site.username').' <span class="required"> * </span>', ['class'=>'col-md-4 col-form-label text-md-right'])) !!}
                                <div class="col-md-6">
                                    {!! Form::text('username',old('username'), array('class' => 'form-control', 'placeholder' => __('site.username'))) !!}
                                    @if ($errors->has('username'))
                                        <div class="invalid-feedback animated fadeInDown">
                                            {{ $errors->first('username') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row {{$errors->has('email') ? 'is-invalid':''}}">
                                {!! Html::decode(Form::label('email', __('site.email').' <span class="required"> * </span>', ['class'=>'col-md-4 col-form-label text-md-right'])) !!}
                                <div class="col-md-6">
                                    {!! Form::text('email',old('email'), array('class' => 'form-control', 'placeholder' => __('site.email'))) !!}
                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback animated fadeInDown">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row {{$errors->has('full_name') ? 'is-invalid':''}}">
                                {!! Html::decode(Form::label('full_name',  __('site.full_name').' <span class="required"> * </span>', ['class'=>'col-md-4 col-form-label text-md-right'])) !!}
                                <div class="col-md-6">
                                    {!! Form::text('full_name',old('username'), array('class' => 'form-control', 'placeholder' =>  __('site.full_name'))) !!}
                                    @if ($errors->has('full_name'))
                                        <div class="invalid-feedback animated fadeInDown">
                                            {{ $errors->first('full_name') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row {{$errors->has('nickname') ? 'is-invalid':''}}">
                                {!! Html::decode(Form::label('nickname',  __('site.nickname').' <span class="required"> * </span>', ['class'=>'col-md-4 col-form-label text-md-right'])) !!}
                                <div class="col-md-6">
                                    {!! Form::text('nickname',old('nickname'), array('class' => 'form-control', 'placeholder' =>  __('site.nickname'))) !!}
                                    @if ($errors->has('nickname'))
                                        <div class="invalid-feedback animated fadeInDown">
                                            {{ $errors->first('nickname') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row {{$errors->has('password') ? 'is-invalid':''}}">
                                {!! Html::decode(Form::label('password',  __('site.password').' <span class="required"> * </span>', ['class'=>'col-md-4 col-form-label text-md-right'])) !!}
                                <div class="col-md-6">
                                    {!! Form::password('password',array('class' => 'form-control', 'placeholder' =>  __('site.password'))) !!}
                                    @if ($errors->has('password'))
                                        <div class="invalid-feedback animated fadeInDown">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{$errors->has('password_confirmation') ? 'is-invalid':''}}">
                                {!! Form::label('password_confirmation', __('site.confirm_password'), ['class'=>'col-md-4 col-form-label text-md-right']) !!}
                                <div class="col-md-6">
                                    {!! Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' =>  __('site.confirm_password'))) !!}
                                    @if ($errors->has('password_confirmation'))
                                        <div class="invalid-feedback animated fadeInDown">
                                            {{ $errors->first('password_confirmation') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
{{--                            <div class="form-group row {{$errors->has('g-recaptcha-response') ? 'is-invalid':''}}">--}}
{{--                                {!! Form::label('g-recaptcha-response',  __('site.captcha'), ['class'=>'col-md-4 col-form-label text-md-right']) !!}--}}
{{--                                <div class="col-md-6">--}}
{{--                                    {!! Captcha::display() !!}--}}
{{--                                    @if ($errors->has('g-recaptcha-response'))--}}
{{--                                        <div class="invalid-feedback animated fadeInDown">--}}
{{--                                            {{ $errors->first('g-recaptcha-response') }}--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('auth.register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="p-4 flex">
                        <div class="text-center mb-20">
                            <div class="text-center">
                                {{ __('auth.already_registered') }} <a href="{{ route('login') }}"
                                                       class="text-primary ml-2 b-b d-inline-block">{{ __('auth.sign_in') }}</a>
                            </div>
                            @if(setting('facebook_login') || setting('twitter_login') || setting('google_login'))
                                <div class="title-sep sep-white mb-2">
                                    <span>{{ __('auth.or') }} {{ __('auth.sign_in_with') }}</span>
                                </div>
                                @if(setting('facebook_login') && config('services.facebook.client_id') && config('services.facebook.client_secret') && config('services.facebook.redirect'))
                                    <a href="{{ route( 'social.redirect', ['provider' => 'facebook'] ) }}"
                                       class="btn btn-facebook btn-outline-primary d-block mb-2"><i
                                            class="fa fa-facebook"></i> {{ __('auth.facebook') }}</a>
                                @endif
                                @if(setting('twitter_login') && config('services.twitter.client_id') && config('services.twitter.client_secret') && config('services.twitter.redirect'))
                                    <a href="{{ route( 'social.redirect', ['provider' => 'twitter'] ) }}"
                                       class="btn btn-twitter btn-outline-primary d-block mb-2"><i
                                            class="fa fa-twitter"></i> {{ __('auth.twitter') }}</a>
                                @endif
                                @if(setting('google_login') && config('services.google.client_id') && config('services.google.client_secret') && config('services.google.redirect'))
                                    <a href="{{ route( 'social.redirect', ['provider' => 'google'] ) }}"
                                       class="btn tn-google-plus btn-outline-danger d-block"><i
                                            class="fa fa-google"></i>
                                        {{ __('auth.google') }}</a>
                                @endif
                            @endif
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <p class="pt-3 no-margin text-center text-muted">
            @if(setting('footer_copyright'))
                {{ setting('footer_copyright') }}
            @else
                © {{ date('Y') }}. {{ setting('site_name', config('app.name')) }}
            @endif
        </p>
    </div>
@endsection
