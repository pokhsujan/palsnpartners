@if(count($errors)>0)
<div class="col-md-12">
  <div class="alert-icon alert alert-danger" role="alert">
    <i class="fa fa-times-circle-o"></i>  Something went wrong. Please check below
  </div>
</div>
@endif
<div class="col-md-9">
    <div class="form-body">
        <div class="form-group {{ $errors->has('title')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('title', __('site.title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('title',old('title'),['class'=>'form-control','placeholder'=> __('site.title')]) !!}
            @if ($errors->has('title'))
                <div class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('title') }}
                </div>
            @endif
        </div>
        <div class="form-group {{ $errors->has('subtitle')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('subtitle', __('site.subtitle').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('subtitle',old('subtitle'),['class'=>'form-control','placeholder'=>__('site.subtitle')]) !!}
            @if ($errors->has('subtitle'))
                <div class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('subtitle') }}
                </div>
            @endif
        </div>
        <div class="form-group {{ $errors->has('content')?' is-invalid':'' }}">
            {!! Form::textarea('content',old('content'),['class'=>'form-control','id'=>'js-ckeditor']) !!}
            <span>{{ __('site.editor_note') }}</span>
            @if ($errors->has('content'))
                <div class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('content') }}
                </div>
            @endif
        </div>

    </div>
</div>
<div class="col-md-3">
    <div class="form-group {{ $errors->has('publish_on')?' is-invalid':'' }}">
      {!!  Html::decode(Form::label('publish_on', __('site.publish_on').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
      <div class="input-group date datepicker">
        {!! Form::text('publish_on',\Carbon\Carbon::now(),['class'=>'form-control','placeholder'=>__('site.publish_on')]) !!}
        <span class="input-group-addon">
                  <button class="btn btn-light" type="button">
                      <i class="fa fa-calendar"></i>
                  </button>
        </span>
      </div>
      <div class="form-text text-muted">{{ __('site.publish_message') }}</div>
      @if ($errors->has('publish_on'))
        <div class="invalid-feedback animated fadeInDown">
          {{ $errors->first('publish_on') }}
        </div>
      @endif
    </div>
    <div class="form-group {{ $errors->has('status')?' is-invalid':'' }}">
        {!! Html::decode(Form::label('status', __('site.post.post_status').'<span class="required"> * </span>', ['class' => 'control-label']))  !!}
        {{ Form::select('status',['publish'=>__('site.published'),'draft'=>__('site.draft')],'',['class'=>'form-control js-select2']) }}
        @if ($errors->has('status'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('status') }}
            </div>
        @endif
    </div>
    <div class="form-group {{ $errors->has('format')?' is-invalid':'' }}">
        {!! Html::decode(Form::label('format', __('site.post.post_format').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
        {{ Form::select('format',['standard'=>__('site.standard'),'video'=>__('site.video')],'',['class'=>'form-control js-select2']) }}
        @if ($errors->has('format'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('format') }}
            </div>
        @endif
    </div>
    @include('backend._macros.media_picker', [
    'media_id' => null,
    'media_url' => null,
    'input_name' => 'featured_image',
    'input_label' => __('site.featured_image'),
    'data_input_id' => 'thumbnail-id',
    'data_input_url' => 'thumbnail-url',
    'data_input_url_name' => 'featured_image_url',
    'data_holder' => 'holder'
    ])
    <div class="form-group mb-20">
        {!! Html::decode(Form::label('categories', __('site.category.categories'), ['class' => 'control-label']))  !!}
        <div class="slimscroll">
            <div class="custom-controls-stacked">
                @foreach($categories as $category)
                    <label class="custom-control custom-checkbox">
                        {{ Form::checkbox('category_id[]',$category->id,'',['class'=>'custom-control-input']) }}
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">{{ $category->name  }}</span>
                    </label>
                    <br>
                    @if(count($category->children))
                        @foreach($category->children as $child)
                            <label class="custom-control custom-checkbox ml-4">
                                {{ Form::checkbox('category_id[]',$child->id,'',['class'=>'custom-control-input']) }}
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">{{ $child->name  }}</span>
                            </label>
                          <br>
                        @endforeach
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="tags-select form-group mb-20">
        {!! Html::decode(Form::label('tags', __('site.tag.tags'), ['class' => 'control-label']))  !!}
        <div class="input-group">
            {!! Form::text('select_tags',old('select_tags'),['class'=>'form-control js-autocomplete','placeholder'=>__('site.tag.select_tags')]) !!}
            <span class="input-group-btn">
                <button class="tag-add btn btn-light" type="button">
                   {{__('site.add')}}
                </button>
            </span>
        </div>
        <ul id="post-tags" class="mt-10 mb-10 fa-ul" role="list"></ul>
    </div>

    <div class="form-group mb-20">
        {!! Html::decode(Form::label('featured_section', __('site.post.featured_sections'), ['class' => 'control-label']))  !!}
        <div class="custom-controls-stacked">

            <label class="custom-control custom-checkbox">
                {{ Form::checkbox('is_breaking_news',1,'',['class'=>'custom-control-input']) }}
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">{{ __('site.post.breaking') }}</span>
            </label>  <br>
            <label class="custom-control custom-checkbox">
                {{ Form::checkbox('is_special_news',1,'',['class'=>'custom-control-input']) }}
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">{{ __('site.post.spotlight') }}</span>
            </label>
            <br>
            <label class="custom-control custom-checkbox">
                {{ Form::checkbox('is_featured_news',1,'',['class'=>'custom-control-input']) }}
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">{{ __('site.post.featured') }}</span>
            </label>
        </div>
    </div>
</div>
