<div class="table-responsive-sm">
    <table class="table table-sm table-vcenter ">
        <thead>
        <tr class="uppercase">
            <th class="text-center">#</th>
            <th>{{ __('site.title') }}</th>
            <th class="d-none d-sm-table-cell">
                <a href="{{ route(\Request::route()->getName(),['order'=>'publish_on', 'orderby'=>$order_by]) }}">{{ __('site.published_on') }} <i class="fa fa-sort"></i></a>
            </th>
            <th class="d-none d-sm-table-cell">
                <a href="{{ route(\Request::route()->getName(),['order'=>'updated_at', 'orderby'=>$order_by]) }}">{{ __('site.updated_at') }} <i class="fa fa-sort"></i></a>
            </th>
            <th>{{ __('site.author') }}</th>
            <th>
                <a href="{{ route(\Request::route()->getName(),['order'=>'view_count', 'orderby'=>$order_by]) }}">{{ __('site.views') }}<i class="fa fa-sort"></i></a>
            </th>
            <th class="text-center">{{ __('site.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <td class="text-center">#</td>
                <td>{{$post->title}}</td>
                <td>{{$post->publish_on->format('F j, Y')}}</td>
                <td>{{$post->updated_at->format('F j, Y')}}</td>
                <td>{{$post->user->username}}</td>
                <td>{{$post->view_count}} <span class="text-lowercase">{{ __('site.views') }}</span></td>
                <td class="text-center">
                    <div class="btn-group">
                        @if($post->status == 'publish')
                            <a href="{{route('posts.show',$post->slug)}}" class="btn btn-sm btn-icon-o btn-light mr-1" target="_blank" data-toggle="tooltip" title="" data-original-title="{{ __('site.preview') }}"><i class="fa fa-eye text-dark" aria-hidden="true"></i></a>
                        @endif
                        <a href="{{route('posts.edit',$post->id)}}" class="btn btn-sm btn-icon-o btn-light mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('site.edit') }}"><i class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                        {!! Form::open(array('route' => ['posts.destroy',$post->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                        {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-square btn-sm btn-alt-danger','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                        {!! Form::close()!!}
                    </div>
                </td>
        @endforeach
        </tbody>
    </table>
</div>
{{ $posts->render() }}
