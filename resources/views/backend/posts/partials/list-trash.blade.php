<table class="table table-sm table-vcenter">
    <thead>
    <tr class="uppercase">
        <th class="text-center">#</th>
        <th>{{ __('site.title') }}</th>
        <th class="d-none d-sm-table-cell">{{ __('site.published_on') }}</th>
        <th class="d-none d-sm-table-cell">{{ __('site.updated_at') }}</th>
        <th class="text-center">{{ __('site.actions') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
        <tr>
            <td class="text-center">#</td>
            <td>{{$post->title}}</td>
            <td>{{$post->created_at->format('F j, Y')}}</td>
            <td>{{$post->updated_at->format('F j, Y')}}</td>
            <td class="text-center">
                <div class="btn-group">
                  <a href="{{route('posts.trash.restore',$post->id)}}" class="btn btn-sm btn-icon-o btn-light mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('site.post.recover_post') }}"><i class="fa fa-recycle text-primary-active" aria-hidden="true"></i></a>
                    {!! Form::open(array('route' => ['posts.trash.delete',$post->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form', 'data-toggle'=>'tooltip','data-original-title'=>__('site.delete_permanently'))) !!}
                    {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-square btn-sm btn-alt-warning','type' => 'submit')) !!}
                    {!! Form::close()!!}
                </div>
            </td>
    @endforeach
    </tbody>
</table>
{{ $posts->render() }}
