<table class="table table-sm table-vcenter">
    <thead>
    <tr class="uppercase">
        <th class="text-center">#</th>
        <th>{{ __('site.title') }}</th>
        <th class="d-none d-sm-table-cell">{{ __('site.published_on') }}</th>
        <th class="d-none d-sm-table-cell">{{ __('site.status') }}</th>
        <th class="d-none d-sm-table-cell">{{ __('site.views') }}</th>
        <th class="text-center">{{ __('site.actions') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
        <tr>
            <td class="text-center">#</td>
            <td>{{$post->title}}
            <td>{{$post->created_at->format('F j, Y')}}</td>
            <td>{{$post->status}}</td>
            <td>{{$post->view_count}} <span class="text-lowercase">{{ __('site.views') }}</span></td>
            <td class="text-center">
                <div class="btn-group">
                    <a href="{{route('posts.show',$post->slug)}}" class="btn btn-sm btn-icon-o btn-light mr-1" target="_blank" data-toggle="tooltip" title="" data-original-title="{{ __('site.preview') }}"><i class="fa fa-eye text-dark" aria-hidden="true"></i></a>
                    <a href="{{route('posts.edit',$post->id)}}" class="btn btn-sm btn-icon-o btn-light mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('site.edit') }}"><i class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                    {!! Form::open(array('route' => ['posts.destroy',$post->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                    {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-square btn-sm btn-alt-danger','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                    {!! Form::close()!!}
                </div>
            </td>
    @endforeach
    </tbody>
</table>
{{ $posts->appends(request()->query())->links() }}
