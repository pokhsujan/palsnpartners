@extends('backend.layout')
@section('title') {{ __('site.post.edit_post') }}@endsection
@push('styles')

@endpush
@section('content')
  {!! Form::open(array('route' => ['posts.update',$post->id], 'method' => 'PATCH', 'role' => 'form','id' => 'post-store')) !!}
      <div class="page-subheader mb-3">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-7">
              <div class="list">
                <i class="icon-papers rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.post.posts') }} - {{ __('site.post.edit_post') }} </span>
              </div>
            </div>
            <div class="col-md-5">
              <div class="float-right">
                <a href="{{route('posts.show',$post->slug)}}" class="btn btn-outline-primary btn-sm" target="_blank"  data-toggle="tooltip" title="" data-original-title="{{ __('site.preview') }}"><i class="fa fa-eye text-dark" aria-hidden="true"></i> {{ __('site.preview') }} </a>
                <a class="btn btn-outline-primary btn-sm" href="{{ route('posts.index') }}">
                  <i class="fa fa-angle-left"></i> {{ __('site.post.back_to_published_posts_list') }}
                </a>
                <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i> {{ __('site.save') }}
                </button>
                <button type="submit" name="submit" class="btn btn-outline-success btn-sm"
                        value="save_continue"><i class="fa fa-check-circle"></i> {{ __('site.save_continue') }}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="page-content d-flex flex">
        <div class="container-fluid">
          <div class="row">
            @include('backend.posts.partials.form-edit')
          </div>
        </div>
      </div>
      <div class="p-3 bg-light border1 mb-4 clearfix">
        <div class="float-right">
          <a class="btn  btn-outline-primary btn-sm" href="{{ route('posts.index') }}">
            <i class="fa fa-angle-left"></i> {{ __('site.post.back_to_published_posts_list') }}
          </a>
          <button type="submit" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i> {{ __('site.save') }}
          </button>
          <button type="submit" name="submit" class="btn btn-outline-success btn-sm"
                  value="save_continue"><i class="fa fa-check-circle"></i> {{ __('site.save_continue') }}
          </button>
        </div>
      </div>
  {!! Form::close() !!}
@endsection
@push('modal')
  @include('backend.medias._modal-media-list')
@endpush
@push('scripts')

@endpush
