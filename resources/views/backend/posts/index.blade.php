@extends('backend.layout')
@section('title') {{ __('site.post.published_posts') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <div class="list">
                        <i class="fa fa-file-text-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m"> {{ __('site.post.posts') }} - {{ __('site.post.published_posts') }}</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="search-horizontal search-posts">
                        {!! Form::open(array('route' => ['posts.search.admin'], 'method' => 'GET', 'role' => 'form')) !!}
                        <div class="input-group">
                            <input name="s" type="text" class="form-control" id="s" placeholder="{{__('site.post.search_posts')}}">
                            <button type="submit" class="btn btn-light"><i class="fa fa-search"></i></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="btn-group float-right">
                        <a class="btn btn-light" href="{{ route('posts.create') }}"><i class="fa fa-plus"></i> {{__('site.add_new')}}</a>
                        <a href="{{ route('posts.pending') }}" class="btn btn-light" title="{{__('site.pending')}}"><i class="fa fa-clock-o"></i> {{__('site.pending')}}</a>
                        <a href="{{ route('posts.draft') }}" class="btn btn-light" title="{{__('site.draft')}}"><i class="fa fa-pencil-square-o"></i> {{__('site.draft')}}</a>
                        <a href="{{ route('posts.trash') }}" class="btn btn-light" title="{{__('site.trash')}}"><i class="fa fa-trash"></i> {{__('site.trash')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex flex-column mb-30">
        @include('backend.posts.partials.list')
    </div>
@endsection
