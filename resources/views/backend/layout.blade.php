<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') - {{ setting('site_name',config('app.name')) }}</title>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin-style.css') }}" rel="stylesheet">
   <meta name="csrf-token" value="{{ csrf_token() }}">
    @stack('styles')
</head>
<body>
<div class="in-wrapper" id="in-app">
    {{ Form::open(array('url' => '/logout','id'=>'logout-form', 'style'=>'display:none;', 'method'=>'post')) }}
    {{ Form::submit('', ['class' => 'btn']) }}
    {{ Form::close() }}
    @include('backend.sidebar')
    <main class="content">
        @include('backend.header')
        @yield('content')
    </main><!-- page content end-->
</div><!-- app's main wrapper end -->
<!-- Common plugins -->
@stack('modal')
<script> var CKEDITOR_BASEPATH = '/js/ckeditor/'; </script>
<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
@stack('scripts')
<script>
  function uiHelperAlert(title, message, type){
    swal({
      title: title,
      text: message,
      icon: type,
      timer: 5000,
      showConfirmButton:false
    });
  }
</script>
@if (session('error'))
  <script>
    var error_message = "{{ session('error') }}";
    uiHelperAlert('Error', error_message, 'error');
  </script>
@endif
@if (session('success'))
  <script>
    var success_message = "{{ session('success') }}";
    uiHelperAlert('Success', success_message, 'success');
  </script>
@endif
@if (session('status'))
  <script>
    var success_message = "{{ session('status') }}";
    uiHelperAlert('Success', success_message, 'success');
  </script>
@endif
@if (session('warning'))
  <script>
    var success_message = "{{ session('warning') }}";
    uiHelperAlert('Warning', success_message, 'warning');
  </script>
@endif

</body>
</html>
