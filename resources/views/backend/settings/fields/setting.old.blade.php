<div class="tab-pane active show" id="btabs-general" role="tabpanel">
    <h4 class="font-w400">General Site Settings</h4>

    <div class="form-body">
        <div class="form-group">
            {!!  Html::decode(Form::label('site_name', 'Site Name <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('site_name',setting('site_name'),['class'=>'form-control','placeholder'=>'Site Name']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('site_tagline', 'Site tagline <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('site_tagline',setting('site_tagline'),['class'=>'form-control','placeholder'=>'Site tagline']) !!}
        </div>

        @include('backend._macros.media_picker', [
            'media_id' => null,
            'media_url' => setting('logo'),
            'input_name' => 'logo',
            'input_label' => 'Site Logo',
            'data_input_id' => 'logo-id',
            'data_input_url' => 'logo',
            'data_input_url_name' => 'logo',
            'data_holder' => 'logo-holder'
        ])

        @include('backend._macros.media_picker', [
            'media_id' => null,
            'media_url' => setting('favicon'),
            'input_name' => 'favicon',
            'input_label' => 'Site Favicon',
            'data_input_id' => 'favicon-id',
            'data_input_url' => 'favicon',
            'data_input_url_name' => 'favicon',
            'data_holder' => 'favicon-holder'
        ])
        <div class="form-group">
            {!!  Html::decode(Form::label('enable_top_bar', 'Enable Top Bar', ['class' => 'control-label']))  !!}
            {{ Form::select('enable_top_bar',['enable'=>'Yes, Enable Top Bar','disable'=>'No, Disable Top Bar'],setting('enable_top_bar'),['class'=>'form-control js-select2']) }}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('top_date', 'Enable Top Date', ['class' => 'control-label']))  !!}
            {{ Form::select('top_date',['enable'=>'Yes, Enable Top Date','disable'=>'No, Disable Top Date'],setting('top_date'),['class'=>'form-control js-select2']) }}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('mailchimp_subscription', 'Enable Mailchimp Subscription', ['class' => 'control-label']))  !!}
            {{ Form::select('mailchimp_subscription',['enable'=>'Yes, Enable Mailchimp Subscription','disable'=>'No, Disable Mailchimp Subscription'],setting('mailchimp_subscription'),['class'=>'form-control js-select2']) }}
            @if(setting('mailchimp_subscription')=='enable' && !config('newsletter.apiKey') && !config('newsletter.lists.subscribers.id'))
                <small class="form-text text-muted text-danger">Please Add MAILCHIMP_APIKEY=your mailchimp api Key & MAILCHIMP_LIST_ID=Your Mailchimp List ID in your .env file. </small>
            @endif
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('head_tags', 'Head Insert Tags', ['class' => 'control-label']))  !!}
            {!! Form::textarea('head_tags', setting('head_tags'),['class'=>'form-control','placeholder'=>'Head Insert Tags']) !!}
        </div>
    </div>

</div>
<div class="tab-pane" id="btabs-contact" role="tabpanel">
    <h4 class="font-w400">Contact Settings</h4>
    <div class="form-body">
        <div class="form-group">
            {!!  Html::decode(Form::label('office_address', 'Address  * </span>', ['class' => 'control-label']))  !!}
            {!! Form::textarea('office_address', setting('office_address'),['class'=>'form-control','placeholder'=>'Address']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('email', 'Site Email', ['class' => 'control-label']))  !!}
            {!! Form::text('email',setting('email'),['class'=>'form-control','placeholder'=>'Site Email']) !!}
        </div>
        @for($i=1; $i<=3; $i++)
            <div class="form-group">
                {!!  Html::decode(Form::label('phone_'.$i, 'Phone '.$i, ['class' => 'control-label']))  !!}
                {!! Form::text('phone_'.$i,setting('phone_'.$i),['class'=>'form-control','placeholder'=>'Phone'.$i]) !!}
            </div>
        @endfor
    </div>
</div>
<div class="tab-pane" id="btabs-social" role="tabpanel">
    <h4 class="font-w400">Social Settings</h4>
    <div class="form-body">
        @foreach(config('site.social_urls') as $social_key => $social_name)
            <div class="form-group">
                {!!  Html::decode(Form::label($social_key, $social_name, ['class' => 'control-label']))  !!}
                {!! Form::text($social_key, setting($social_key),['class'=>'form-control','placeholder'=>$social_name]) !!}
            </div>
        @endforeach
    </div>
</div>
<div class="tab-pane" id="btabs-ia" role="tabpanel">
    <h4 class="font-w400">Instant Article Settings</h4>
    <div class="form-body">
        <a target="_blank" href="{{ route('feed.rss') }}" class="text-primary">RSS Feed Url</a>
        <br>
        <a target="_blank" href="{{ route('feed.instantarticles') }}" class="text-primary">Instant
            Article Feed Url</a>
        <br>
        <div class="form-group">
            {!!  Html::decode(Form::label('fb_page_id', 'Facebook Page ID', ['class' => 'control-label']))  !!}
            {!! Form::text('fb_page_id',setting('fb_page_id'),['class'=>'form-control','placeholder'=>'Facebook Page ID']) !!}
        </div>

        @for($i=1; $i<=2; $i++)
            <div class="form-group">
                {!!  Html::decode(Form::label('ia_ad_'.$i, 'IA Ad '.$i, ['class' => 'control-label']))  !!}
                {!! Form::text('ia_ad_'.$i,setting('ia_ad_'.$i),['class'=>'form-control','placeholder'=>'IA Ad'.$i]) !!}
            </div>
        @endfor
    </div>
</div>
<div class="tab-pane" id="btabs-home" role="tabpanel">
    <h4 class="font-w400">HomePage Settings</h4>
    <div class="form-body">
        <div class="form-group">
            {!!  Html::decode(Form::label('home_title', 'Page Title', ['class' => 'control-label']))  !!}
            {!! Form::text('home_title', setting('home_title')?setting('home_title'):'Home',['class'=>'form-control','placeholder'=>'Page Title']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('home_meta_keywords', 'Meta Keywords', ['class' => 'control-label']))  !!}
            {!! Form::text('home_meta_keywords', setting('home_meta_keywords'),['class'=>'form-control','placeholder'=>'Meta Keywords']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('home_meta_description', 'Meta Description', ['class' => 'control-label']))  !!}
            {!! Form::textarea('home_meta_description', setting('home_meta_description'),['class'=>'form-control','placeholder'=>'Meta Description']) !!}
        </div>

        <div class="form-group">
            {!!  Html::decode(Form::label('home_spotlight', 'Enable Spotlight Section', ['class' => 'control-label']))  !!}
            {{ Form::select('home_spotlight',['enable'=>'Yes, Enable ','disable'=>'No, Disable '],setting('home_spotlight'),['class'=>'form-control js-select2']) }}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('home_spotlight_title', 'Spotlight Section Title', ['class' => 'control-label']))  !!}
            {!! Form::text('home_spotlight_title', setting('home_spotlight_title')?setting('home_spotlight_title'):'Spotlight',['class'=>'form-control','placeholder'=>'Spotlight Section Title']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('recent_posts_per_page', 'Home Recent Posts Per page', ['class' => 'control-label']))  !!}
            {!! Form::text('recent_posts_per_page', setting('recent_posts_per_page')?setting('recent_posts_per_page'):config('site.posts_per_page'),['class'=>'form-control','placeholder'=>'Home Recent Posts Per page']) !!}
        </div>
    </div>
</div>
<div class="tab-pane" id="btabs-footer" role="tabpanel">
    <h4 class="font-w400">Footer Settings</h4>
    <div class="form-body">
        <div class="form-group">
            {!!  Html::decode(Form::label('missed_post_title', 'Missed Post Title', ['class' => 'control-label']))  !!}
            {!! Form::text('missed_post_title', setting('missed_post_title')?setting('missed_post_title'):config('site.missed_post_title'),['class'=>'form-control','placeholder'=>'Missed Post Title']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('footer_copyright', 'Copyright Text', ['class' => 'control-label']))  !!}
            {!! Form::textarea('footer_copyright', setting('footer_copyright'),['class'=>'form-control','placeholder'=>'Copyright Text']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('footer_scripts', 'Footer Scripts', ['class' => 'control-label']))  !!}
            {!! Form::textarea('footer_scripts', setting('footer_scripts'),['class'=>'form-control','placeholder'=>'All Footer Scripts Here!']) !!}
        </div>
    </div>
</div>
<div class="tab-pane" id="btabs-social-login" role="tabpanel">
    <h4 class="font-w400">Social Login Setting</h4>
    @if(setting('enable_social_login') == 'enable')
        <div class="text-danger">
            @if(setting('facebook_login') == 'yes' && !config('services.facebook.client_id') && !config('services.facebook.client_secret') && !config('services.facebook.redirect'))
                <small class="form-text text-muted">Please Add FB_ID=Your Facebook API ID</small>
                <small class="form-text text-muted">Please Add FB_SECRET=Your Facebook API SECRET in</small>
                <small class="form-text text-muted">Please Add
                    FB_REDIRECT={{ url('social/handle/facebook') }} in .env file
                </small>
                <br>
            @endif

            @if(setting('twitter_login') == 'yes' && !config('services.twitter.client_id') && !config('services.twitter.client_secret') && !config('services.twitter.redirect'))
                <small class="form-text text-muted">Please Add TW_ID=Your Twitter API ID in .env
                    file
                </small>
                <small class="form-text text-muted">Please Add TW_SECRET=Your Twitter API SECRET
                    in .env
                    file
                </small>
                <small class="form-text text-muted">Please Add
                    TW_REDIRECT={{ url('social/handle/twitter') }} in .env file
                </small><br>
            @endif
            @if(setting('google_login') == 'yes' && !config('services.google.client_id') && !config('services.google.client_secret') && !config('services.google.redirect'))
                <small class="form-text text-muted">Please Add GOOGLE_ID=Your Google API ID in
                    .env file
                </small>
                <small class="form-text text-muted">Please Add GOOGLE_SECRET=Your Google API
                    SECRET in
                    .env file
                </small>
                <small class="form-text text-muted">Please Add
                    GOOGLE_REDIRECT={{ url('social/handle/google') }} in .env file
                </small><br>
            @endif
        </div>
    @endif
    <div class="form-group">
        {!!  Html::decode(Form::label('enable_social_login', 'Enable Social Login', ['class' => 'control-label']))  !!}
        {{ Form::select('enable_social_login',['enable'=>'Yes, Enable','disable'=>'No, Disable'],setting('enable_social_login'),['class'=>'form-control js-select2']) }}
    </div>
    @if(setting('enable_social_login') == 'enable')
        <div class="form-group">
            <label class="custom-checkbox">
                {{ Form::checkbox('facebook_login',null, setting('facebook_login')=='yes'?1:0,['class'=>'form-control']) }}
                <span> <i class="fa fa-facebook"></i> Enable Facebook Login</span>
            </label>
            <br>
            <label class="custom-checkbox">
                {{ Form::checkbox('twitter_login',null, setting('twitter_login')=='yes'?1:0,['class'=>'form-control']) }}
                <span> <i class="fa fa-twitter"></i> Enable Twitter Login</span>
            </label>
            <br>
            <label class="custom-checkbox">
                {{ Form::checkbox('google_login',null, setting('google_login')=='yes'?1:0,['class'=>'form-control']) }}
                <span> <i class="fa fa-google"></i> Enable Google Login</span>
            </label>
        </div>
    @endif
</div>
<div class="tab-pane" id="btabs-posts" role="tabpanel">
    <h4 class="font-w400">Posts Settings</h4>
    <div class="form-body">
        <div class="form-group">
            {!!  Html::decode(Form::label('posts_per_page', 'Posts Per Page', ['class' => 'control-label']))  !!}
            {!! Form::text('posts_per_page', setting('posts_per_page')?setting('posts_per_page'):config('site.posts_per_page'),['class'=>'form-control','placeholder'=>'Posts Per Page']) !!}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('enable_comment', 'Enable Comment', ['class' => 'control-label']))  !!}
            {{ Form::select('enable_comment',['enable'=>'Yes, Enable Comment','disable'=>'No, Disable Comment'],setting('enable_comment'),['class'=>'form-control js-select2']) }}
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('comment', 'Comment Type', ['class' => 'control-label']))  !!}
            {{ Form::select('comment',['default'=>'Default Comment','facebook'=>'Facebook Comment','disqus'=>'Disqus Comment'],setting('comment'),['class'=>'form-control js-select2']) }}
            @if(setting('comment') == 'facebook')
                <small class="form-text text-muted"><i class="fa fa-info-circle"></i> For Facebook
                    Comment Go to the facebook developer section (<a class="text-success"
                                                                     href="https://developers.facebook.com/docs/plugins/comments/">FB
                        Comment</a>) to get your comment codes and paste in Footer > Footer Scripts.
                </small>@endif
        </div>
        <div class="form-group">
            {!!  Html::decode(Form::label('enable_rating', 'Enable Rating', ['class' => 'control-label']))  !!}
            {{ Form::select('enable_rating',['enable'=>'Yes, Enable Rating','disable'=>'No, Disable Rating'],setting('enable_rating'),['class'=>'form-control js-select2']) }}
        </div>
    </div>
</div>








<li class="nav-item">
    <a class="nav-link show" href="#btabs-home" aria-controls="btabs-home" role="tab"
       data-toggle="tab" aria-selected="true">HomePage</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#btabs-contact" aria-controls="btabs-contact" role="tab"
       data-toggle="tab" aria-selected="false">Contact</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#btabs-social" aria-controls="btabs-social" role="tab"
       data-toggle="tab" aria-selected="false">Social</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#btabs-ia" aria-controls="btabs-ia" role="tab" data-toggle="tab"
       aria-selected="false">Instant Article</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#btabs-posts" aria-controls="btabs-posts" role="tab" data-toggle="tab"
       aria-selected="false">Posts Setting</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#btabs-social-login" aria-controls="btabs-social-login" role="tab"
       data-toggle="tab" aria-selected="false">Social Login</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="#btabs-footer" aria-controls="btabs-footer" role="tab"
       data-toggle="tab" aria-selected="false">Footer</a>
</li>
