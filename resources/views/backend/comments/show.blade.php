@extends('backend.layout')
@section('title') {{ __('site.comment.comments') }} @endsection
@push('styles')

@endpush
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-file-text-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span
                            class="d-inline-block title-lg ml-3 v-m">  {{ __('site.comment.comments') }} - {{ __('site.comment.comment_detail') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="pull-right">
                        <a class="btn btn-outline-dark btn-sm" href="{{ url()->previous() }}">
                            <i class="fa fa-angle-left"></i> {{ __('site.comment.back_to_comments_list') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex flex-column mb-30">
        <div class="comments-container">
            <ul id="comments-list" class="comments-list">
                @include('backend.comments._comment')
            </ul>
        </div>
    </div>
@endsection
