@inject('markdown', 'Parsedown')
<li>
    @if($comment->parent == null)
        <div class="comment-main-level">
            @endif

            <div class="card">
                <div class="card-header">
                    @if(!is_null($comment->commenter->profile) && $comment->commenter->profile->avatar)
                        <div class="comment-avatar "><img class="img-fluid rounded-circle"
                                                         src="{{ $comment->commenter->profile->avatar  }}"
                                                         alt="{{ $comment->commenter->name }} Avatar"></div>
                    @else
                        <div class="comment-avatar"><img class="img-fluid rounded-circle"
                                                         src="{{ Gravatar::src($comment->commenter->email) }}"
                                                         alt="{{ $comment->commenter->name }} Avatar"></div>
                    @endif
                    <div class="flex mr-auto order-0">
                        <h4 class="title mb-0">{{ __('site.comment.commented_by') }} <a target="_blank" class="text-secondary" href="{{ route('user.posts',$comment->commenter->id ) }}">{{ $comment->commenter->full_name }}</a></h4>
                        <h4 class="title mb-0"> {{ __('site.comment.comment_for') }}  <a target="_blank" class="text-secondary" href="{{ route('posts.show',$comment->commentable->slug ) }}">{{ $comment->commentable->title }}</a></h4>
                        <span>{{ $comment->created_at->diffForHumans() }}</span>
                        @if($comment->approved)
                           <a data-toggle="tooltip" title="" data-original-title="{{ __('site.comment.disapprove_comment') }}"  href="{{ route('comments.suspend', $comment->id) }}"><span class="text-warning"><i class="fa fa-times-circle-o"></i> {{ __('site.comment.disapprove') }}</span></a>
                        @else
                           <a data-toggle="tooltip" title="" data-original-title="{{ __('site.comment.approve_comment') }}" href="{{ route('comments.approve', $comment->id) }}"><span class="text-success"><i class="fa fa-check-circle-o"></i> {{ __('site.comment.approve') }}</span></a>
                        @endif

                    </div>
                        <div class="pull-right">
                            @can('delete-comment', $comment)
                                <form id="comment-delete-form-{{ $comment->id }}"
                                      action="{{ route('comments.destroy', $comment->id) }}" method="POST" class="delete-form">
                                    @method('DELETE')
                                    @csrf
                                    <button  data-toggle="tooltip" title="" data-original-title="{{ __('site.comment.delete_comment') }}" class="btn btn-square btn-sm btn-outline-danger" type="submit"><i class="fa fa-trash text-danger-active"></i></button>
                                </form>
                            @endcan
                        </div>
                </div>
                <div class="card-body">

                   {!! $markdown->line($comment->comment) !!}
                </div>
            </div>
        @if($comment->children)
            <ul class="comments-list reply-list">
                @each ('backend.comments._comment', $comment->children, 'comment')
            </ul>
        @endif
            @if($comment->parent == null)
                </div>
    @endif
</li>
