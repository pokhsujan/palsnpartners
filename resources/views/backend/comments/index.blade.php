@extends('backend.layout')
@section('title') {{ __('site.comment.comments') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="list">
                        <i class="fa fa-file-text-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m"> {{ __('site.comment.comments') }} - {{ __('site.comment.all_comments') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex flex-column mb-30">
        @if(in_array(setting('comment_type'), ['facebook', 'disqus']))
            <div class="text-center alert alert-info">
                Site is Using <span class="text-uppercase">{{ setting('comment_type') }}</span> Comment System.
            </div>
        @else
        <div class=" table-responsive-sm">
            <table class="table table-sm">
                <thead>
                <tr class="uppercase">
                    <th class="text-center">#</th>
                    <th>{{ __('site.comment.comment_for') }}</th>
                    <th>{{ __('site.comment.commented_by') }}</th>
                    <th>{{ __('site.status') }}</th>
                    <th>{{ __('site.comment.commented_at') }}</th>
                    <th>Replies</th>
                    <th class="text-center">{{ __('site.actions') }}</th>
                </tr>
                </thead>
                <tbody>

                @foreach($comments as $comment)
                    <tr>
                        <td class="text-center">#</td>
                        <td>{{$comment->commentable->title}}</td>
                        <td>{{$comment->commenter->full_name}}</td>
                        <td>{!!  $comment->approved?'<span class="text-success">'.__('site.comment.approved').'</span>':'<span class="text-danger">'.__('site.comment.disapproved').'</span>'  !!}</td>
                        <td>{{ $comment->created_at->diffForHumans() }}</td>
                        <td>{{count($comment->children) }} Replies</td>
                        <td class="text-center">
                            @if($comment->approved)
                                <a data-toggle="tooltip" title="" data-original-title="{{ __('site.comment.disapprove_comment') }}"  class="btn btn-sm btn-light" href="{{ route('comments.suspend', $comment->id) }}"><i class=" text-danger fa fa-times-circle-o"></i></a>
                            @else
                                <a data-toggle="tooltip" title="" data-original-title="{{ __('site.comment.approve_comment') }}"  class="btn btn-sm btn-light" href="{{ route('comments.approve', $comment->id) }}"><i class="text-success fa fa-check-circle-o"></i></a>
                            @endif
                            <div class="btn-group">
                                <a href="{{route('comments.show',$comment->id)}}" class="btn btn-sm btn-light"
                                   data-toggle="tooltip" title="" data-original-title="{{ __('site.comment.show_comment') }}"><i
                                        class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                        </td>
                @endforeach

                </tbody>
            </table>
        </div>
        @endif
    </div>
@endsection
