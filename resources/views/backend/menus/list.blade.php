@extends('backend.layout')
@section('title')  {{ __('site.menu.menus') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-navicon rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m"> {{ __('site.menu.menus') }} - {{ __('site.menu.all_menu_position') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                <tr class="uppercase">
                    <th class="text-center">#</th>
                    <th> {{ __('site.menu.menu_position') }}</th>
                    <th class="text-center">{{ __('site.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach(config('site.menupositions') as $menu_type => $menu)
                    <tr>
                        <td class="text-center">#</td>
                        <td>{{ $menu }} ({{$menu_type}})
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{route('menus.edit','menu_type='.$menu_type)}}"
                                   class="btn btn-sm btn-icon-o btn-light mr-1" data-toggle="tooltip" title=""
                                   data-original-title="{{ __('site.edit') }}"><i class="fa fa-pencil text-primary-active"
                                                                 aria-hidden="true"></i></a>
                            </div>
                        </td>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
