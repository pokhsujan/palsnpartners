@extends('backend.layout')
@section('title') {{ __('site.menu.menus') }} @endsection
@push('styles')
@endpush
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-navicon rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.menu.menus') }} - {{ __('site.menu.edit_menu') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 mb-30">
                    <div class="card category-lists mb-40">
                        <div class="card-header">
                            <h4 class="title mb-0 flex mr-auto order-0">{{ __('site.category.categories') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="slimscroll">
                                <div class="custom-controls-stacked">
                                    @foreach($categories as $category)
                                        <label class="custom-control custom-checkbox checkbox-success">
                                            {{ Form::hidden('name',$category->name) }}
                                            {{ Form::checkbox('menus[]',$category->name,null,['data-url'=>route('categories.show', $category->slug),'class'=>'custom-control-input']) }}
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{ $category->name  }}</span>
                                        </label>
                                        <br>
                                        @if(count($category->children))
                                            @foreach($category->children as $child)
                                                <label class="custom-control custom-checkbox checkbox-success ml-4">
                                                    {{ Form::checkbox('menus[]',$child->name,null,['data-url'=>route('categories.show', $child->slug),'class'=>'custom-control-input']) }}
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">{{ $child->name  }}</span>
                                                </label>
                                                <br>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                            <a class="btn btn-sm  btn-primary add-to-menu float-right"
                               href="javascript:void(0);">{{ __('site.menu.add_to_menu') }}</a>
                        </div>
                    </div>
                    <div class="card page-lists mb-40">
                        <div class="card-header">
                            <h4 class="title mb-0 flex mr-auto order-0">{{ __('site.page.pages') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="slimscroll">
                                <div class="custom-controls-stacked ml-10">
                                    @foreach($pages as $page)
                                        <label class="custom-control custom-checkbox checkbox-success">
                                            {{ Form::hidden('name',$page->title) }}
                                            {{ Form::checkbox('menus[]',$page->title,null,['data-url'=>route('pages.show', $page->slug), 'class'=>'custom-control-input']) }}
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{ $page->title  }}</span>
                                        </label>
                                        <br>
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                            <a class="btn btn-sm  btn-primary add-to-menu float-right"
                               href="javascript:void(0);">{{ __('site.menu.add_to_menu') }} </a>
                        </div>
                    </div>
                    <div class="card custom-links mb-40">
                        <div class="card-header">
                            <h4 class="title mb-0 flex mr-auto order-0">{{ __('site.menu.custom_link') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                {{Form::text('menu_title',old('menu_title'),['class'=>'form-control','placeholder'=>__('site.menu.menu_text')])}}
                            </div>
                            <div class="form-group">
                                {{Form::text('menu_link',old('menu_link'),['class'=>'form-control','placeholder'=>__('site.menu.menu_link')])}}
                            </div>
                            <hr>
                            <a class="btn btn-sm  btn-primary add-to-menu float-right"
                               href="javascript:void(0);">{{ __('site.menu.add_to_menu') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 mb-30">
                    <div class="card menu-edit-block">
                        <div class="card-header">
                            <h4 class="title mb-0 flex mr-auto order-0">{{ config('site.menupositions.'.$menu_type) }}</h4>
                            <a class="btn btn-outline-primary btn-sm" href="{{ route('menus.index') }}">
                                <i class="fa fa-angle-left"></i> {{ __('site.menu.back_to_menus_list') }}
                            </a>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            {{ Form::hidden('menu_parent_type',$menu_type) }}
                            {{ Form::hidden('menu_type',$menu_type.'_'.$menu->id) }}
                            <div class="dd" id="menus">
                                <ol id="dd-menu-list" class="dd-list">
                                    @if($menu->children)
                                        {{ menusRecursive($menu->children) }}
                                    @endif
                                </ol>
                            </div>
                            <hr class="mt-4">
                            <a class="btn btn-sm btn-square btn-success save-menu float-right"
                               href="javascript:void(0);">{{ __('site.save') }} {{ __('site.menu.menu') }}</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('modal')
    <div class="modal" id="edit-menu-modal" tabindex="-1" role="dialog" aria-labelledby="edit-menu-modal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="fa fa-times"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::text('menu_title',old('menu_title'),['class'=>'form-control','placeholder'=>'Enter Title', 'id'=>'menu-title'])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('menu_link',old('menu_link'),['class'=>'form-control menu-link','placeholder'=>'Enter Link', 'id'=>'menu-link'])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-success" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-check"></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Normal Modal -->
@endpush
@push('scripts')
@endpush
