<div class="form-group {{ $errors->has($input_name)?' is-invalid':'' }}">
    {!! Html::decode(Form::label($input_name, $input_label, ['class' => 'control-label']))  !!}
    <div class="input-group">
           <span class="input-group-btn">
             <a data-input="{{ $data_input_url }}" data-preview="{{ $data_holder }}" data-hidden="{{ $data_input_id }}" class="js-media-manager-button btn btn-light btn-icon">
               <i class="fa fa-image"></i> {{ __('site.choose_image') }}
             </a>
           </span>
        {{ Form::hidden($input_name, $media_id?$media_id:old($input_name),['class'=>'form-control', 'id'=>$data_input_id]) }}
        {{ Form::text($data_input_url_name, $media_url?$media_url:old($data_input_url_name),['class'=>'form-control', 'id'=>$data_input_url]) }}
    </div>
    <div class="preview-wrapper position-relative">
        <img id="{{ $data_holder }}" style="margin-top:15px;max-height:100px;" src="{{ $media_url?$media_url:old($data_input_url_name) }}">
        <a href="javascript:void(0);" class="remove-featured-image position-absolute text-dark-active" style="left:2px; top:15px; display:{{$media_url?'block':'none'}}"><i class="fa fa-times-circle-o"></i></a>
    </div>
    @if ($errors->has($input_name))
        <div class="invalid-feedback animated fadeInDown">
            {{ $errors->first($input_name) }}
        </div>
    @endif
</div>
