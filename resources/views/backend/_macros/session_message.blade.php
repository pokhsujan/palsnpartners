@if (session('error'))
    <div class="alert-icon alert alert-danger" role="alert">
        <i class="fa fa-times-circle-o"></i> {{ session('error') }}
    </div>
@endif
@if (session('success'))
    <div class="alert-icon alert alert-success" role="alert">
        <i class="fa fa-check-circle-o"></i> {{ session('success') }}
    </div>
@endif
@if (session('status'))
    <div class="alert-icon alert alert-info" role="alert">
        <i class="fa fa-info-circle"></i> {{ session('status') }}
    </div>
@endif
@if (session('warning'))
    <div class="alert-icon alert alert-warning" role="alert">
        <i class="fa fa-warning"></i> {{ session('warning') }}
    </div>
@endif
@if (session('verified'))
    <div class="alert-icon alert alert-success" role="alert">
        <i class="fa fa-check-circle-o"></i> {{__('Email address Verified.')}}
        {{ __('Thank you for verifying your address.') }}
    </div>
@endif
