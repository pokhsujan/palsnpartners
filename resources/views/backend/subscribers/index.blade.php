@extends('backend.layout')
@section('title') {{ __('site.subscriber.subscribers') }} @endsection
@section('content')
    <div class="page-subheader mb-0">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-flag-checkered rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.subscriber.subscribers') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="btn-group float-right">
                        <a class="btn btn-light" href="{{ route('subscribers.export','csv') }}"><i class="fa fa-file"></i> {{ __('site.subscriber.export_csv') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex no-padding">
        <div class="flex d-flex">
            <div class="d-flex flex flex-column">
                <div class="flex p-3">
                    <div class="table-responsive">
                        <table class="table table-sm">
                            <thead>
                            <tr class="uppercase">
                                <th>#</th>
                                <th>{{ __('site.full_name') }}</th>
                                <th>{{ __('site.email') }}</th>
                                <th>{{ __('site.subscriber.subscribed_at') }}</th>
                                <th class="text-center">{{ __('site.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subscribers as $subscriber)
                                <tr>
                                    <td>#</td>
                                    <td>{{$subscriber->full_name}} {!! $subscriber->subscribed?'<a href="javascript:void(0);" data-toggle="tooltip" data-original-title="'.__('site.subscriber.subscribed').'"><i class="fa fa-check-circle-o text-success"></i></a>':'<a href="javascript:void(0);" data-toggle="tooltip" data-original-title="'.__('site.subscriber.unsubscribed').'"><i class="fa fa-times-circle-o text-danger"></i></a>'  !!}</td>
                                    <td>{{$subscriber->email}}</td>
                                    <td>{{$subscriber->created_at->format('l, d M Y')}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            {!! Form::open(array('route' => ['subscribers.destroy',$subscriber->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                                            {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                                            {!! Form::close()!!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $subscribers->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection
