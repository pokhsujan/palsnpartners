@extends('backend.layout')
@section('title') {{ __('site.ad.ads') }} @endsection
@section('content')
  <div class="page-subheader mb-3">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-7">
          <div class="list">
            <i class="fa fa-newspaper-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
            <span class="d-inline-block title-lg ml-3 v-m">  {{ __('site.ad.ads') }}  - {{ __('site.ad.all_ads') }}</span>
          </div>
        </div>
        <div class="col-md-5 float-right">
            <a data-toggle="tooltip" title="" data-original-title="{{ __('site.ad.new_ad') }} " class="btn btn-outline-success btn-sm mr-1" href="{{ route('ads.create') }}"><i class="icon-file-add"></i> {{ __('site.ad.new_ad') }}</a>
            <a data-toggle="tooltip" title="" data-original-title="{{ __('site.ad.upcoming') }} {{ __('site.ad.ads') }}" href="{{ route('ads.upcoming') }}" class="btn btn-outline-secondary btn-sm mr-1 ">{{ __('site.ad.upcoming') }}</a>
            <a data-toggle="tooltip" title="" data-original-title="{{ __('site.ad.expired') }} {{ __('site.ad.ads') }}" href="{{ route('ads.expired') }}" class="btn btn-outline-danger btn-sm mr-1 float-right">{{ __('site.ad.expired') }}</a>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content d-flex flex">
    @include('backend.ads.partials.list')
  </div>
@endsection
