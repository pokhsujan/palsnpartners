<div class="table-responsive">
    <table class="table table-sm">
        <thead>
        <tr class="uppercase">
            <th class="text-center">#</th>
            <th>{{ __('site.title') }}</th>
            <th>{{ __('site.ad.ad_position') }}</th>
            <th class="d-none d-sm-table-cell">{{ __('site.ad.start_date') }}</th>
            <th class="d-none d-sm-table-cell">{{ __('site.ad.end_date') }}</th>
            <th class="d-none d-sm-table-cell">{{ __('site.ad.ends_after') }}</th>
            <th class="text-center">{{ __('site.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($advertisements as $key=>$ad)
            <tr>
                <td>{{++$key}}</td>
                <td>{{$ad->title}}
                <td>{{ implode(', ',$ad->adspaces()->pluck('name')->toArray()) }}</td>
                <td>{{$ad->start_date->format('l, d M Y') }}</td>
                <td>{{$ad->end_date->format('l, d M Y') }}</td>
                <td>{{$ad->end_date->diffInDays(\Carbon\Carbon::now()) }} Days</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{route('ads.edit',$ad->id)}}" class="btn btn-sm btn-icon-o btn-light mr-1"
                           data-toggle="tooltip" title="" data-original-title="{{ __('site.edit') }}"><i
                                class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                        {!! Form::open(array('route' => ['ads.destroy',$ad->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                        {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-square btn-sm btn-alt-danger','type' => 'submit', 'data-toggle'=>'tooltip', 'data-original-title'=>__('site.ad.delete_ad'))) !!}
                        {!! Form::close()!!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{ $advertisements->render() }}
