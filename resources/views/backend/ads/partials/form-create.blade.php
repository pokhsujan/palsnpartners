<div class="col-md-9">
    @if(count($errors)>0)
        <div class="error mb-10">
            <strong>Something went wrong. Please check below</strong>
        </div>
    @endif
    <div class="form-body">
        <div class="form-group {{ $errors->has('title')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('title', __('site.title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('title',old('title'),['class'=>'form-control','placeholder'=>__('site.title')]) !!}
            @if ($errors->has('title'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('title') }}
                </span>
            @endif
        </div>
        <div class="form-group gold-type {{ $errors->has('ad_type')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('ad_type', __('site.ad.ad_type'). ' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            <div class="form-check">
                <label class="custom-radio radio-primary">
                    {{ Form::radio('ad_type', 'image_ad', old('image_ad')) }}
                    <span>{{ __('site.ad.image_ad') }}</span>
                </label>
            </div>
            <div class="form-check">
                <label class="custom-radio radio-primary">
                    {{ Form::radio('ad_type', 'script_ad', old('script_ad')) }}
                    <span>{{ __('site.ad.script_ad') }}</span>
                </label>
            </div>
            @if ($errors->has('ad_type'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('ad_type') }}
                </span>
            @endif
        </div>
        <div class="gold-featured-image">
            @include('backend._macros.media_picker', [
               'media_id' => null,
               'media_url' => null,
               'input_name' => 'featured_image',
               'input_label' => __('site.ad.ad_image') . __('<span class="required"> * </span>'),
               'data_input_id' => 'thumbnail-id',
               'data_input_url' => 'thumbnail-url',
               'data_input_url_name' => 'featured_image_url',
               'data_holder' => 'holder'
            ])
        </div>
        <div class="form-group gold-url {{ $errors->has('url')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('url', __('site.ad.ad_url').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('url',null,['class'=>'form-control','placeholder'=>__('site.ad.ad_url')]) !!}
            @if ($errors->has('url'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('url') }}
                </span>
            @endif
        </div>
        <div class="form-group gold-content {{ $errors->has('script')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('script', __('site.ad.script_ad_here'). __('<span class="required"> * </span>'), ['class' => 'control-label']))  !!}
            {!! Form::textarea('script',old('script'),['class'=>'form-control']) !!}
            @if ($errors->has('script'))
                <div class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('script') }}
                </div>
            @endif
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group mb-20">
        <div class="form-group {{ ($errors->has('start_date'))?' is-invalid':'' }}">

            {!!  Html::decode(Form::label('input-dates', __('site.ad.ad_dates').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            <div class="input-group date" id="start-date">
                <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1"
                     data-autoclose="true" data-today-highlight="true">
                    {!! Form::text('start_date',old('start_date'),['id'=>'','class'=>'form-control','placeholder'=>__('site.ad.start_date'),'data-week-start'=>"1", 'data-autoclose'=>true, 'data-today-highlight'=>true]) !!}

                    <span class="input-group-addon">
             <button class="btn btn-light" type="button">
               <i class="fa fa-calendar"></i>
             </button>
          </span>
                </div>
            </div>
            @if ($errors->has('start_date'))
                <span class="invalid-feedback animated fadeInDown">
          {{ $errors->first('start_date') }}
        </span>
            @endif
        </div>
        <div class="form-group {{ ($errors->has('end_date'))?' is-invalid':'' }}">
            <div class="input-group date" id="end-date">
                {!! Form::text('end_date',old('end_date'),['id'=>'','class'=>'form-control','placeholder'=>__('site.ad.end_date'),'data-week-start'=>"1", 'data-autoclose'=>true, 'data-today-highlight'=>true]) !!}
                <span class="input-group-addon">
             <button class="btn btn-light" type="button">
               <i class="fa fa-calendar"></i>
             </button>
          </span>
            </div>
            @if ($errors->has('end_date'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('end_date') }}
                </span>
            @endif
        </div>
       <div class="form-group">
           {!! Html::decode(Form::label('adspaces', __('site.adspace.adspaces'), ['class' => 'control-label']))  !!}
           <div class="slimscroll">
               <div class="custom-controls-stacked">
                   @foreach($adspaces as $adspace)
                       <label class="custom-control custom-checkbox">
                           {{ Form::checkbox('adspace_id[]',$adspace->id,'',['class'=>'custom-control-input']) }}
                           <span class="custom-control-indicator"></span>
                           <span class="custom-control-description">{{ $adspace->name  }}</span>
                       </label>
                       <br>
                   @endforeach
               </div>
           </div>
       </div>

    </div>
</div>
