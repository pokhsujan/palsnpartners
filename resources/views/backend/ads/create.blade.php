@extends('backend.layout')
@section('title') {{ __('site.ad.ads') }} @endsection
@push('styles')

@endpush
@section('content')
  {!! Form::open(array('route' => ['ads.store'], 'method' => 'POST', 'role' => 'form')) !!}
  <div class="page-subheader mb-3">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-7">
          <div class="list">
            <i class="fa fa-newspaper-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
            <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.ad.ads') }} - {{ __('site.ad.new_ad') }}</span>
          </div>
        </div>
        <div class="col-md-5">
          <div class="float-right">
            <a class="btn btn-square btn-outline-primary btn-sm" href="{{ route('ads.index') }}">
              <i class="fa fa-angle-left"></i> {{ __('site.ad.back_to_running_ads_list') }}
            </a>
            <button type="submit" class="btn btn-square btn-outline-success btn-sm"><i class="fa fa-check"></i> {{ __('site.save') }}
            </button>
            <button type="submit" name="submit" class="btn btn-square btn-outline-success btn-sm"
                    value="save_continue"><i class="fa fa-check-circle"></i> {{ __('site.save_continue') }}
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content d-flex flex">
    @include('backend.ads.partials.form-create')
  </div>
  <div class="p-3 bg-light border1 mb-4 clearfix">
    <div class="float-right">
      <a class="btn btn-square btn-outline-primary btn-sm" href="{{ route('ads.index') }}">
        <i class="fa fa-angle-left"></i> {{ __('site.ad.back_to_running_ads_list') }}
      </a>
      <button type="submit" class="btn btn-square btn-outline-success btn-sm"><i class="fa fa-check"></i> {{ __('site.save') }}
      </button>
      <button type="submit" name="submit" class="btn btn-square btn-outline-success btn-sm"
              value="save_continue"><i class="fa fa-check-circle"></i> {{ __('site.save_continue') }}
      </button>
    </div>
  </div>
  {!! Form::close() !!}
@endsection
@push('modal')
    @include('backend.medias._modal-media-list')
@endpush
@push('scripts')

@endpush
