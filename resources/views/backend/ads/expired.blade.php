@extends('backend.layout')
@section('title') {{ __('site.ad.ads') }} @endsection
@section('content')
  <div class="page-subheader mb-3">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-7">
          <div class="list">
            <i class="fa fa-newspaper-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
            <span class="d-inline-block title-lg ml-3 v-m"> {{ __('site.ad.ads') }}  - {{ __('site.ad.all_expired_ads') }}</span>
          </div>
        </div>
        <div class="col-md-5 float-right">
          <a class="btn btn-outline-success btn-sm mr-1" href="{{ route('ads.create') }}"><i class="icon-file-add"></i> {{ __('site.ad.new_ad') }}</a>
          <a href="{{ route('ads.index') }}" class="btn btn-outline-primary btn-sm mr-1 ">{{ __('site.ad.all_ads') }}</a>
          <a href="{{ route('ads.upcoming') }}" class="btn btn-outline-secondary btn-sm mr-1 float-right">{{ __('site.ad.upcoming') }}</a>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content d-flex flex">
    @include('backend.ads.partials.list')
  </div>
@endsection
