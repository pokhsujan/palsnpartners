@extends('backend.layout')
@section('title') {{ __('site.message.messages') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-file-text-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">  {{ __('site.message.messages') }} -  {{ __('site.message.message_detail') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                  <div class="pull-right">
                      <a class="btn btn-outline-dark btn-sm" href="{{ url()->previous() }}">
                          <i class="fa fa-angle-left"></i> {{ __('site.message.back_to_messages_list') }}
                      </a>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex flex-column mb-30">
        <div class="table-responsive">
            <table class="table table-borderless text-left mb-0">
                <thead>
                <th style="width: 250px;"></th>
                <th style="width: 50px;"></th>
                <th></th>
                </thead>
                <tbody>
                <tr>
                    <td>{{ __('site.message.messaged_at') }}</td>
                    <td>:</td>
                    <td><mark class="text-black">{{ $message->created_at->format('l, d M Y') }}</mark></td>
                </tr>
                <tr>
                    <td>{{ __('site.full_name')  }}</td>
                    <td>:</td>
                    <td>{{ $message->full_name }}</td>
                </tr>
                <tr>
                    <td>{{ __('site.email')  }}</td>
                    <td>:</td>
                    <td>
                        <a class="text-primary" href="mailto:{{ $message->email }}">
                            {{ $message->email }}
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="font-w600">{{ __('site.phone')  }}</td>
                    <td>:</td>
                    <td>
                        <a class="text-primary" href="tel:{{ $message->phone }}">
                            {{ $message->phone }}
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="font-w600">{{ __('site.mobile')  }}</td>
                    <td>:</td>
                    <td>
                        <a class="text-primary" href="tel:{{ $message->mobile }}">
                            {{ $message->mobile }}
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="font-w600">{{ __('site.message.message')  }}</td>
                    <td>:</td>
                    <td>{{ $message->message }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
