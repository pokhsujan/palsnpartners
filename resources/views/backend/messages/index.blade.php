@extends('backend.layout')
@section('title') {{ __('site.message.messages')  }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="list">
                        <i class="fa fa-file-text-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m"> {{ __('site.message.messages')  }} - {{ __('site.message.all_messages') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex flex-column mb-30">
        <div class=" table-responsive-sm">
            <table class="table table-sm">
                <thead>
                <tr class="uppercase">
                    <th class="text-center">#</th>
                    <th>{{ __('site.full_name')  }}</th>
                    <th>{{ __('site.email')  }}</th>
                    <th>{{ __('site.message.messaged_at')  }}</th>
                    <th class="text-center">{{ __('site.actions')  }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $message)
                    <tr>
                        <td class="text-center">#</td>
                        <td>{{$message->full_name}}</td>
                        <td>{{$message->email}}</td>
                        <td>{{$message->created_at->format('l, d M Y')}}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{route('messages.show',$message->id)}}" class="btn btn-sm btn-light"
                                   data-toggle="tooltip" title="" data-original-title="{{ __('site.message.show_message')  }}"><i
                                        class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                        </td>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection
