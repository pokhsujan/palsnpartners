<div class="modal" id="show-attachment-frame" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-fullwidth">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('site.media.media_manager') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="fa fa-times-circle-o"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="dad-tabs">
          <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="nav-item">
              <a href="#uploadImage" class="nav-link" aria-controls="uploadImage" role="tab" data-toggle="tab">{{ __('site.media.upload_media') }} </a>
            </li>
            <li role="presentation" class="nav-item">
              <a href="#selectImage" class="nav-link active" aria-controls="selectImage" role="tab" data-toggle="tab"> {{ __('site.media.select_media') }} </a>
            </li>
          </ul>
        </div>
        <div class="tab-content mt-10">
          <div id="uploadImage" class="tab-pane" role="tabpanel">
            <div id="media-create" class="m-3">
              @include('backend.medias._create', ['modal'=>'true'])
              <div id="info-load" style="display:none">{{ __('site.media.uploading') }}</div>
            </div>
          </div>
          <div id="selectImage" class="tab-pane active show" role="tabpanel">
            <div class="media-popup">
              <div class="media-search row mb-10">
                <div class="col-md-4 col-md-offset-4">
                  {!! Form::open(['route'=>'media.lists', 'method' => 'POST', 'role' => 'form' ,'class'=>'form-inline']) !!}
                    {{ Form::select('type',[null => __('site.media.select_media_type'),'video'=>__('site.videos'),'image'=>__('site.images'),'audio'=>__('site.audios'), 'document'=>__('site.documents')],'',['class'=>'select2 form-control mb-2 mr-sm-2 mb-sm-0']) }}
                    {{ Form::text('s', old('s'), ['class'=>'form-control mb-2 mr-sm-2 mb-sm-0', 'placeholder'=> __('site.search')]) }}
                  {!! Form::close() !!}
                </div>
              </div>
              <div class="row">
                <div class="col-md-9">
                  <div class="media-lists block block-mode-loading-refresh">
                    <div class="block-content">
                      <ul id="media-list-modal" class="attachments" data-toggle="slimscroll" data-color="#42a5f5" data-opacity="1" data-always-visible="true" data-height="550px">
                      </ul>
                      <div class="row mt-5 mb-20 media-load-footer text-center">
                        <div class="col-md-12">
                          <a href="javascript:void(0);" id="load-more" class="btn btn-danger loadmore-images" data-type="image" style="display:none;">{{ __('site.media.load_more_media') }}</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div id="media-selected-info" class="block block-mode-loading-refresh block-shadow">
                    <div class="block-content text-primary-dark p-20">{{ __('site.media.media_select_info') }}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
