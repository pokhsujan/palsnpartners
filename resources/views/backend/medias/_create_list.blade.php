@if($medias)
    @foreach($medias as $media)
    <div class="media-item">
        <div class="media-thumb">
          <img class="pinkynail" src="{{ $media->preview_url }}" alt="">
        </div>
        <div class="filename">
          <span>{{ $media->original_name }}</span>
        </div>
        <a href="javascript:void(0);" data-edit-url="{{ route('media.edit', $media->id) }}" class="edit-attachment" target="_blank"> {{ __('site.edit') }}</a>
    </div>
    @endforeach
@endif
