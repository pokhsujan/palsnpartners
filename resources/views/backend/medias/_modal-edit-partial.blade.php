<div class="row">
    <div class="col-md-6">
        <div class="attachment-details save-ready">
            <div class="attachment-media-view landscape">
                <div class="thumbnail thumbnail-image">
                    @if($media->type == 'image')
                    <img class="details-image" src="{{ url($media->media_url) }}" alt="">
                        @else
                        <img class="details-image" src="{{ $media->preview_url }}" alt="">
                        @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="attachment-info">
            <div class="details">
                <div class="filename"><strong>{{ __('site.media.file_name') }}:</strong> {{ $media->filename }}</div>
                <div class="filename"><strong>{{ __('site.media.url') }}:</strong> {{ url($media->media_url) }}</div>
                <div class="filename"><strong>{{ __('site.media.file_type') }}:</strong> {{ $media->mime }}</div>
                <div class="uploaded"><strong>{{ __('site.media.uploaded_at') }}:</strong> {{ $media->created_at->format('l, d M Y') }}</div>
                <div class="file-size"><strong>{{ __('site.media.file_size') }}:</strong> {{ bytesToHuman(\Storage::size($media->upload_directory. '/'.$media->filename)) }}</div>
                <div class="compat-meta">
                </div>
            </div>
            <div class="settings">
                {!! Form::open(['url' => route('media.update', $media->id), 'method'=>'PATCH','role'=>'form','id'=>'attachment-edit']) !!}
                <div class="form-body">
                    <div class="form-group row">
                        {!!  Html::decode(Form::label('url',  __('site.media.url'), ['class' => 'col-lg-3 col-form-label']))  !!}
                        <div class="col-lg-9">
                            {!! Form::text('url',url($media->media_url) ,['class'=>'form-control','placeholder'=> __('site.media.url') ,'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!!  Html::decode(Form::label('original_name',  __('site.title'), ['class' => 'col-lg-3 col-form-label']))  !!}
                        <div class="col-lg-9">
                            {!! Form::text('original_name',$media->original_name,['class'=>'form-control','placeholder'=> __('site.title')]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!!  Html::decode(Form::label('caption',  __('site.media.caption'), ['class' => 'col-lg-3 col-form-label']))  !!}
                        <div class="col-lg-9">
                            {!! Form::textarea('caption',$media->caption,['size'=>'30x3', 'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!!  Html::decode(Form::label('original_name',  __('site.description'), ['class' => 'col-lg-3 col-form-label']))  !!}
                        <div class="col-lg-9">
                            {!! Form::textarea('description',$media->description,['size'=>'30x3', 'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <input value="{{  __('site.save') }}" class="btn btn-md btn-outline-success" type="submit" id="attachment-save">
                    <span class="settings-save-status text-success" style="display:none">
                        <i class="fa fa-check-circle-o"></i>
                        <span class="saved">{{  __('site.media.saved') }}</span>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="action text-right">
                {!! Form::open(array('route' => ['media.destroy',$media->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','id'=>'attachment-delete')) !!}
                    {!! Form::button('<i class="fa fa-trash"></i> '. __('site.delete'), array('class' => 'btn btn-sm btn-outline-danger','type' => 'submit')) !!}
                {!! Form::close()!!}
            </div>
        </div>
    </div>
</div>
