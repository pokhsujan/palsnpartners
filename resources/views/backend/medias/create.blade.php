@extends('backend.layout')
@section('title') {{ __('site.media.media_manager') }}  @endsection
@push('styles')

@endpush
@section('content')
  <div class="page-subheader mb-3">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-7">
          <div class="list">
            <i class="fa fa-file-picture-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
            <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.media.media_manager') }} - {{ __('site.media.upload_media') }}</span>
          </div>
        </div>
        <div class="col-md-5">
          <div class="float-right">
            <a class="btn btn-outline-primary btn-sm" href="{{ route('media.index') }}"><i class="fa fa-list"></i> {{ __('site.media.media_list') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content d-flex flex">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 mb-30">
          <div id="media-create" class="mb-20">
            @include('backend.medias._create', ['modal'=>'false'])
            <div id="info-load" style="display:none">{{ __('site.media.uploading') }}</div>
          </div>
          <div id="media-manager">

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
@push('modal')
    @include('backend.medias._modal-edit')
@endpush
@push('scripts')

@endpush
