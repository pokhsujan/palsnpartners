
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title> {{ __('site.media.media_manager') }} | {{ config('cms.site_name','CMS') }}</title>
    <meta name="author" content="lamputermedia">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}">
    <!-- END Icons -->
    <!-- Stylesheets -->

  <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
  <link href="{{ asset('css/admin-style.css') }}" rel="stylesheet">
</head>
    <body>
    <div id="attachment-container">
    <!-- END Header -->
        <!-- Main Container -->
        <main id="main-container">
        <!-- Page Content -->
            <section id="app" class="content"  v-cloak>
                <div id="show-attachment-frame">
                    <div class="modal-fullwidth">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2> {{ __('site.media.media_manager') }}</h2>
                            </div>
                            <div class="modal-body">
                              <div class="dad-tabs">
                                <ul class="nav nav-pills" role="tablist">
                                  <li class="nav-item">
                                    <a href="#uploadImage" class="nav-link" aria-controls="uploadImage" role="tab" data-toggle="tab"> {{ __('site.media.upload_media') }} </a>
                                  </li>
                                  <li class="nav-item">
                                    <a href="#selectImage"class="nav-link active" aria-controls="selectImage" role="tab" data-toggle="tab">  {{ __('site.media.select_media') }} </a>
                                  </li>
                                </ul>
                                <div class="tab-content mt-10 p-4">
                                  <div id="uploadImage" class="tab-pane" role="tabpanel">
                                    <div id="media-create" class="mb-3">
                                      @include('backend.medias._create', ['modal'=>'true'])
                                      <div id="info-load" style="display:none"> {{ __('site.media.uploading') }}</div>
                                    </div>
                                  </div>
                                  <div id="selectImage" class="tab-pane active show" role="tabpanel">
                                    <div class="media-popup">
                                      <div class="media-search row mb-10">
                                        <div class="col-md-4 col-md-offset-4">
                                          {!! Form::open(['route'=>'media.lists', 'method' => 'POST', 'role' => 'form' ,'class'=>'form-inline']) !!}
                                          {{ Form::select('type',[null => __('site.media.select_media_type'),'video'=>__('site.videos'),'image'=>__('site.images'),'audio'=>__('site.audios'), 'document'=>__('site.documents')],$type,['class'=>'select2 form-control mb-2 mr-sm-2 mb-sm-0']) }}
                                          {{ Form::text('s', old('s'), ['class'=>'form-control mb-2 mr-sm-2 mb-sm-0', 'placeholder'=> __('site.search')]) }}
                                          {!! Form::close() !!}
                                        </div>
                                      </div>
                                      <div class="row mb-20">
                                        <div class="col-md-9">
                                          <div class="media-lists block block-mode-loading-refresh">
                                            <div class="block-content">
                                              <ul id="media-list-modal" class="attachments" data-toggle="slimscroll" data-color="#42a5f5" data-opacity="1" data-always-visible="true" data-height="400px">
                                                @include('backend.medias._index_list',['medias'=>$medias])

                                              </ul>
                                              @if($medias->lastPage() > 1)
                                                <div class="row mt-20 mb-20 media-load-footer text-center">
                                                  <div class="col-md-12">
                                                    <a href="javascript:void(0);" id="load-more" class="btn btn-danger loadmore-images" data-type="{{ $type }}">{{ __('site.media.load_more_media') }}</a>
                                                  </div>
                                                </div>
                                              @endif
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div id="media-selected-info" class="block block-mode-loading-refresh block-shadow">
                                            <div class="text-primary-dark p-20 block-content">{{ __('site.media.media_select_info') }}</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <script> var CKEDITOR_BASEPATH = '/js/ckeditor/'; </script>
    <script type="text/javascript" src="{{ mix('js/admin.js') }}"></script>
    <script>
      function insertCKeditor(url, message) {
        window.opener.CKEDITOR.tools.callFunction(getUrlParameter('CKEditorFuncNum'), url, function () {
            var dialog = this.getDialog();
            // Check if this is the Image Properties dialog window.
            if ( dialog.getName() == 'image' ) {
                // Get the reference to a text field that stores the "alt" attribute.
                var element = dialog.getContentElement( 'info', 'txtAlt' );
                // Assign the new value.
                if ( element )
                    element.setValue( message );
            }
        });
        window.close();
      }
      function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');
          if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
          }
        }
      };
    </script>
    </body>
</html>
