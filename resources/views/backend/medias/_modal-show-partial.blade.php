<div class="block-content attachment-info">
    <div class="attachment-media-view landscape mb-20">
        <div id="media-selector" class="thumbnail thumbnail-image">
            <img class="details-image"  data-id="{{ $media->id }}" data-original-url="{{$media->media_url}}" src="{{ $media->preview_url }}" alt="{{ $media->caption }}">
        </div>
    </div>
    <div class="details">
        <div class="filename"><strong>{{ __('site.media.file_name') }}:</strong> {{ $media->filename }}</div>
        <div class="filename"><strong>{{ __('site.media.url') }}:</strong> {{ url($media->media_url) }}</div>
        <div class="filename"><strong>{{ __('site.media.file_type') }}:</strong> {{ $media->mime }}</div>
        <div class="uploaded"><strong>{{ __('site.media.uploaded_at') }}:</strong> {{ $media->created_at->format('l, d M Y') }}</div>
        <div class="file-size"><strong>{{ __('site.media.file_size') }}:</strong> {{ bytesToHuman(\Storage::size($media->upload_directory. '/'.$media->filename)) }}</div>
        @if($media->type == 'image')
            <div class="dimensions"><strong>{{ __('site.media.dimensions') }}:</strong> {{ imageDimention(\Storage::path($media->upload_directory.'/'.$media->filename), true) }}</div>
        @endif
        <div class="compat-meta">
        </div>
    </div>
    <div class="text-center mb-20">
        @if($media->type == 'image')
        <button id="set-image" type="button" class="btn btn-outline-primary mt-20">{{ __('site.media.select_media') }}</button>
        @endif
        <a id="insert-image" class="btn btn-outline-primary mt-20" href="javascript:insertCKeditor('{{ $media->media_url }}', '{{ $media->caption }}')">{{ __('site.media.insert_media') }}</a>
    </div>
</div>
