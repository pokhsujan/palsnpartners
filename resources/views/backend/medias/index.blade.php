@extends('backend.layout')
@section('title') {{ __('site.media.media_manager') }}  @endsection
@push('styles')

@endpush
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <div class="list">
                        <i class="fa fa-file-picture-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.media.media_manager') }} </span>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="media-search float-right">
                        {!! Form::open(['route'=>'media.lists', 'method' => 'POST', 'role' => 'form' ,'class'=>'form-inline d-inline media-search-form']) !!}
                        {{ Form::select('type',[null => __('site.media.select_media_type'),'video'=>__('site.videos'),'image'=>__('site.images'),'audio'=>__('site.audios'), 'document'=>__('site.documents')],'',['class'=>'select2 form-control mb-2 mr-sm-2 mb-sm-0']) }}
                        {{ Form::text('s', old('s'), ['class'=>'form-control mb-2 mr-sm-2 mb-sm-0', 'placeholder'=> __('site.search')]) }}
                        {!! Form::close() !!}
                        <a class="btn btn-outline-primary btn-sm" href="{{ route('media.create') }}"><i class="fa fa-plus"></i> {{ __('site.media.add_new_media') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content">
        <div class="media-scroll">
            <ul id="media-list-modal" class="attachments" data-toggle="slimscroll" data-color="#42a5f5" data-opacity="1"
                data-always-visible="true" data-height="600px">
                @include('backend.medias._index_list')
            </ul>
        </div>
        @if($medias->lastPage() > 1)
            <div class="row mt-20 mb-20 media-load-footer text-center">
                <div class="col-md-12">
                    <a href="javascript:void(0);" id="load-more" class="btn btn-light loadmore-images">{{ __('site.media.load_more_media') }}</a>
                </div>
            </div>
        @endif
    </div>
@endsection
@push('modal')
    @include('backend.medias._modal-edit')
@endpush
@push('scripts')
    <script>
        $(".generate-thumbnail").click(function(e){
            e.preventDefault();
            var showcase = $(this);
            showcase.find('span').html("Generating...")
            $.ajax({
                type: 'GET',
                url: $(this).attr('href'),
                success: function(response){
                    showcase.find('span').html("<i class=\"fa fa-recycle\"></i> Regenerate Thumbnails");
                    uiHelperAlert('Success', response.data, 'success');
                }
            });
        });
    </script>
@endpush
