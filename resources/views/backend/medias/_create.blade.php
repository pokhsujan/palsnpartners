{!! Form::open(['url' => route('media.store'), 'method'=>'post', 'class' => 'dropzone js-dropzone', 'files'=>true, 'id'=>'media-upload', 'data-modal'=>$modal]) !!}
{{Form::hidden('user_id', Auth::user()->id)}}
<div class="dz-message">
    <i class="fa fa-plus"></i>
    {{ __('site.media.drop_files') }}<br />
    <span class="note needsclick">{{ __('site.media.upload_info') }}<br /></span>
</div>
<div class="fallback">
    <input name="file" type="file" multiple />
</div>
{!! Form::close() !!}
<div class="text-right d-inline-block">
    <span class="text-info-active">
       {{ __('site.media.max_file_size') }} {{ ini_get('post_max_size') }}
    </span>
</div>
