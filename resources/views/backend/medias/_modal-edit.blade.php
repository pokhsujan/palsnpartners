<!-- Large Modal -->
<div class="modal" id="edit-attachment-frame" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
              <h5 class="modal-title">{{ __('site.media.media_detail') }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="fa fa-times-circle-o"></span>
              </button>
            </div>
          <div class="modal-body">
            <div id="media-frame-content" class="block">
            </div>
          </div>
        </div>
    </div>
</div>
<!-- END Large Modal -->
