@if($medias)
    @foreach($medias as $media)
        <li class="attachment save-ready" data-edit-url="{{ route('media.edit', $media->id) }}" data-show-url="{{ route('media.show', $media->id) }}">
            <div class="attachment-preview landscape">
                <div class="thumbnail">
                    <div class="centered">
                        <img src="{{ $media->preview_url }}" alt="{{ $media->title }}">
                    </div>
                  <div class="filename">
                    <div>{{ $media->original_name }}</div>
                  </div>
                </div>
            </div>
        </li>
    @endforeach
@endif
