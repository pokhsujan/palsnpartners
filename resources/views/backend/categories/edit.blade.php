@extends('backend.layout')
@section('title') {{ __('site.category.edit_category') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-th-list rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.category.categories') }} - {{ __('site.category.edit_category') }} <span class="badge badge-primary badge-text">{{ $category->name }}</span></span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right">
                        <a class="btn btn-outline-primary btn-sm" href="{{ route('categories.index') }}"><i class="fa fa-angle-left"></i> {{ __('site.category.back_to_categories_list') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content d-flex flex">
        <div class="container-fluid">
            @include('backend.categories.partials.edit')
        </div>
    </div>
@endsection
