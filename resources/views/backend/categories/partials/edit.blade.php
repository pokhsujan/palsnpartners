{!! Form::open(array('route' => ['categories.update',$category->id],  'method' => 'PUT', 'role' => 'form',  'required' => 'required')) !!}
    <div class="form-group">
        {!! Form::label(__('site.category.select_parent_category')) !!}
        {!! Form::select('parent_id',$allCategories, $category->parent_id, ['class'=>'select2 form-control select-category', 'placeholder'=>__('site.category.select_parent_category')]) !!}
    </div>
    <div class="form-group {{$errors->has('name') ? 'is-invalid':''}}">
        {!! Form::label('name', __('site.category.category_name')) !!}
        {!! Form::text('name',$category->name, array('class' => 'form-control', 'placeholder' => __('site.category.category_name'),'id'=>'category-title')) !!}
        @if ($errors->has('name'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>
    <div class="form-group {{$errors->has('slug') ? 'is-invalid':''}}">
        {!! Form::label('slug', __('site.category.category_slug')) !!}
        {!! Form::text('slug',$category->slug, array('class' => 'form-control', 'placeholder' => __('site.category.category_slug'),'id'=>'category-slug')) !!}
        @if ($errors->has('slug'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('slug') }}
            </div>
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('description', __('site.description')) !!}
        {!! Form::textarea('description',$category->description, array('class' => 'form-control', 'placeholder' => __('site.description'))) !!}
    </div>

    <div class="form-group">
        {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success','type' => 'submit')) !!}
    </div>
{!! Form::close() !!}
