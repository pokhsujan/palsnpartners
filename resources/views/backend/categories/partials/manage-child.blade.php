@foreach($children as $child)
    <tr>
        <td class="highlight">
            <div class="info"></div>&nbsp; &nbsp; &nbsp; &nbsp;{{$child->name }}
        </td>
        <td>{{$child->slug}}</td>
        <td class="text-center">
            <div class="btn-group">
                <a href="{{route('categories.edit',$child->id)}}" class='btn btn-sm btn-icon-o btn-light mr-1' data-toggle="tooltip" data-original-title="{{ __('site.edit') }}"><i class="fa fa-pencil text-primary-active"></i></a>
                {!! Form::open(array('route' => ['categories.destroy',$child->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                {!! Form::close()!!}
            </div>
        </td>
    </tr>
@endforeach