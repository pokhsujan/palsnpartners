@extends('backend.layout')
@section('title'){{ __('site.category.categories') }} @endsection
@section('content')
    <div class="page-subheader mb-0">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-th-list rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.category.categories') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex no-padding">
        <div class="flex d-flex">
            <div class="sidebar sidebar-sm bg-white" id="sidebar-collapse">
                <div class="d-flex flex-column b-r">
                    <div class="navbar b-b">
                        {{ __('site.category.categories') }} - {{ __('site.category.add_category') }}
                    </div>
                    <div class="flex p-3">
                        @include('backend.categories.partials.create')
                    </div>
                </div>
            </div>
            <div class="sidebar-body flex d-flex" id="app-body">
                <div class="d-flex flex flex-column">
                    <div class="navbar bg-white align-items-center b-b">
                        {{ __('site.category.categories') }} - {{ __('site.category.list_categories') }}
                    </div>
                    <div class="flex p-3">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr class="uppercase">
                                    <th>{{ __('site.category.category_name') }}</th>
                                    <th>{{ __('site.category.category_slug') }}</th>
                                    <th class="text-center">{{ __('site.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->slug}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{route('categories.edit',$category->id)}}"
                                                   class='btn btn-sm btn-icon-o btn-light mr-1' data-toggle="tooltip" data-original-title="{{ __('site.edit') }}"><i
                                                        class="fa fa-pencil text-primary-active"></i></a>
                                                {!! Form::open(array('route' => ['categories.destroy',$category->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                                                {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </td>
                                    </tr>
                                    @if(count($category->children))
                                        @include('backend.categories.partials.manage-child',['children' => $category->children])
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $categories->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
