@extends('backend.layout')
@section('title') {{ __('site.tag.edit_tag') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-tags rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.tag.tags') }} - {{ __('site.tag.edit_tag') }} <span class="badge badge-primary badge-text">{{ $tag->name }}</span></span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right">
                        <a class="btn btn-outline-primary btn-sm" href="{{ route('tags.index') }}"><i class="fa fa-angle-left"></i> {{ __('site.tag.back_to_tags_list') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content d-flex flex">
        <div class="container-fluid">
            {!! Form::open(array('route' => ['tags.update', $tag->id],  'method' => 'PUT', 'role' => 'form',  'required' => 'required')) !!}
            <div class="form-group {{$errors->has('name') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('name', __('site.tag.tag_name').' <span class="required"> * </span>')) !!}
                {!! Form::text('name',$tag->name, array('class' => 'form-control', 'placeholder' => __('site.tag.tag_name'))) !!}
                @if ($errors->has('name'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('name') }}
                    </div>
                @endif
            </div>

            <div class="form-group {{$errors->has('slug') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('slug', __('site.tag.tag_slug').' <span class="required"> * </span>')) !!}
                {!! Form::text('slug',$tag->slug, array('class' => 'form-control', 'placeholder' => __('site.tag.tag_slug'),'disabled'=>'disabled')) !!}
                @if ($errors->has('slug'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('slug') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success','type' => 'submit')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
