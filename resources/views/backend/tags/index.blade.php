@extends('backend.layout')
@section('title') {{ __('site.tag.tags') }} @endsection
@section('content')

    <div class="page-subheader mb-0">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-tags rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m"> {{ __('site.tag.tags') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex no-padding">
        <div class="flex d-flex">
            <div class="sidebar sidebar-sm bg-white" id="sidebar-collapse">
                <div class="d-flex flex-column b-r">
                    <div class="navbar b-b">
                        {{ __('site.tag.tags') }} -  {{ __('site.tag.add_tag') }}
                    </div>
                    <div class="flex p-3">
                        {!! Form::open(array('route' => 'tags.store',  'method' => 'POST', 'role' => 'form',  'required' => 'required')) !!}
                        <div class="form-group {{$errors->has('name') ? 'is-invalid':''}}">
                            {!! Html::decode(Form::label('name', __('site.tag.tag_name').' <span class="required"> * </span>')) !!}
                            {!! Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => __('site.tag.tag_name'))) !!}
                            @if ($errors->has('name'))
                                <div class="invalid-feedback animated fadeInDown">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success btn-block','type' => 'submit')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="sidebar-body flex d-flex" id="app-body">
                <div class="d-flex flex flex-column">
                    <div class="navbar bg-white align-items-center b-b">
                        {{ __('site.tag.tags') }} - {{ __('site.tag.list_tags') }}
                    </div>
                    <div class="flex p-3">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr class="uppercase">
                                    <th>#</th>
                                    <th>{{ __('site.tag.tag_name') }}</th>
                                    <th>{{ __('site.tag.tag_slug') }}</th>
                                    <th class="text-center">{{ __('site.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tags as $tag)
                                    <tr>
                                        <td>#</td>
                                        <td>{{$tag->name}}</td>
                                        <td>{{$tag->slug}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{route('tags.edit',$tag->id)}}"
                                                   class='btn btn-sm btn-icon-o btn-light mr-1'><i
                                                        class="fa fa-pencil text-primary-active" data-toggle="tooltip" data-original-title="{{ __('site.edit') }}"></i></a>
                                                {!! Form::open(array('route' => ['tags.destroy',$tag->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                                                {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $tags->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
