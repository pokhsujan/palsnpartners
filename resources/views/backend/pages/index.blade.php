@extends('backend.layout')
@section('title') {{ __('site.page.pages') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-file-o rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.page.pages') }} - {{ __('site.page.all_pages') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <a title="Add New Page" class="btn btn-outline-primary btn-sm float-right"
                       href="{{ route('pages.create') }}"><i class="icon-file-add"></i> {{ __('site.page.new_page') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex flex-column">
        <div class=" table-responsive">
            <table class="table table-sm">
                <thead>
                <tr class="uppercase">
                    <th class="text-center">#</th>
                    <th>{{ __('site.title') }}</th>
                    <th>{{ __('site.slug') }}</th>
                    <th class="text-center">{{ __('site.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $key=>$page)
                    <tr>
                        <td class="text-center">#</td>
                        <td>{{$page->title}}
                        <td>{{$page->slug}}</td>
                        <td class="text-center">
                            <div class="btn-group-sm ">
                                <a href="{{route('pages.edit',$page->id)}}"
                                   class="btn btn-sm btn-icon-o btn-light mr-1"><i
                                        class="fa fa-pencil text-primary-active" data-toggle="tooltip" data-original-title="{{ __('site.edit') }}"></i></a>
                                @if($page->slug != 'homepage')
                                {!! Form::open(array('route' => ['pages.destroy',$page->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'d-inline-block delete-form')) !!}
                                {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light','type' => 'submit','data-toggle'=>'tooltip','data-original-title'=>__('site.delete'))) !!}
                                {!! Form::close()!!}
                                @endif
                            </div>
                        </td>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $pages->render() }}
    </div>
@endsection
