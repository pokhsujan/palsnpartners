@if(count($errors)>0)
    <div class="col-md-12">
        <div class="alert-icon alert alert-danger" role="alert">
            <i class="fa fa-times-circle-o"></i> Something went wrong. Please check below
        </div>
    </div>
@endif
<div class="col-md-8">
    <div class="form-body">
        <div class="form-group {{$errors->has('title') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('title', __('site.title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('title',$page->title,['class'=>'form-control','placeholder'=>__('site.title')]) !!}
            @if ($errors->has('title'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('title') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('banner_style') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('banner_style', 'Banner Style <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::select('banner_style', array('style-1' => 'Style 1', 'style-2' => 'Style2'), $page->getMeta('banner_style'), ['class'=> 'form-control js-select2']); !!}
            @if ($errors->has('banner_style'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('banner_style') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('enable_spotlight') ? 'is-invalid':''}}">
            <label for="enable_spotlight" class="custom-checkbox mb-3 checkbox-primary d-block">
                <input type="hidden" name="enable_spotlight" value="0">
                <input id="enable_spotlight" type="checkbox"
                       name="enable_spotlight"
                       {{old('enable_spotlight', $page->getMeta('enable_spotlight')) ? 'checked' : '' }} value="1">
                <span class="label-helper">Enable Spotlight News</span>
            </label>
        </div>
        <div class="form-group {{$errors->has('enable_recent_news') ? 'is-invalid':''}}">
            <label for="enable_recent_news" class="custom-checkbox mb-3 checkbox-primary d-block">
                <input type="hidden" name="enable_recent_news" value="0">
                <input id="enable_recent_news" type="checkbox"
                       name="enable_recent_news"
                       {{old('enable_recent_news', $page->getMeta('enable_recent_news')) ? 'checked' : '' }} value="1">
                <span class="label-helper">Enable Recent News</span>
            </label>
        </div>
        <div class="form-group {{$errors->has('recent_post_title') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('recent_post_title', __('site.recent_post_title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('recent_post_title',$page->getMeta('recent_post_title'),['class'=>'form-control','placeholder'=>__('site.recent_post_title')]) !!}
            @if ($errors->has('recent_post_title'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('recent_post_title') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('recent_posts_per_page') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('recent_posts_per_page', 'Recent Posts Per Page <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {{ Form::number('recent_posts_per_page',  $page->getMeta('recent_posts_per_page', 4), ['class'=>'form-control']) }}
            @if ($errors->has('recent_posts_per_page'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('recent_posts_per_page') }}
                </span>
            @endif
        </div>

        @for($i = 1; $i <= config('site.home_cat_sections_count', 4); $i++)
            <div class="p-3 border1 mb-30">
                <div class="title mb-10">Category Section {{ $i }}</div>
                <div class="title-sep mb-20"></div>
                <div class="form-row mb-20">
                    <div class="col">
                        <div class="form-group {{$errors->has('sec_'.$i.'_enable') ? 'is-invalid':''}}">
                            <label for="sec_{{ $i }}_enable" class="custom-checkbox mb-3 checkbox-primary d-block">
                                <input type="hidden" name="sec_{{ $i }}_enable" value="0">
                                <input id="sec_{{ $i }}_enable" type="checkbox"
                                       name="sec_{{ $i }}_enable"
                                       {{old('sec_'.$i.'_enable', $page->getMeta('sec_'.$i.'_enable')) ? 'checked' : '' }} value="1">
                                <span class="label-helper">Enable Section {{ $i }}</span>
                            </label>
                        </div>
                        <div class="form-group {{$errors->has('sec_'.$i.'_enable_ad') ? 'is-invalid':''}}">
                            <label for="sec_{{ $i }}_enable_ad" class="custom-checkbox mb-3 checkbox-primary d-block">
                                <input type="hidden" name="sec_{{ $i }}_enable_ad" value="0">
                                <input id="sec_{{ $i }}_enable_ad" type="checkbox"
                                       name="sec_{{ $i }}_enable_ad"
                                       {{old('sec_'.$i.'_enable_ad', $page->getMeta('sec_'.$i.'_enable_ad')) ? 'checked' : '' }} value="1">
                                <span class="label-helper">Enable Section {{ $i }} Ad Space</span>
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        {!!  Html::decode(Form::label('sec_'.$i.'_ad_position', 'Ad Position', ['class' => 'control-label']))  !!}
                        {{ Form::select('sec_'.$i.'_ad_position', config('site.adpos'), $page->getMeta('sec_'.$i.'_ad_position'), ['class'=>'form-control']) }}
                        @if ($errors->has('sec_'.$i.'_ad_position'))
                            <span class="invalid-feedback animated fadeInDown">
                            {{ $errors->first('sec_'.$i.'_ad_position') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row mb-20">
                    <div class="col">
                        {!!  Html::decode(Form::label('sec_'.$i.'_category_id', 'Select Category', ['class' => 'control-label']))  !!}
                        {{ Form::select('sec_'.$i.'_category_id', [null=>'Select Category']+$categories, $page->getMeta('sec_'.$i.'_category_id'), ['class'=>'form-control']) }}
                        @if ($errors->has('sec_'.$i.'_category_id'))
                            <span class="invalid-feedback animated fadeInDown">
                            {{ $errors->first('sec_'.$i.'_category_id') }}
                            </span>
                        @endif
                    </div>
                    <div class="col">
                        {!!  Html::decode(Form::label('sec_2_style', 'Category Post Style', ['class' => 'control-label']))  !!}
                        {{ Form::select('sec_'.$i.'_style', ['style-1'=> 'Style I', 'style-2'=> 'Style II','style-3'=> 'Style III', 'style-4'=> 'Style IV'], $page->getMeta('sec_'.$i.'_style'), ['class'=>'form-control']) }}
                        @if ($errors->has('sec_'.$i.'_style'))
                            <span class="invalid-feedback animated fadeInDown">
                            {{ $errors->first('sec_'.$i.'_style') }}
                            </span>
                        @endif
                    </div>

                    <div class="col">
                        {!!  Html::decode(Form::label('sec_2_posts_per_page', 'No Of Posts', ['class' => 'control-label']))  !!}
                        {{ Form::number('sec_'.$i.'_posts_per_page',  $page->getMeta('sec_'.$i.'_posts_per_page'), ['class'=>'form-control']) }}
                        @if ($errors->has('sec_'.$i.'_posts_per_page'))
                            <span class="invalid-feedback animated fadeInDown">
                            {{ $errors->first('sec_'.$i.'_posts_per_page') }}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>
<div class="col-md-4">
    <div class="form-group {{ $errors->has('template')?' is-invalid':'' }}">
        {!! Html::decode(Form::label('template', __('site.page.page_template'), ['class' => 'control-label']))  !!}
        {{ Form::select('template',config('site.page_templates'),$page->template, ['class'=>'form-control js-select2']) }}
        @if ($errors->has('template'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('template') }}
            </div>
        @endif
    </div>
    <div class="form-group {{ $errors->has('meta_keywords')?' is-invalid':'' }}">
        {!!  Html::decode(Form::label('meta_keywords', __('site.meta_keywords'), ['class' => 'control-label']))  !!}
        {!! Form::text('meta_keywords',old('meta_keywords'),['class'=>'form-control','placeholder'=>__('site.meta_keywords')]) !!}
        @if ($errors->has('meta_keywords'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('meta_keywords') }}
            </div>
        @endif
    </div>
    <div class="form-group {{ $errors->has('meta_description')?' is-invalid':'' }}">
        {!!  Html::decode(Form::label('meta_description', __('site.meta_description'), ['class' => 'control-label']))  !!}
        {!! Form::textarea('meta_description',old('meta_description'),['class'=>'form-control','placeholder'=>__('site.meta_description_info')]) !!}
        @if ($errors->has('meta_description'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('meta_description') }}
            </div>
        @endif
    </div>
</div>
