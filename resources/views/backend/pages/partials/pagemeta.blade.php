<div class="row">
    @if($page->slug && config('site.pagemeta.'.$page->slug))
        @foreach(config('site.pagemeta.'.$page->slug) as $metas)
            <div class="{{ isset($metas['column'])?$metas['column']:'col-md-6' }} form-group {{$errors->has($metas['name']) ? 'has-error':''}}">
                {!! Html::decode(Form::label($metas['name'], $metas['text'], ['class' => 'control-label'])) !!}
                {!! meta_view($metas, $page) !!}
                <div class="form-text text-muted">{{ isset($metas['info'])?$metas['info']:'' }}</div>
                @if ($errors->has($metas['name']))
                    <span class="help-block">
                        <strong>{{ $errors->first($metas['name']) }}</strong>
                    </span>
                @endif
            </div>
        @endforeach
    @endif
</div>
