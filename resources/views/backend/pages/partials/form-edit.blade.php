@if(count($errors)>0)
  <div class="col-md-12">
    <div class="alert-icon alert alert-danger" role="alert">
      <i class="fa fa-times-circle-o"></i>  Something went wrong. Please check below
    </div>
  </div>
@endif
<div class="col-md-8">
    <div class="form-body">
        <div class="form-group {{$errors->has('title') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('title', __('site.title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('title',$page->title,['class'=>'form-control','placeholder'=> __('site.title')]) !!}
            @if ($errors->has('title'))
                <span class="error label-error">
                    {{ $errors->first('title') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('slug') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('slug',  __('site.slug').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('slug',$page->slug,['class'=>'form-control','placeholder'=> __('site.slug')]) !!}
            @if ($errors->has('slug'))
                <span class="error label-error">
                    {{ $errors->first('slug') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('subtitle') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('subtitle',  __('site.subtitle'), ['class' => 'control-label']))  !!}
            {!! Form::text('subtitle',$page->subtitle,['class'=>'form-control','placeholder'=> __('site.subtitle')]) !!}
            @if ($errors->has('subtitle'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('subtitle') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('content') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('content',  __('site.content'), ['class' => 'control-label']))  !!}
            {!! Form::textarea('content',$page->content,['class'=>'form-control','id'=>'js-ckeditor']) !!}
            @if ($errors->has('content'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('content') }}
                </span>
            @endif
        </div>
        @include('backend.pages.partials.pagemeta')
    </div>
</div>
<div class="col-md-4">
    @include('backend._macros.media_picker', [
        'media_id' => $page->featured_image?$page->media->id:null,
        'media_url' => $page->featured_image?url($page->media->preview_url):null,
        'input_name' => 'featured_image',
        'input_label' => __('site.featured_image'),
        'data_input_id' => 'thumbnail-id',
        'data_input_url' => 'thumbnail-url',
        'data_input_url_name' => 'featured_image_url',
        'data_holder' => 'holder'
    ])
    <div class="form-group {{ $errors->has('template')?' is-invalid':'' }}">
        {!! Html::decode(Form::label('template', __('site.page.page_template'), ['class' => 'control-label']))  !!}
        {{ Form::select('template',config('site.page_templates'),$page->template, ['class'=>'form-control js-select2']) }}
        @if ($errors->has('template'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('template') }}
            </div>
        @endif
    </div>

    @if($page->template == 'contact')
        <div class="form-group {{ $errors->has('form_title')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('form_title', __('site.page.contact_form_title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('form_title',$page->getMeta('form_title'),['class'=>'form-control','placeholder'=>__('site.page.contact_form_title'), 'required']) !!}
            @if ($errors->has('form_title'))
                <span class="invalid-feedback animated fadeInDown">
                {{ $errors->first('form_title') }}
            </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('map_iframe')?' is-invalid':'' }}">
            {!!  Html::decode(Form::label('map_iframe', __('site.page.map_iframe'), ['class' => 'control-label']))  !!}
            {!! Form::text('map_iframe',$page->getMeta('map_iframe'),['class'=>'form-control','placeholder'=>__('site.page.map_iframe')]) !!}
            @if ($errors->has('map_iframe'))
                <span class="invalid-feedback animated fadeInDown">
                {{ $errors->first('map_iframe') }}
            </span>
            @endif
        </div>
    @endif
    <div class="form-group {{ $errors->has('meta_keywords')?' is-invalid':'' }}">
        {!!  Html::decode(Form::label('meta_keywords', __('site.meta_keywords').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
        {!! Form::text('meta_keywords',$page->getMeta('meta_keywords'),['class'=>'form-control','placeholder'=>__('site.meta_keywords_info')]) !!}
        @if ($errors->has('meta_keywords'))
            <span class="invalid-feedback animated fadeInDown">
                {{ $errors->first('meta_keywords') }}
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('meta_description')?' is-invalid':'' }}">
        {!!  Html::decode(Form::label('meta_description', __('site.meta_description').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
        {!! Form::textarea('meta_description',$page->getMeta('meta_description'),['class'=>'form-control','placeholder'=>__('site.meta_description_info')]) !!}
        @if ($errors->has('meta_description'))
            <span class="invalid-feedback animated fadeInDown">
                {{ $errors->first('meta_description') }}
            </span>
        @endif
    </div>
</div>
