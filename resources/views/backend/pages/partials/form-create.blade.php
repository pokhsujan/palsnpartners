@if(count($errors)>0)
  <div class="col-md-12">
    <div class="alert-icon alert alert-danger" role="alert">
      <i class="fa fa-times-circle-o"></i>  Something went wrong. Please check below
    </div>
  </div>
@endif
<div class="col-md-8">
    <div class="form-body">
        <div class="form-group {{$errors->has('title') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('title', __('site.title').' <span class="required"> * </span>', ['class' => 'control-label']))  !!}
            {!! Form::text('title',old('title'),['class'=>'form-control','placeholder'=>__('site.title')]) !!}
            @if ($errors->has('title'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('title') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('subtitle') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('subtitle', __('site.subtitle'), ['class' => 'control-label']))  !!}
            {!! Form::text('subtitle',old('subtitle'),['class'=>'form-control','placeholder'=>__('site.subtitle')]) !!}
            @if ($errors->has('subtitle'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('subtitle') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('content') ? 'is-invalid':''}}">
            {!!  Html::decode(Form::label('content',  __('site.content'), ['class' => 'control-label']))  !!}
            {!! Form::textarea('content',old('content'),['class'=>'form-control','id'=>'js-ckeditor']) !!}
            @if ($errors->has('content'))
                <span class="invalid-feedback animated fadeInDown">
                    {{ $errors->first('content') }}
                </span>
            @endif
        </div>
    </div>
</div>
<div class="col-md-4">
    @include('backend._macros.media_picker', [
        'media_id' => null,
        'media_url' => null,
        'input_name' => 'featured_image',
        'input_label' => __('site.featured_image'),
        'data_input_id' => 'thumbnail-id',
        'data_input_url' => 'thumbnail-url',
        'data_input_url_name' => 'featured_image_url',
        'data_holder' => 'holder'
    ])
    <div class="form-group {{ $errors->has('template')?' is-invalid':'' }}">
        {!! Html::decode(Form::label('template', __('site.page.page_template'), ['class' => 'control-label']))  !!}
        {{ Form::select('template',config('site.page_templates'),old('template'), ['class'=>'form-control js-select2']) }}
        @if ($errors->has('template'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('template') }}
            </div>
        @endif
    </div>

    <div class="form-group {{ $errors->has('meta_keywords')?' is-invalid':'' }}">
        {!!  Html::decode(Form::label('meta_keywords', __('site.meta_keywords'), ['class' => 'control-label']))  !!}
        {!! Form::text('meta_keywords',old('meta_keywords'),['class'=>'form-control','placeholder'=>__('site.meta_keywords')]) !!}
        @if ($errors->has('meta_keywords'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('meta_keywords') }}
            </div>
        @endif
    </div>
    <div class="form-group {{ $errors->has('meta_description')?' is-invalid':'' }}">
        {!!  Html::decode(Form::label('meta_description', __('site.meta_description'), ['class' => 'control-label']))  !!}
        {!! Form::textarea('meta_description',old('meta_description'),['class'=>'form-control','placeholder'=>__('site.meta_description_info')]) !!}
        @if ($errors->has('meta_description'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('meta_description') }}
            </div>
        @endif
    </div>
</div>
