@extends('backend.layout')
@section('title') {{ __('site.widget.widgets') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-navicon rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.widget.widgets') }} - {{ __('site.widget.sidebar_list') }} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                <tr class="uppercase">
                    <th class="text-center">#</th>
                    <th>{{ __('site.widget.widget_type') }}</th>
                    <th class="text-center">{{ __('site.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach(config('site.sidebars') as $sidebar_id => $sidebar_name)
                    <tr>
                        <td class="text-center">#</td>
                        <td>{{ $sidebar_name }} </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{route('widgets.edit','widget_type='.$sidebar_id)}}" class="btn btn-sm btn-icon-o btn-light mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('site.edit') }}"><i class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                            </div>
                        </td>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
