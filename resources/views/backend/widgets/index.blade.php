@extends('backend.layout')
@section('title') {{ __('site.widget.widgets') }} @endsection
@push('styles')
@endpush
@section('content')
  <div class="page-subheader mb-3">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-7">
          <div class="list">
            <i class="fa fa-navicon rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
            <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.widget.widgets') }} - {{ __('site.widget.edit_sidebar_widgets') }}</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content d-flex flex">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4 mb-30">
          <div class="card widgets-lists mb-40">
            <div class="card-header">
              <h4 class="title mb-0 flex mr-auto order-0">{{ __('site.widget.widgets') }}</h4>
            </div>
            <div class="card-body">
              <div data-toggle="slimscroll" data-color="#42a5f5" data-opacity="1" data-always-visible="true">
                <div class="custom-controls-stacked">
                  @foreach(config('site.widgets') as $widget_id => $widget_name)
                    <label class="custom-control custom-checkbox checkbox-success">
                      {{ Form::checkbox('widgets[]',$widget_name, null,['data-widget_id'=>$widget_id, 'class'=>'custom-control-input']) }}
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">{{ $widget_name }}</span>
                    </label>
                    <br>
                  @endforeach
                  @foreach($adspaces as $widget)
                      <label class="custom-control custom-checkbox checkbox-success">
                        {{ Form::checkbox('widgets[]',$widget->name, null,['data-widget_id'=>$widget->ad_position, 'class'=>'custom-control-input']) }}
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">{{ $widget->name }}</span>
                      </label>
                      <br>
                  @endforeach
                </div>
              </div>
              <hr>
              <a class="btn btn-sm  btn-primary add-to-sidebar float-right" href="javascript:void(0);">{{ __('site.widget.add_to_sidebar') }} </a>
            </div>
          </div>
        </div>
        <div class="col-md-8 mb-30">
          <div class="card widget-edit-block">
            <div class="card-header">
              <h4 class="title mb-0 flex mr-auto order-0">{{ __('site.widget.sidebar') }} : <span class="text-primary">{{ config('site.sidebars.'.$widget_type) }}</span> </h4>
              <a class="btn btn-outline-primary btn-sm" href="{{ route('widgets.index') }}">
                <i class="fa fa-angle-left"></i> {{ __('site.widget.back_to_sidebar_lists') }}
              </a>
            </div>
            <div class="card-body">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              {{ Form::hidden('sidebar_type',$widget_type) }}
              <div class="dd" id="widgets">
                <ol id="dd-widget-list" class="dd-list">
                  @if($widgets)
                  @foreach ($widgets as $widget)
                    <li class="dd-item" data-widget_id="{{ $widget->widget_id }}" data-title="{{ $widget->title }}"  data-sidebar_type="{{ $widget->sidebar_type }}">
                      <div class="dd-handle">{{ $widget->title }}</div>
                      <span class="widget-addons pull-right"><a class="widget-button-edit text-primary"><i class="fa fa-edit"></i></a><a class="button-delete text-danger"><i class="fa fa-trash"></i></a></span>
                    </li>
                  @endforeach
                  @endif
                </ol>
              </div>
              <hr class="mt-4">
              <a class="btn btn-sm btn-square btn-success save-widget float-right" href="javascript:void(0);">{{ __('site.save') }} {{ __('site.widget.widget') }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('modal')
  <div class="modal" id="edit-widget-modal" tabindex="-1" role="dialog" aria-labelledby="edit-widget-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ __('site.widget.edit_widget') }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="fa fa-times"></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {{Form::text('title',old('title'),['class'=>'form-control','placeholder'=>__('site.widget.widget_title'), 'id'=>'widget-title'])}}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-success" class="close" data-dismiss="modal" aria-label="Close">
            <i class="fa fa-check"></i> {{ __('site.save') }}
          </button>
        </div>
      </div>
    </div>
  </div>
  <!-- END Normal Modal -->
@endpush
@push('scripts')
@endpush
