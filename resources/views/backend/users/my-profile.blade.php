@extends('backend.layout')
@section('title') {{ __('site.user.my_profile') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-users rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.user.my_profile') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right">
                        <a class="btn btn-outline-dark btn-sm" href="{{ route('users.mylogs') }}"><i class="fa fa-history"></i> {{ __('site.user.my_logs') }}</a>
                        <a class="btn btn-outline-dark btn-sm" href="{{ route('users.profile.change.password') }}"><i class="fa fa-lock"></i> {{ __('site.user.change_password') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="container-fluid">
            {!! Form::open(array('route' => 'users.profile',  'method' => 'POST', 'role' => 'form',  'required' => 'required')) !!}
            <div class="title-sep sep-white text-left text-primary mb-10">
                <span class="rounded">{{ __('site.user.update_my_profile') }}</span>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group row">
                        <div class="col-6">
                            {!! Form::label('username', __('site.username')) !!}
                            {!! Form::text('username',$user->username, array('class' => 'form-control', 'placeholder' => __('site.username'),'disabled' => 'disabled')) !!}
                        </div>
                        <div class="col-6">
                            {!! Form::label('email', __('site.email')) !!}
                            {!! Form::text('email',$user->email, array('class' => 'form-control', 'placeholder' => __('site.email'),'disabled' => 'disabled')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('full_name', __('site.full_name')) !!}
                        {!! Form::text('full_name',is_null($user->profile)?null:$user->profile->full_name, array('class' => 'form-control', 'placeholder' => 'Full Name')) !!}
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group row">
                        <div class="col-12">
                            {!! Html::decode(Form::label('featured_image', __('site.avatar'), ['class' => 'control-label']))  !!}
                            {{ Form::text('avatar',(!is_null($user->profile) && !is_null($user->profile->avatar))?$user->profile->avatar:Gravatar::src($user->email),['class'=>'form-control', 'id'=>'thumbnail']) }}
                            <img id="holder" style="margin-top:15px;max-height:100px;"
                                 src="{{ (!is_null($user->profile) && !is_null($user->profile->avatar))?$user->profile->avatar:Gravatar::src($user->email) }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group row">
                        <div class="col-12">
                            {!! Form::label('display_name', __('site.biographical_info')) !!}
                            {{ Form::textarea('description', is_null($user->profile)?null:$user->profile->description, array('class' => 'form-control', 'placeholder' => __('site.user.few_details_about_yourself'))) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('nickname', __('site.user.nickname')) !!}
                        {!! Form::text('nickname',is_null($user->profile)?null:$user->profile->nickname, array('class' => 'form-control', 'placeholder' => __('site.user.nickname'))) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('display_name', __('site.user.display_name')) !!}
                        {!! Form::text('display_name',is_null($user->profile)?null:$user->profile->display_name, array('class' => 'form-control', 'placeholder' =>__('site.user.display_name'))) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('website', __('site.user.website_url')) !!}
                        {!! Form::text('website',is_null($user->profile)?null:$user->profile->website, array('class' => 'form-control', 'placeholder' => __('site.user.website_url'))) !!}
                    </div>
                    <div class="form-group mb-0">
                        <label>Social Media</label>
                        <div class="row">
                            <div class="col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-facebook"></i></span>
                                    </div>
                                    {!! Form::text('facebook_url',$user->getMeta('facebook_url'), array('class' => 'form-control', 'placeholder' => __('site.facebook_url'))) !!}
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"><i
                                                class="fa fa-twitter"></i></span>
                                    </div>
                                    {!! Form::text('twitter_url',$user->getMeta('twitter_url'), array('class' => 'form-control', 'placeholder' => __('site.twitter_url'))) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon4"><i
                                                class="fa fa-youtube"></i></span>
                                    </div>
                                    {!! Form::text('youtube_url',$user->getMeta('youtube_url'), array('class' => 'form-control', 'placeholder' => __('site.youtube_url'))) !!}
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon3"><i
                                                    class="fa fa-instagram"></i></span>
                                    </div>
                                    {!! Form::text('instagram_url',$user->getMeta('instagram_url'), array('class' => 'form-control', 'placeholder' => __('site.instagram_url'))) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-outline-success btn-sm">
                        <i class="fa fa-check"></i> {{ __('site.save') }} {{ __('site.user.profile') }}
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@push('scripts')
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $('#lfm').filemanager('image');
    </script>
@endpush

