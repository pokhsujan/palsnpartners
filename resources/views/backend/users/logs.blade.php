@extends('backend.layout')
@section('title') {{ __('site.user.users') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-users rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.user.users') }} - {{ __('site.user.user_log') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <a class="btn btn-outline-primary btn-sm float-right" href="{{ route('users.index') }}"><i
                            class="fa fa-angle-left"></i> {{ __('site.user.back_to_users_list') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                <tr class="uppercase">
                    <th>#</th>
                    <th>{{ __('site.user.ip') }}</th>
                    <th>{{ __('site.user.user_agent') }}</th>
                    <th>{{ __('site.user.logged_in_at') }}</th>
                    <th>{{ __('site.user.logged_out_at') }}</th>
                    <th>{{ __('site.user.active_for') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($userlogs as $userlog)
                    <tr>
                        <td>#</td>
                        <td>{{$userlog->ip_address}}</td>
                        <th>{{ browserInfo($userlog->user_agent) }}</th>
                        <th>{{$userlog->login_at->format('l, d M Y g:i a')}}</th>
                        <th>{{ is_null($userlog->logout_at)?'Logged In':$userlog->logout_at->format('l, d M Y g:i a')}}</th>
                        <th>{{ is_null($userlog->logout_at)?\Carbon\Carbon::parse($userlog->login_at)->diffForHumans(\Carbon\Carbon::now(), true):\Carbon\Carbon::parse($userlog->login_at)->diffForHumans($userlog->logout_at, true)}}</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
