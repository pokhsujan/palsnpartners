@extends('backend.layout')
@section('title') {{ __('site.user.users') }} @endsection
@section('content')
    <div class="page-subheader mb-0">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-users rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.user.users') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="btn-group float-right">
                        <a href="{{ route('users.online') }}" class="btn btn-sm btn-outline-success"><i class="fa fa-users"></i> {{ __('site.user.online_users') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex no-padding">
        <div class="flex d-flex">
            <div class="sidebar sidebar-sm bg-white" id="sidebar-collapse">
                <div class="d-flex flex-column b-r">
                    <div class="navbar b-b">
                        {{ __('site.user.users') }} - {{ __('site.user.new_user') }}
                    </div>
                    <div class="flex p-3">
                        @include('backend.users.partials.create')
                    </div>
                </div>
            </div><!-- sidebar end-->
            <div class="sidebar-body flex d-flex" id="app-body">
                <div class="d-flex flex flex-column">
                    <div class="navbar bg-white align-items-center b-b">
                        {{ __('site.user.users') }} -  {{ __('site.user.list_users') }}
                    </div>
                    <div class="flex p-3">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr class="uppercase">
                                    <th>#</th>
                                    <th> {{ __('site.username') }}</th>
                                    <th>{{ __('site.email') }}</th>
                                    <th>{{ __('site.role.roles') }}</th>
                                    <th>{{ __('site.status') }}</th>
                                    <th class="text-center">{{ __('site.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>#</td>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->email}} {!! $user->hasVerifiedEmail()?'<i data-toggle="tooltip" data-original-title="'. __('site.user.verified') .'" class="fa fa-check-circle-o text-success"></i>':'<i data-toggle="tooltip" data-original-title="'. __('site.user.not_verified') .'" class="fa fa-times-circle-o text-danger"></i>'  !!} </td>
                                        <td>
                                            @if(!$user->roles->isEmpty())
                                                <span class="badge badge-text bg-info">
                                            {!! implode('</span> <span class="badge badge-text bg-info">', $user->roles->pluck('display_name')->toArray())  !!}
                                         </span>
                                            @endif
                                        </td>
                                        <td>{!! $user->status == \App\User::ACTIVE ? '<span class="badge badge-text bg-success">'. __('site.user.active_user') .'</span>':'<span class="badge badge-text bg-danger">'. __('site.user.banned_user') .'</span>' !!} </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                @if(auth()->user()->hasRole(['developer', 'admin']))
                                                    <a href="{{route('users.edit',$user->id)}}" class='btn btn-sm btn-icon-o btn-light mr-1 '><i class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                                                @endif
                                                <a href="{{route('users.logs',$user->id)}}" class='btn btn-sm btn-icon-o btn-light mr-1'><i class="fa fa-history text-secondary" aria-hidden="true"></i></a>
                                                @if(!$user->hasRole('developer'))
                                                    {!! Form::open(array('route' => ['users.destroy',$user->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                                                    {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light mr-1 ','type' => 'submit')) !!}
                                                    {!! Form::close()!!}
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $users->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
