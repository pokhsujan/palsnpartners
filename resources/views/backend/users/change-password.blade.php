@extends('backend.layout')
@section('title') {{ __('site.user.my_profile') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-users rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.user.my_profile') }}</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right">
                        <a class="btn btn-outline-dark btn-sm"  href="{{ route('users.profile') }}"><i class="fa fa-user-circle-o"></i> {{ __('site.edit') }} {{ __('site.user.profile') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="container-fluid">
            {!!Form::open(['route'=>'users.profile.update.password','method'=>'POST'])!!}
            <div class="title-sep sep-white text-left text-primary mb-10">
                <span class="rounded">{{ __('Change Password') }}</span>
            </div>
            <div class="row">
                <div class="col-md-7">
                            <div class="form-group {{$errors->has('password') ? 'is-invalid':''}}">
                                {!! Form::label('password', 'Password') !!}
                                {!! Form::text('password',null, array('class' => 'form-control', 'placeholder' => __('site.password'))) !!}
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback animated fadeInDown">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                    <div class="form-group {{$errors->has('password_confirmation') ? 'is-invalid':''}}">
                        {!! Form::label('password_confirmation', 'Confirm Password') !!}
                        {!! Form::text('password_confirmation',null, array('class' => 'form-control', 'placeholder' => __('site.confirm_password'))) !!}
                        @if ($errors->has('password_confirmation'))
                            <div class="invalid-feedback animated fadeInDown">
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-success btn-sm">
                        <i class="fa fa-check"></i> {{ __('site.change_password') }}
                    </button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

