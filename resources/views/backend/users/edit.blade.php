@extends('backend.layout')
@section('title') {{ __('site.user.users') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-users rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.user.users') }} - {{ __('site.user.update_user') }} <span class="text-dark-active text-lowercase">"{{ $user->username }}"</span></span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="pull-right">
                        @if($user->status == 0)
                        <a class="btn btn-outline-success btn-sm" href="{{ route('users.active', $user->id) }}">{{ __('site.user.active') }} {{ __('site.user.user') }}</a>
                        @else
                        <a class="btn btn-outline-danger btn-sm" href="{{ route('users.suspend', $user->id) }}">{{ __('site.user.block') }} {{ __('site.user.user') }}</a>
                        @endif
                        <a class="btn btn-outline-primary btn-sm" href="{{ route('users.index') }}"><i class="fa fa-angle-left"></i> {{ __('site.user.back_to_users_list') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="container-fluid">
            @include('backend.users.partials.edit')
        </div>
    </div>
@endsection
@push('scripts')

@endpush

