@extends('backend.layout')
@section('title') {{ __('site.permission.permissions') }} @endsection
@section('content')
    <div class="page-subheader mb-0">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-th-list rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.permission.permissions') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex no-padding">
        <div class="flex d-flex">
            <div class="sidebar sidebar-sm bg-white" id="sidebar-collapse">
                <div class="d-flex flex-column b-r">
                    <div class="navbar b-b">
                        {{ __('site.permission.permissions') }} - {{ __('site.permission.new_permission') }}
                    </div>
                    <div class="flex p-3">
                        {!! Form::open(array('route' => 'permissions.store',  'method' => 'POST', 'role' => 'form',  'required' => 'required')) !!}
                        <div class="form-group {{$errors->has('display_name') ? 'is-invalid':''}}">
                            {!! Html::decode(Form::label('display_name', __('site.display_name').' <span class="required"> * </span>')) !!}
                            {!! Form::text('display_name',old('display_name'), array('class' => 'form-control', 'placeholder' => __('site.display_name'))) !!}
                            @if ($errors->has('display_name'))
                                <div class="invalid-feedback animated fadeInDown">
                                    {{ $errors->first('display_name') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group {{$errors->has('name') ? 'is-invalid':''}}">
                            {!! Html::decode(Form::label('name', __('site.permission.permission_name').' <span class="required"> * </span>')) !!}
                            {!! Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => __('site.permission.permission_name'))) !!}
                            @if ($errors->has('name'))
                                <div class="invalid-feedback animated fadeInDown">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group {{$errors->has('description') ? 'is-invalid':''}}">
                            {!! Html::decode(Form::label('description', __('site.description').' <span class="required"> * </span>')) !!}
                            {!! Form::textarea('description',old('description'), array('class' => 'form-control', 'placeholder' => __('site.description'))) !!}
                            @if ($errors->has('description'))
                                <div class="invalid-feedback animated fadeInDown">
                                    {{ $errors->first('description') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success btn-block','type' => 'submit')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div><!-- sidebar end-->
            <div class="sidebar-body flex d-flex" id="app-body">
                <div class="d-flex flex flex-column">
                    <div class="navbar bg-white align-items-center b-b">
                        {{ __('site.permission.permissions') }} - {{ __('site.permission.all_permissions') }}
                    </div>
                    <div class="flex p-3">
                        <div class=" table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr class="uppercase">
                                    <th>#</th>
                                    <th>{{  __('site.display_name') }}</th>
                                    <th>{{  __('site.permission.permission_name') }}</th>
                                    <th>{{  __('site.description') }}</th>
                                    <th class="text-center">{{  __('site.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr>
                                        <td>#</td>
                                        <td>{{$permission->display_name}}</td>
                                        <td>{{$permission->name}}</td>
                                        <td>{{$permission->description }}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{route('permissions.edit',$permission->id)}}"
                                                   class='btn btn-sm btn-icon-o btn-light mr-1'><i
                                                        class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $permissions->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
