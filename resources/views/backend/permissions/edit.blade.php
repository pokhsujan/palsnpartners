@extends('backend.layout')
@section('title') {{ __('site.permission.permissions') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-th-list rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">  {{ __('site.permission.permissions') }} - {{ __('site.permission.update_permission') }} <span class="badge badge-primary badge-text">{{ $permission->name }}</span></span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right">
                        <a class="btn btn-outline-primary btn-sm" href="{{ route('permissions.index') }}"><i class="fa fa-angle-left"></i> {{ __('site.permission.back_to_permissions_list') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content d-flex flex">
        <div class="container-fluid">
            {!! Form::open(array('route' => ['permissions.update', $permission->id],  'method' => 'PUT', 'role' => 'form',  'required' => 'required')) !!}
            <div class="form-group {{$errors->has('display_name') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('display_name', __('site.display_name').' <span class="required"> * </span>')) !!}
                {!! Form::text('display_name',$permission->display_name, array('class' => 'form-control', 'placeholder' => __('site.display_name'))) !!}
                @if ($errors->has('display_name'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('display_name') }}
                    </div>
                @endif
            </div>

            <div class="form-group {{$errors->has('name') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('name', __('site.permission.permission_name').' <span class="required"> * </span>')) !!}
                {!! Form::text('name',$permission->name, array('class' => 'form-control', 'placeholder' => __('site.permission.permission_name'),'disabled'=>'disabled')) !!}
                @if ($errors->has('name'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('name') }}
                    </div>
                @endif
            </div>

            <div class="form-group {{$errors->has('description') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('description', __('site.description').' <span class="required"> * </span>')) !!}
                {!! Form::textarea('description',$permission->description, array('class' => 'form-control', 'placeholder' => __('site.description'))) !!}
                @if ($errors->has('description'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('description') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success','type' => 'submit')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
