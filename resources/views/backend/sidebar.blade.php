<aside id="page-aside" class=" in-aside aside-fixed">
    <div class="in-nav in-nav-light">
{{--                <a href="{{ route('dashboard') }}" class="p-1 w100 in-logo d-flex flex flex-row align-items-center">--}}
{{--                    <img src="{{ setting('backend_logo', url('images/logo.png')) }}">--}}
{{--                </a>--}}
        <p>Nexco Portall</p>
        <div class="flex">
            <div class="aside-content slim-scroll">
                <ul class="metisMenu" id="metisMenu">
                    <li>
                        <i class="fa fa-home nav-thumbnail"></i>
                        <a href="{{ route('dashboard') }}" {!! classActivePath('admin/dashboard') !!}>
                            <span class="nav-text">
                                {{__('site.dashboard.dashboard')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @permission('manage-media')
                    <li>
                        <i class="fa fa-file-picture-o nav-thumbnail"></i>
                        <a href="{{ route('media.index') }}" {!! classActivePath('admin/media/*') !!}>
                            <span class="nav-text">
                                 {{__('site.media.media')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endpermission
                   {{-- @permission('manage-ads')
                    <li {!! classActivePath(['admin/ads*'],'active') !!}>
                        <i class="fa fa-file-o nav-thumbnail"></i>
                        <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                            <span class="nav-text"> {{__('site.ad.ads')}}</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a {!! classActivePath('admin/ads/create') !!} href="{{ route('ads.create') }}"> {{__('site.add_new')}}</a>
                            </li>
                            <li>
                                <a {!! classActivePath('admin/ads') !!} href="{{ route('ads.index') }}"> {{__('site.view_all')}}</a>
                            </li>
                            <li>
                                <a {!! classActivePath('admin/adspaces') !!} href="{{ route('adspaces.index') }}"> {{__('site.adspace.adspaces')}}</a>
                            </li>
                        </ul>
                    </li><!--Menu-item-->
                    @endpermission--}}
                    @permission('manage-pages')
                    <li {!! classActivePath(['admin/pages*'],'active') !!}>
                        <i class="fa fa-file-o nav-thumbnail"></i>
                        <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                            <span class="nav-text"> {{__('site.page.pages')}}</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a {!! classActivePath('admin/pages/create') !!} href="{{ route('pages.create') }}">{{__('site.add_new')}}</a>
                            </li>
                            <li>
                                <a {!! classActivePath('admin/pages') !!} href="{{ route('pages.index') }}">{{__('site.view_all')}}</a>
                            </li>

                        </ul>
                    </li><!--Menu-item-->
                    @endpermission
                    {{--@permission('manage-posts')
                    <li {!! classActivePath('admin/comments*','active') !!}>
                        <i class="fa fa-comment-o nav-thumbnail"></i>
                        <a  {!! classActivePath('admin/comments*') !!} href="{{ route('comments.index')  }}" >
                            <span class="nav-text">{{__('site.comment.comments')}}</span>
                        </a>
                    </li><!--Menu-item-->

                    @endpermission
                    @permission('manage-posts')
                    <li {!! classActivePath(['admin/posts*','admin/categories*','admin/tags*'],'active') !!}>
                        <i class="fa fa-file-text-o nav-thumbnail"></i>
                        <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                            <span class="nav-text">{{__('site.post.posts')}}</span>
                        </a>
                        <ul aria-expanded="false">
                            <li>
                                <a {!! classActivePath('admin/posts/create') !!} href="{{ route('posts.create') }}">{{__('site.add_new')}}</a>
                            </li>
                            <li>
                                <a {!! classActivePath('admin/posts') !!} href="{{ route('posts.index') }}">{{__('site.view_all')}}</a>
                            </li>
                            <li>
                                <a {!! classActivePath('admin/categories*') !!} href="{{ route('categories.index') }}">{{__('site.category.categories')}}</a>
                            </li>
                            <li>
                                <a {!! classActivePath('admin/tags*') !!} href="{{ route('tags.index') }}">{{__('site.tag.tags')}}</a>
                            </li>
                        </ul>
                    </li><!--Menu-item-->
                    @endpermission
                    @permission('manage-menus')
                    <li>
                        <i class="fa fa-navicon nav-thumbnail"></i>
                        <a {!! classActivePath('admin/menus*') !!} href="{{ route('menus.index') }}">
                            <span class="nav-text">
                                {{__('site.menu.menus')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endpermission
                    @permission('manage-widgets')
                    <li>
                        <i class="fa fa-newspaper-o nav-thumbnail"></i>
                        <a href="{{ route('widgets.index') }}" {!! classActivePath('admin/widgets*') !!}>
                            <span class="nav-text">
                                {{__('site.widget.widgets')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endpermission
                    @role('admin')
                    <li>
                        <i class="fa fa-envelope-o nav-thumbnail"></i>
                        <a href="{{ route('messages.index') }}" {!! classActivePath('admin/messages*') !!}>
                            <span class="nav-text">
                                {{__('site.message.messages')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endrole
                    @role('admin')
                    <li>
                        <i class="fa fa-flag-checkered nav-thumbnail"></i>
                        <a href="{{ route('subscribers.index') }}" {!! classActivePath('admin/subscribers*') !!}>
                            <span class="nav-text">
                                {{__('site.subscriber.subscribers')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endrole--}}
                    @permission('manage-users')
                    <li>
                        <i class="fa fa-user-secret nav-thumbnail"></i>
                        <a {!! classActivePath('admin/users*') !!} href="{{ route('users.index') }}">
                            <span class="nav-text">
                                {{__('site.user.users')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endpermission
                    @role(['developer'])
                    <li>
                        <i class="fa fa-th-large nav-thumbnail"></i>
                        <a {!! classActivePath('admin/roles*') !!} href="{{ route('roles.index') }}">
                            <span class="nav-text">
                                {{__('site.role.roles')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    <li>
                        <i class="fa fa-th-list nav-thumbnail"></i>
                        <a {!! classActivePath('admin/permissions*') !!} href="{{ route('permissions.index') }}">
                            <span class="nav-text">
                                {{__('site.permission.permissions')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endrole
                    @role(['admin','developer'])
                    <li>
                        <i class="fa fa-gear nav-thumbnail"></i>
                        <a {!! classActivePath('admin/settings*') !!}  href="{{ route('settings.index') }}">
                            <span class="nav-text">
                                {{__('site.setting.settings')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endrole
                    @role(['user'])
                    <li>
                        <i class="fa fa-user nav-thumbnail"></i>
                        <a {!! classActivePath('my-profile*') !!}  href="{{ route('users.profile') }}">
                            <span class="nav-text">
                                {{__('site.user.my_profile')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    <li>
                        <i class="fa fa-gear nav-thumbnail"></i>
                        <a {!! classActivePath('my-profile/logs') !!}  href="{{ route('users.mylogs') }}">
                            <span class="nav-text">
                                {{__('site.user.my_logs')}}
                            </span>
                        </a>
                    </li><!--Menu-item-->
                    @endrole
                </ul>
            </div><!-- aside content end-->
        </div><!-- aside hidden scroll end-->
        <div class="aside-footer p-3 pl-25">
            <div class="fs12 font500 text-muted">
                © {{ date('Y') }}, {{ setting('site_name', config('site.site_name')) }}
            </div>
        </div><!-- aside footer end-->
    </div><!-- dad-nav end-->
</aside><!-- in-aside end-->
