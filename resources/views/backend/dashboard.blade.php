@extends('backend.layout')
@section('title'){{ __('Dashboard') }} @endsection
@section('content')
    <div class="page-content d-flex flex mt-10">
        <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
                   @include('backend._macros.session_message')
               </div>
                @role(['admin', 'developer' ])
                <div class="col-md-12">
                   <div class="row">
                       <div class="col-xl-3 col-lg-4 col-md-6">
                           <div class="list bg-white border1  rounded">
                               <div class="list-item">
                                   <div class="list-thumb bg-dark-active avatar rounded-circle avatar50 text-white ">
                                       <i class="fa fa-file-text-o fs20"></i>
                                   </div>
                                   <div class="list-body text-right">
                                       <span class="list-title text-success">{{ \App\Post::published()->count() }}</span>
                                       <span class="list-content">{{ __('site.dashboard.published_posts') }}</span>
                                   </div>
                               </div>
                           </div>
                       </div><!--col-->
                       <div class="col-xl-3 col-lg-4 col-md-6 mb-3">
                           <div class="list bg-white border1 rounded">
                               <div class="list-item">
                                   <div class="list-thumb bg-info-active avatar rounded-circle avatar50 text-white ">
                                       <i class="fa fa-list-alt fs20"></i>
                                   </div>
                                   <div class="list-body text-right">
                                       <span class="list-title text-danger">{{ \App\Category::count() }}</span>
                                       <span class="list-content">{{ __('site.category.categories') }}</span>
                                   </div>
                               </div>
                           </div>
                       </div><!--col-->
                       <div class="col-xl-3 col-lg-4 col-md-6">
                           <div class="list bg-white border1 rounded">
                               <div class="list-item">
                                   <div class="list-thumb bg-success-active avatar rounded-circle avatar50 text-white ">
                                       <i class="fa fa-file-o fs20"></i>
                                   </div>
                                   <div class="list-body text-right">
                                       <span class="list-title text-primary">{{ \App\Ad::count() }}</span>
                                       <span class="list-content">{{ __('site.dashboard.running_ads') }} </span>
                                   </div>
                               </div>
                           </div>
                       </div><!--col-->
                       <div class="col-xl-3 col-lg-4 col-md-6">
                           <div class="list bg-white border1  rounded">
                               <div class="list-item">
                                   <div class="list-thumb bg-info avatar rounded-circle avatar50 text-white ">
                                       <i class="fa fa-user fs20"></i>
                                   </div>
                                   <div class="list-body text-right">
                                       <span class="list-title">{{ \App\User::whereStatus(1)->count() }}</span>
                                       <span class="list-content">{{ __('site.dashboard.active_users') }}
                                            </span>
                                   </div>
                               </div>
                           </div>
                       </div><!--col-->
                   </div>
                </div>
                @endrole
                @permission('manage-posts')
                <div class="col-lg-3">
                    <div class="card mb-10">
                        <div class="card-body text-center">
                            <div class="title-lg mb-20 text-secondary text-uppercase"><i class="fa fa-file-text-o"></i> {{ __('COURSES') }}</div>
                            <a href="{{ route('posts.index') }}" class="btn btn-sm btn-icon-o text-success"> {{ __('site.post.published_posts') }}</a>
                            <a href="{{ route('posts.create') }}" class="btn btn-sm btn-icon-o text-primary"> <i class="d-inline fa fa-plus"></i> {{ __('site.post.new_post') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card mb-10">
                        <div class="card-body text-center">
                            <div class="title-lg mb-20 text-secondary text-uppercase"><i class="fa fa-list-alt"></i> {{ __('site.category.categories') }}</div>
                            <a href="{{ route('categories.index') }}" class="btn btn-sm btn-icon-o text-success"> {{ __('site.view') }} {{ __('site.category.list_categories') }}</a>
                        </div>
                    </div>
                </div>
                @endpermission
                @permission('manage-pages')
                <div class="col-lg-3">
                    <div class="card mb-10">
                        <div class="card-body text-center">
                            <div class="title-lg mb-20 text-secondary text-uppercase"><i class="fa fa-file-o"></i> {{ __('site.page.pages') }}</div>
                            <a href="{{ route('pages.index') }}" class="btn btn-sm btn-icon-o text-success"> {{ __('site.view') }} {{ __('site.page.all_pages') }}</a>
                            <a href="{{ route('pages.create') }}" class="btn btn-sm btn-icon-o text-primary"> <i
                                    class="d-inline fa fa-plus"></i>  {{ __('site.page.new_page') }}</a>
                        </div>
                    </div>
                </div>
                @endpermission
                @permission('manage-ads')
                <div class="col-lg-3">
                    <div class="card mb-10">
                        <div class="card-body text-center">
                            <div class="title-lg mb-20 text-secondary text-uppercase"><i class="fa fa-file-o"></i> {{ __('site.ad.ads') }}</div>
                            <a href="{{ route('ads.index') }}" class="btn btn-sm btn-icon-o text-success">{{ __('site.view') }} {{ __('site.ad.ads') }}</a>
                            <a href="{{ route('ads.create') }}" class="btn btn-sm btn-icon-o text-primary"> <i
                                    class="d-inline fa fa-plus"></i>  {{ __('site.ad.new_ad') }}</a>
                        </div>
                    </div>
                </div>
                @endpermission
                <div class="col-lg-12">
                    <div class="card mb-10">
                        <div class="card-header"><h4 class="title mb-0 flex mr-auto order-0">{{ __('site.dashboard.recent_posts') }}</h4></div>
                        <div class="card-body table-responsive">
                            <table class="table table-sm">
                                <tbody>
                                @foreach($recentPosts as $post)
                                    <tr>
                                        <td><a href="{{route('posts.show',$post->slug)}}" target="_blank"
                                               class="text-title text-primary">{{$post->title}}</a></td>
                                        @permission('manage-posts')
                                        <td>{{$post->user->fullname}}</td>
                                        <td>{{$post->view_count}} views</td>
                                        <td class="text-center">
                                            <div class="btn-group">

                                                @if($post->status == 'publish')
                                                    <a href="{{route('posts.show',$post->slug)}}"
                                                       class="btn btn-sm btn-icon-o btn-light mr-1" target="_blank"
                                                       data-toggle="tooltip" title="" data-original-title="{{ __('site.preview') }}"><i
                                                            class="fa fa-eye text-dark" aria-hidden="true"></i></a>
                                                @endif
                                                @can('update', $post)
                                                    <a href="{{route('posts.edit',$post->id)}}"
                                                       class="btn btn-sm btn-icon-o btn-light mr-1"
                                                       data-toggle="tooltip" title="" data-original-title="{{ __('site.edit') }}"><i
                                                            class="fa fa-pencil text-primary-active"
                                                            aria-hidden="true"></i></a>
                                                @endcan
                                                @can('update', $post)
                                                    {!! Form::open(array('route' => ['posts.destroy',$post->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                                                    {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-square btn-sm btn-alt-danger','type' => 'submit')) !!}
                                                    {!! Form::close()!!}
                                                @endcan
                                            </div>
                                        </td>
                                        @endpermission
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card mb-10">
                        <div class="card-header"><h4 class="title mb-0 flex mr-auto order-0">{{ __('site.dashboard.popular_posts') }}</h4></div>
                        <div class="card-body table-responsive">
{{--                            <table class="table table-sm">--}}
{{--                                <tbody>--}}
{{--                                @foreach($popularPosts as $post)--}}
{{--                                    <tr>--}}
{{--                                        <td><a href="{{route('posts.show',$post->slug)}}" target="_blank" class="text-title text-primary">{{$post->title}}</a></td>--}}
{{--                                        @permission('manage-posts')--}}
{{--                                        <td>{{$post->user->fullname}}</td>--}}
{{--                                        <td>{{$post->view_count}} views</td>--}}
{{--                                        <td class="text-center">--}}
{{--                                            <div class="btn-group">--}}
{{--                                                @if($post->status == 'publish')--}}
{{--                                                    <a href="{{route('posts.show',$post->slug)}}"--}}
{{--                                                       class="btn btn-sm btn-icon-o btn-light mr-1" target="_blank"--}}
{{--                                                       data-toggle="tooltip" title="" data-original-title="{{ __('site.preview') }}"><i--}}
{{--                                                            class="fa fa-eye text-dark" aria-hidden="true"></i></a>--}}
{{--                                                @endif--}}
{{--                                                @can('update', $post)--}}
{{--                                                    <a href="{{route('posts.edit',$post->id)}}"--}}
{{--                                                       class="btn btn-sm btn-icon-o btn-light mr-1"--}}
{{--                                                       data-toggle="tooltip" title="" data-original-title="{{ __('site.edit') }}"><i--}}
{{--                                                            class="fa fa-pencil text-primary-active"--}}
{{--                                                            aria-hidden="true"></i></a>--}}
{{--                                                @endcan--}}
{{--                                                @can('update', $post)--}}
{{--                                                    {!! Form::open(array('route' => ['posts.destroy',$post->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}--}}
{{--                                                    {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-square btn-sm btn-alt-danger','type' => 'submit')) !!}--}}
{{--                                                    {!! Form::close()!!}--}}
{{--                                                @endcan--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
{{--                                        @endpermission--}}
{{--                                @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
                        </div>
                    </div>
                </div>
                @permission('manage-users')
                <div class="col-lg-12">
                    <div class="card mb-10">
                        <div class="card-header"><h4 class="title mb-0 flex mr-auto order-0">{{ __('site.dashboard.recent_registered_users') }}</h4></div>
                        <div class="card-body table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr class="uppercase">
                                    <th>#</th>
                                    <th>{{ __('site.username') }}</th>
                                    <th>{{ __('site.email') }}</th>
                                    <th>{{ __('site.role.roles') }}</th>
                                    <th>{{ __('site.status') }}</th>
                                    <th class="text-center">{{ __('site.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>#</td>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->email}} {!! $user->hasVerifiedEmail()?'<i data-toggle="tooltip" data-original-title="Verified" class="fa fa-check-circle-o text-success"></i>':'<i data-toggle="tooltip" data-original-title="Not Verified" class="fa fa-times-circle-o text-danger"></i>'  !!} </td>
                                        <td>
                                            @if(!$user->roles->isEmpty())
                                                <span class="badge badge-text bg-info">
                                            {!! implode('</span> <span class="badge badge-text bg-info">', $user->roles->pluck('display_name')->toArray())  !!}
                                         </span>
                                            @endif
                                        </td>
                                        <td>{!! $user->status == \App\User::ACTIVE ? '<span class="badge badge-text bg-success">Active</span>':'<span class="badge badge-text bg-danger">Banned</span>' !!} </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                @if(auth()->user()->hasRole(['developer', 'admin']))
                                                    <a href="{{route('users.edit',$user->id)}}" class='btn btn-sm btn-icon-o btn-light mr-1 '><i class="fa fa-pencil text-primary-active" aria-hidden="true"></i></a>
                                                @endif
                                                <a href="{{route('users.logs',$user->id)}}" class='btn btn-sm btn-icon-o btn-light mr-1'><i class="fa fa-history text-secondary" aria-hidden="true"></i></a>
                                                @if(!$user->hasRole('developer'))
                                                    {!! Form::open(array('route' => ['users.destroy',$user->id],  'method' => 'DELETE', 'role' => 'form',  'required' => 'required','class'=>'delete-form')) !!}
                                                    {!! Form::button('<i class="fa fa-trash text-danger-active"></i>', array('class' => 'btn btn-sm btn-icon-o btn-light mr-1 ','type' => 'submit')) !!}
                                                    {!! Form::close()!!}
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endpermission
                @permission('manage-posts')
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="title mb-0 flex mr-auto order-0">{{ __('site.dashboard.post_monthly_views') }}</h4>
                        </div>
                        <div class="card-body">
                            <canvas class="js-chartjs-lines"></canvas>
                        </div>
                    </div>
                </div>
                @endpermission
            </div>
        </div>
    </div>
@endsection
