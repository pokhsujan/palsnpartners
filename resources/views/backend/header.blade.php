<header class="navbar in-header in-header-light bg-white navbar-expand-lg">
    <ul class="nav flex-row mr-auto">
        <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link dad-nav-btn h-lg-down">
                <i class="fa fa-navicon"></i>
            </a>
            <a class="nav-link dad-nav-btn h-lg-up" href="#page-aside" data-toggle="dropdown" data-target="#page-aside">
                <i class="fa fa-navicon"></i>
            </a>
        </li>
    </ul>
    <ul class="nav flex-row order-lg-2 ml-auto nav-icons">
        <li>
            <a href="{{ url('/') }}" target="_blank" class="text-dark">
                            <span class="nav-text">
                                <i class="fa fa-desktop nav-thumbnail"></i> {{ __('site.visit_site') }}
                            </span>
            </a>
        </li>
        <li class="nav-item dropdown user-dropdown align-items-center">
            <a class="nav-link" href="#" id="dropdown-user" role="button" data-toggle="dropdown">
                {{ __('site.welcome') }}, {{ auth()->user()->username }}
                <span class="user-states states-online ml-2">
                    @if(!is_null(auth()->user()->profile) && auth()->user()->profile->avatar)
                        <img class="img-fluid rounded-circle" src="{{ auth()->user()->profile->avatar  }}" alt="">
                    @else
                        <img class="img-fluid rounded-circle" src="{{ Gravatar::src(auth()->user()->email) }}" alt="">
                    @endif
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                <a class="dropdown-item" href="{{ route('users.profile') }}"><i class="fa fa-user"></i>{{ __('site.user.my_profile') }}</a>
                <a class="dropdown-item" href="{{ route('users.mylogs') }}"><i class="fa fa-history"></i>{{ __('site.user.my_logs') }}</a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                        class="fa fa-sign-out"></i> {{ __('site.logout') }}</a>
            </div>
        </li>
    </ul>
</header>
