@extends('backend.layout')
@section('title') {{ __('site.adspace.edit_adspace') }} @endsection
@section('content')
    <div class="page-subheader mb-3">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-adspaces rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{ __('site.adspace.adspaces') }} - {{ __('site.adspace.edit_adspace') }} <span class="badge badge-primary badge-text">{{ $adspace->name }}</span></span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="float-right">
                        <a class="btn btn-outline-primary btn-sm" href="{{ route('adspaces.index') }}"><i class="fa fa-angle-left"></i> {{ __('site.adspace.back_to_adspaces_list') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content d-flex flex">
        <div class="container-fluid">
            {!! Form::open(array('route' => ['adspaces.update', $adspace->id],  'method' => 'PUT', 'role' => 'form',  'required' => 'required')) !!}
            <div class="form-group {{$errors->has('name') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('name', __('site.adspace.adspace_name').' <span class="required"> * </span>')) !!}
                {!! Form::text('name',$adspace->name, array('class' => 'form-control', 'placeholder' => __('site.adspace.adspace_name'))) !!}
                @if ($errors->has('name'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('name') }}
                    </div>
                @endif
            </div>

            <div class="form-group {{$errors->has('ad_position') ? 'is-invalid':''}}">
                {!! Html::decode(Form::label('ad_position', __('site.adspace.adspaces_position').' <span class="required"> * </span>')) !!}
                {!! Form::text('ad_position',$adspace->ad_position, array('class' => 'form-control', 'placeholder' => __('site.adspace.adspaces_position'),'disabled'=>'disabled')) !!}
                @if ($errors->has('ad_position'))
                    <div class="invalid-feedback animated fadeInDown">
                        {{ $errors->first('ad_position') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                {!! Form::label('description', __('site.information')) !!}
                {!! Form::textarea('description',$adspace->description, array('class' => 'form-control', 'placeholder' => __('site.information'))) !!}
            </div>
            <div class="form-group">
                {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success','type' => 'submit')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
