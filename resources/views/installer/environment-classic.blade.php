@extends('installer.layouts.master')

@section('template_title')
    {{ trans('installer_messages.environment.classic.templateTitle') }}
@endsection

@section('title')
    <i class="fa fa-code fa-fw" aria-hidden="true"></i> {{ trans('installer_messages.environment.classic.title') }}
@endsection

@section('container')
    <form method="post" action="{{ route('Installer::environmentSaveClassic') }}">
        {!! csrf_field() !!}
        <div class="form-group">
            <textarea class="form-control installer-textarea" name="envConfig">{{ $envConfig }}</textarea>
        </div>
            <button class="btn btn-outline-info" type="submit">
                <i class="fa fa-floppy-o fa-fw" aria-hidden="true"></i>
                {!! trans('installer_messages.environment.classic.save') !!}
            </button>
    </form>

    @if( ! isset($environment['errors']))
           <div class="btn-group d-block mb-20">
               <a class="btn btn-dark float-right" href="{{ route('Installer::database') }}">
                   <i class="fa fa-check fa-fw" aria-hidden="true"></i>
                   {!! trans('installer_messages.environment.classic.install') !!}
                   <i class="fa fa-angle-double-right fa-fw" aria-hidden="true"></i>
               </a>
           </div>
    @endif

@endsection
