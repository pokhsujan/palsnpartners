@extends('installer.layouts.master')

@section('template_title')
    {{ trans('installer_messages.final.templateTitle') }}
@endsection

@section('title')
    <i class="fa fa-flag-checkered fa-fw" aria-hidden="true"></i>
    {{ trans('installer_messages.final.title') }}
@endsection

@section('container')

    @if(session('message')['dbOutputLog'])
        <p>{{ trans('installer_messages.final.migration') }}</p>
        <div class="slimscroll">
            <pre><code>{{ session('message')['dbOutputLog'] }}</code></pre>
        </div>
    @endif
    <br>
    <p>{{ trans('installer_messages.final.console') }}</p>
    <pre><code>{{ $finalMessages }}</code></pre>
    <br>
    <p>{{ trans('installer_messages.final.log') }}</p>
    <pre><code>{{ $finalStatusMessage }}</code></pre>
    <br>
    <p>{{ trans('installer_messages.final.env') }}<p>
    <div class="slimscroll">
        <pre><code>{{ $finalEnvFile }}</code></pre>
    </div>
    <div class="mt-20 mb-20 text-center">
        <a href="{{ route('admin') }}" class="btn btn-dark">{{ trans('installer_messages.final.exit') }}</a>
    </div>
@endsection
