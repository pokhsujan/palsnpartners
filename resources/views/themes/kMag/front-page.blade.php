@extends('themes.kMag.layout')

@section('title')
    {{ $page->title }}
@endsection
@push('meta-tags')
    @include('themes.kMag.partials.head-meta', [
        'image' =>  $page->featured_image?($page->media?url($page->media->media_url):''):'',
        'description' => $page->getMeta('meta_description'),
        'title' =>  $page->title,
        'keywords' => $page->getMeta('meta_keywords')
    ])
@endpush
@push('styles')

@endpush
@include('themes.kMag.partials.ads')
@section('content')
    @if(!$breaking_news->isEmpty())
        @if($page->getMeta('banner_style') == 'style-2')
            @include('themes.kMag.partials.module-breaking-news-l2', ['posts'=> $breaking_news])
        @else
            @include('themes.kMag.partials.module-breaking-news', ['posts'=> $breaking_news])
        @endif
    @endif
    @if(!$cover_news->isEmpty() && $page->getMeta('enable_spotlight'))
        @include('themes.kMag.partials.module-cover-news', ['posts'=> $cover_news])
    @endif
    <section class="news-module sidebar-module">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @stack('ad-header-middle')
                    @if($page->getMeta('enable_recent_news'))
                        @stack('ad-header-middle')
                        <div class="module module--highlights">
                            <div class="row align-items-center module__header">
                                <h2 class="module__title mb-0 col-auto">{{ $page->getMeta('recent_post_title')?$page->getMeta('recent_post_title'):'Recent News' }}</h2>
                                <div class="col pl-0">
                                    <div class="line"></div>
                                </div>
                            </div>
                            <div class="row">
                                @php $k = 0; @endphp
                                @foreach($recent_news as $post)
                                    @php $k++ @endphp
                                    <div class="col-md-6">
                                        <article class="post hero-post">
                                            {{ featured_image($post, 'medium', null, true) }}
                                            <div class="post-entry">
                                                <h2 class="post-title"><a
                                                            href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                                                </h2>
                                                <div class="post-meta">
                                                    <span class="post-date">{{ $post->published_date }}</span>
                                                    <a href="{{ route('user.posts', $post->user->username) }}"
                                                       class="author">{{ $post->user->profile->full_name }}</a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    @if($k>=($page->getMeta('recent_posts_per_page')?$page->getMeta('recent_posts_per_page'):6))
                                        @break;
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @else
                        @for($i=0; $i<=config('site.home_cat_sections_count',4); $i++)
                            @if($page->getMeta('sec_'.$i.'_enable'))
                                @if(array_key_exists($page->getMeta('sec_'.$i.'_category_id'), $catPosts))
                                    @if($page->getMeta('sec_'.$i.'_enable_ad'))
                                        <div class="poster adbm">
                                            @stack($page->getMeta('sec_'.$i.'_ad_position'))
                                        </div>
                                    @endif
                                    @php
                                        $sectionPosts = $catPosts[$page->getMeta('sec_'.$i.'_category_id')];
                                        $category = $categories->find($page->getMeta('sec_'.$i.'_category_id'));
                                        $title = $category->name;
                                        $cat_url = route('categories.show', $category->slug);
                                        $no_of_posts = $page->getMeta('sec_'.$i.'_posts_per_page')?$page->getMeta('sec_'.$i.'_posts_per_page'):6;
                                    @endphp
                                    @include('themes.kMag.post-layouts.'.$page->getMeta('sec_'.$i.'_style'), ['posts'=> $sectionPosts, 'title'=>$title,'url'=>$cat_url, 'no_of_posts'=> $no_of_posts])
                                @endif
                            @endif
                        @endfor
                    @endif
                    @stack('ad-header-bottom')
                </div>
                <aside class="col-md-4 sidebar">
                    @include('themes.kMag.sidebar', ['sidebar_type' => 'home_page'])
                </aside>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
