<!--.main-content end-->
@include('themes.kMag.partials.module-missed-posts')
<!--.related-module end-->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3 footer-widget info-widget">
                    <a href="{{ url('/') }}" class="footer-logo"><img
                                src="{{ \setting('', '/images/pnp-pic.png') }}"
                                alt="Kumaley Blog"></a>
                    <p class="description">
                        {{ setting('footer_widget_description') }}
                    </p>
                </div>
                <div class="col-md-5 col-lg-6 footer-widget footer-menu-widget">
                    <div class="footer-widget">
                        <div class="row justify-content-center">
                            <div class="footer-widget-col col-lg-3 col-6">
                                @include('themes.kMag.menus.footer-menu', ['menu_type'=>'footer_menu1'])
                            </div>
                            <div class="footer-widget-col col-lg col-6">
                                @include('themes.kMag.menus.footer-menu', ['menu_type'=>'footer_menu2'])
                            </div>
                            <div class="footer-widget-col col-lg col-6">
                                @include('themes.kMag.menus.footer-menu', ['menu_type'=>'footer_menu3'])
                            </div>
                            <div class="footer-widget-col col-lg col-6">
                                @include('themes.kMag.menus.footer-menu', ['menu_type'=>'footer_menu4'])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 footer-widget contact-widget">
                    <div class="footer-widget">
                        <h3 class="footer-sec-title">{{ \setting('footer_contact_widget_title') }}</h3>
                        <ul class="contact-info">
                            @if(setting('office_address'))
                                <li>
                                    <i class="icon-map-marker"></i> {{ \setting('office_address', 'Your Address Here') }}
                                </li>@endif
                            @if(setting('site_phone_number'))
                                <li><i class="icon-mobile"></i> {{ \setting('site_phone_number', '+977123456789') }}
                                </li>@endif
                            @if(setting('site_email'))
                                <li><i class="icon-envelope"></i> <a
                                            href='mailto:{{ \setting('site_email') }}'>{{ \setting('site_email', 'your@email.com') }} </a>@endif
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--.footer-top end-->
    <div class="footer-bottom text-center">
        <div class="container">
            <div class="row">
                <div class="col">
                    @if(setting('footer_copyright'))
                        {{ setting('footer_copyright') }}
                    @else
                        © {{ date('Y') }}. All Rights Reserved. {{ setting('site_name', config('app.name')) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--.footer-bottom end-->
</footer>
<!--.footer end-->
<!--.search-overlay end-->
<div class="overlay"></div>
<span class="scrolltop"><i class="fa fa-angle-up"></i></span>
@include('themes.kMag.partials.module-search-box')

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=1527387440824990&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
@stack('scripts')
{!! setting('footer_scripts') !!}
</body>
</html>
