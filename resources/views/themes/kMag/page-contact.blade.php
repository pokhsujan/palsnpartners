@extends('themes.kMag.layout')
@include('themes.kMag.partials.ads')
@section('title')
    {{ $page->title }}
@endsection
@push('styles')
@endpush
@section('content')
    <section class="news-module page news-column sidebar-module">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-content">
                        <h1 class="page-title">{{ $page->title }}</h1>
                        @if($page->featured_image)
                            <figure class="post-media">
                                <img src="{{ url($page->media->media_url) }}" alt="{{ $page->title }}"/>
                            </figure>
                        @endif
                        <ul class="contact-details">
                            <li class="contact-item"><i class="icon-map-marker"></i>Kumaley , 15 Green Street Kogarah 2217</li>
                            <li class="contact-item"><i class="icon-mobile"></i><a href="tel:1234567890">1234567890</a></li>
                            <li class="contact-item"><i class="icon-envelope"></i><a href="mailto:your@email.com">your@email.com</a></li>
                        </ul>
                        {!! $page->getMeta('map_iframe') !!}
                        <div class="post-entry">
                            {!!  $page->content !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h1 class="form-title">
                        {{ $page->getMeta('form_title') }}
                    </h1>
                    @include('themes.kMag.partials.message-form')
                </div>
            </div>
        </div>
    </section>
@endsection
