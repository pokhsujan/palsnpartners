<nav class="primary-nav">
    <div class="nav-trigger">
        <span class="burger"></span>
    </div>
    <div class="container">
        <ul class="main-menu">
            {!! nav_menu($navmenu,'main_menu') !!}
            <li class="search-trigger pull-right"><a href="javascript:void(0);"><i class="icon-search"></i></a></li>
        </ul>
    </div>
</nav>