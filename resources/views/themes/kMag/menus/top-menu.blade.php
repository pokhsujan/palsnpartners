<ul class="top-menu">
    {!! nav_menu($navmenu, 'top_menu') !!}
    @if(Auth::user())
        <li class="user-login"><a href="{{ route('users.profile') }}"><i class="icon-user"></i> Profile</a></li>
        <li class="user-login"><a href="{{ route('logout') }}"
                                  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                    class="fa fa-sign-out"></i> Logout</a></li>
    @else
        <li class="user-login"><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
        <li class="user-login"><a href="{{ route('register') }}"><i class="icon-user-add"></i> Register</a></li>
    @endif
</ul>
@if(Auth::user())
{{ Form::open(array('url' => '/logout','id'=>'logout-form', 'style'=>'display:none;', 'method'=>'post')) }}
{{ Form::submit('', ['class' => 'btn']) }}
{{ Form::close() }}
@endif
