<div class="widget" id="subscription-form">
    <div class="newsletter-sec-heading">
        <div class="tag">
            <a href="javascript:void(0);">
                <i class="icon-envelope"></i>{!! isset($title)?$title:'Subscribe <br>newsletter' !!}</a>
        </div>
    </div>
    <div class="newsletter-box">
        @if(count($errors)>0)
                <div class="alert-icon alert alert-danger alert-dismissible" role="alert">
                     Something went wrong. Please check below
                </div>
        @endif
        @if(session('success'))
                <div class="alert-icon alert alert-success alert-dismissible" role="alert">
                    {{ session('success') }}
                </div>
            @endif
        {!! Form::open(array('route' => ['subscriber.create'], 'method' => 'POST', 'role' => 'form')) !!}
        <div class="form-group  {{$errors->has('full_name') ? 'is-invalid':''}}">
            {!! Form::text('full_name',old('full_name'),['class'=>'form-control','placeholder'=>'Your Full Name','required']) !!}
            @if ($errors->has('full_name'))
                <span class="text-danger mt-2">
                    {{ $errors->first('full_name') }}
                </span>
            @endif
        </div>
        <div class="form-group {{$errors->has('email') ? 'is-invalid':''}}">
            {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'Your Email','required']) !!}
            @if ($errors->has('email'))
                <span class="text-danger mt-2">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
        <button type="submit" class="btn btn-block primary-btn rounded-0">Subscribe</button>
        {!! Form::close() !!}
    </div>
</div>
