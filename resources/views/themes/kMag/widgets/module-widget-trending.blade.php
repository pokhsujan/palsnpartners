<div class="widget featured-widget">
    <div class="widget-title"><span>{{ isset($title)?$title:'Trending Posts' }}</span></div>
    <div class="news-slides trending-news">
        @foreach($posts as $post)
        <article class="post hero-post">
            {{ featured_image($post, 'medium', null, true) }}
            <div class="post-entry">
                <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                {{  post_meta($post) }}
                <p>{{ $post->excerpt }}</p>
        </article>
        @endforeach
    </div>
</div>
