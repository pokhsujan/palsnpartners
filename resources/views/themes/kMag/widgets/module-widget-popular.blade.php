<div class="widget ">
    <div class="news-sec-heading">
        <div class="tag"><a href="javascript:void(0);">{{ isset($title)?$title:'Popular Posts' }}</a></div>
    </div>
    <div class="post-list">
        @foreach($posts as $post)
            <article class="post">
                {{ featured_image($post, 'small', null, true) }}
                <div class="post-entry">
                    <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                    {{  post_meta($post) }}
                </div>
            </article>
        @endforeach
    </div>
</div>
