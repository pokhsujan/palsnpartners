<div class="widget">
    <div class="widget-social">
        <div class="news-sec-heading">
            <div class="tag"><a href="javascript:void(0);">{{ isset($title)?$title:'Social' }}</a></div>
        </div>
        <ul id="share" class="get-social">
            @if(setting('facebook_url') || setting('twitter_url') || setting('youtube_url') || setting('instagram_url') || setting('gplus_url'))

                @if(setting('facebook_url'))
                    <li class="facebook">
                        <a href="{{ \setting('facebook_url') }}" target="_blank">
                            <i class="icon-facebook rounded-circle"></i>
                            <span class="follow-type">Like</span>
                        </a>
                    </li>
                @endif
                @if(setting('twitter_url'))
                    <li class="twitter">
                        <a href="{{ \setting('twitter_url') }}" target="_blank">
                            <i class="icon-twitter rounded-circle"></i>
                            <span class="follow-type">Follow</span>
                        </a>
                    </li>
                @endif
                @if(setting('youtube_url'))
                    <li class="youtube">
                        <a href="{{ \setting('youtube_url') }}" target="_blank">
                            <i class="fa fa-youtube rounded-circle"></i>
                            <span class="follow-type">Subscribe</span>
                        </a>
                    </li>
                @endif
                @if(setting('instagram_url'))
                    <li class="instagram">
                        <a href="{{ \setting('instagram_url') }}" target="_blank">
                            <i class="icon-instagram rounded-circle"></i>
                            <span class="follow-type">Follow</span>
                        </a>
                    </li>
                @endif
                @if(setting('pinterest_url'))
                    <li class="pinterest">
                        <a href="{{ \setting('pinterest_url') }}" target="_blank">
                            <i class="fa fa-pinterest rounded-circle"></i>
                            <span class="follow-type">Follow</span>
                        </a>
                    </li>
                @endif
            @else

                <li>
                    Please add social url's in Settings > Social Tab
                </li>

            @endif
        </ul>
    </div>
</div>
