@php
    $widget_type = isset($sidebar_type)?$sidebar_type:'default';
    $widgets_to_add = isset($widgets[$widget_type])?$widgets[$widget_type]:$widgets['default'];
    //dd($widgets_to_add);
@endphp
@if($widgets_to_add)
    @foreach($widgets_to_add as $s_widget)
        @switch($s_widget->widget_id)
            @case('popular_news')
            @include('themes.kMag.widgets.module-widget-popular',['posts'=> $popular_posts,'title'=>$s_widget->title])
            @break
            @case('featured_news')
            @include('themes.kMag.widgets.module-widget-trending', ['posts'=> $featured_posts, 'title'=>$s_widget->title])
            @break
            @case('recent_news')
            @include('themes.kMag.widgets.module-widget-popular', ['posts'=> $latest_posts, 'title'=>$s_widget->title])
            @break
            @case('facebook_likebox')
            @include('themes.kMag.widgets.module-widget-facebook',['title'=>$s_widget->title])
            @break
            @case('social_box')
            @include('themes.kMag.widgets.module-widget-social', ['title'=>$s_widget->title])
            @break
            @case('subscriber')
            @include('themes.kMag.widgets.module-widget-subscriber', ['title'=>$s_widget->title])
            @break
            @case(in_array($s_widget->widget_id, $ad_positions)?$s_widget->widget_id:'ads')
            <div class="poster adbm">
                @stack($s_widget->widget_id)
            </div>
            @break
            @default
        @endswitch
    @endforeach
@endif


