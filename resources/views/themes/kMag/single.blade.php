@extends('themes.kMag.layout')
@section('title'){{ $post->title }}@endsection
@push('meta-tags')
    @include('themes.kMag.partials.head-meta', [
        'image' =>  $post->featured_image?($post->media?url($post->media->media_url):''):'',
        'description' => $post->excerpt,
        'title' =>  $post->title,
        'keywords' => ''
    ])
@endpush
@include('themes.kMag.partials.ads')
@push('styles')
    <link rel="canonical" href="{{ $post->url }}">
@endpush
@section('content')
    <section class="news-module news-column sidebar-module">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="single-post">
                        <div class="post-cat">
                        @foreach($categories as $category )
                            <a class="cat-name"
                               href="{{ route('categories.show', $category->slug) }}">{{$category->name}}</a>&nbsp;
                            &nbsp;
                        @endforeach
                        </div>
                        <h1 class="post-title">{{ $post->title }} </h1>
                        @if($post->subtitle)
                            <h4 class="post-subtitle">{{ $post->subtitle }}</h4>
                        @endif
                        <div class="post-head row align-items-center">
                            <div class="col-md-8">
                                {{ post_meta($post) }}
                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                        @if($post->format == 'standard' && $post->featured_image)
                            <figure class="post-media">
                                <img class="img-responsive" src="{{ url($post->media->media_url) }}"
                                     alt="{{ $post->title }}"/>
                            </figure>
                        @endif
                        <div class="post-entry">
                            {!!  $post->formatted_content !!}
                        </div>
                        <!-- end article-holder -->
                        <div class="tag-cloud clearfix">
                            @foreach($tags as $tag)
                                <a href="{{ route('tags.show', $tag->slug) }}" class="tag">
                                    <span class="tags">
                                        {{$tag->name}}
                                    </span>
                                </a>
                            @endforeach
                        </div>
                        <div class="post-share">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-12 col-lg-auto">
                                    <span class="tag">Share this article on:</span>
                                </div>
                                <div class="col-12 col-lg-auto">
                                    <ul id="shareButtons" class="sharethis-inline-share-buttons get-social">
                                        <li class="facebook">
                                            <a href="" target="_blank">
                                                <i class="icon-facebook rounded-circle"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="" target="_blank">
                                                <i class="icon-twitter rounded-circle"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="" target="_blank">
                                                <i class="fa fa-youtube rounded-circle"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="" target="_blank">
                                                <i class="icon-instagram rounded-circle"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            @rating(['model' => $post])
                            @endrating
                            <div class="poster adbm adtm">
                                @stack('single-bottom-1')
                            </div>
                            @comment(['model' => $post])
                            @endcomment
                            <div class="poster  adbm adtm">
                                @stack('single-bottom-2')
                            </div>
                    </div>
                    @include('themes.kMag.partials.module-widget-related', ['posts'=>$related_posts])
                    <div class="poster  adbm adtm">
                        @stack('single-bottom-3')
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget author-module">
                        <h2>About Author</h2>
                        <div class="author-pic">
                            <img class="rounded-circle"
                                 src="{{ $post->user->profile->avatar?$post->user->profile->avatar:Gravatar::src($post->user->email) }}"
                                 alt="{{ $post->user->full_name }}">
                        </div>
                        <div class="author-desc">
                            <h3><a href="{{ route('user.posts', $post->user->username) }}"
                                   class="author">{{ $post->user->full_name }}</a></h3>
                            <a href="{{ route('user.posts', $post->user->username) }}" class="author">More
                                By {{ $post->user->full_name }}</a>
                        </div>
                    </div>
                    @include('themes.kMag.sidebar', ['sidebar_type' => 'single'])
                </div>
            </div>
        </div>
    </section>
    <!--.news-module end-->
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            // Check Radio-box
            var $star_rating = $('.rating .fa-star');
            var SetRatingStar = function() {
                return $star_rating.each(function() {
                    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                        return $(this).removeClass('fa-star-o').addClass('fa-star');
                    } else {
                        return $(this).removeClass('fa-star').addClass('fa-star-o');
                    }
                });
            };

            SetRatingStar();
            $star_rating.on('click', function(e) {
                $star_rating.siblings('input.rating-value').val($(this).data('rating'));
                SetRatingStar();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    }
                });
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: $('#postRating').attr('action'),
                    data: $('#postRating').serialize(),
                    success: function(data) {
                         SetRatingStar();
                         $('.rating-success').html('Thank you for your rating.')
                    },
                });
                return false;
            });
        });
    </script>
@endpush
