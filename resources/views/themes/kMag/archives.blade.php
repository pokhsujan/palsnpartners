@extends('themes.kMag.layout')
@section('title')
    {{ $title }}
@endsection
@push('styles')
@endpush
@include('themes.kMag.partials.ads')
@push('page-head')
    @include('themes.kMag.page-head.'. $type)
@endpush
@section('content')
    @if(in_array($type, ['category', 'tag'], true))
        @include('themes.kMag.partials.module-cat-billboard', ['posts'=> $topPosts, 'name'=>$title])
    @endif
    <section class="news-module archive-module news-column">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @stack('archive-content-middle')
                    @include('themes.kMag.archive-layouts.'.setting($type.'_layout', 'one-col-layout'))
                    {{ $posts->render() }}
                    @stack('archive-content-bottom')
                </div>
                <div class="col-md-4">
                    <div class="sidebar-module">
                        @include('themes.kMag.sidebar', ['sidebar_type' => $type])
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
