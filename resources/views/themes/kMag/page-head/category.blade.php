<section class="page-head-module">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h1 class="page-title">{{ $title }}</h1>
            </div>
            <div class="col-md-8">
                @stack('archive-content-top')
            </div>
        </div>
    </div>
</section>