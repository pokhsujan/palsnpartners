<section class="page-head-module">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-auto">
                        <div class="author-pic">
                            <img class="rounded-circle"
                                 src="{{$user->profile->avatar?$user->profile->avatar:Gravatar::src($user->email)}}"
                                 alt="{{$user->full_name}}">
                        </div>
                    </div>
                    <div class="col">
                        <span class="page-subtitle">All Posts By</span>
                        <h1 class="page-title author-name">{{ $user->profile->display_name?$user->profile->display_name:$user->username }}</h1>
                        @if($user->profile->description)
                            <p class="author-description">{{$user->profile->description}}</p>
                        @endif
                        @if($user->profile->website)
                            <div class="author-website"><a href="{{ $user->profile->website }}" class="" target="_blank">{{ $user->profile->website }}</a></div>
                        @endif
                        <ul class="get-social author-social">
                            @if($user->getMeta('facebook_url'))
                                <li class="facebook"><a href="{{ $user->getMeta('facebook_url') }}" class=""> <i
                                                class="icon-facebook"></i></a></li>
                            @endif
                            @if($user->getMeta('twitter_url'))
                                <li class="twitter"><a href="{{ $user->getMeta('twitter_url') }}" class=""> <i
                                                class="icon-twitter"></i></a></li>
                            @endif
                            @if($user->getMeta('youtube_url'))
                                <li class="youtube"><a href="{{ $user->getMeta('youtube_url') }}" class=""> <i
                                                class="fa fa-youtube"></i></a></li>
                            @endif
                            @if($user->getMeta('instagram_url'))
                                <li class="instagram"><a href="{{ $user->getMeta('instagram_url') }}" class=""> <i
                                                class="icon-instagram"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>