<section class="page-head-module search-head-module">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h2 class="page-title">Search Results for : <b>{{ $search }}</b></h2>
            </div>
            <div class="col-md-8">
                @stack('archive-content-top')
            </div>
        </div>
        <div class="adv-search-module">
            {!! Form::open(array('route' => 'posts.search',  'method' => 'GET', 'class' => 'advance-search-form')) !!}
            <div class="form-group row no-gutters">
                <div class="col">
                    {{ Form::text('s', $search, ['class'=> 'form-control', 'placeholder'=>'type here to search', 'required'=>'required']) }}
                </div>
                <div class="col-auto">
                    <button class="btn primary-btn" type="submit"><i class="icon-search"></i>SUBMIT</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</section>