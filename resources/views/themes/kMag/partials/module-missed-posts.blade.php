<section class="related-news news-column">
    <div class="container">
        <h2 class="sec-heading row align-items-center"><span
                    class="col-auto">{{ setting('missed_post_title') }}</span><span class="col pl-0"><span
                        class="d-block divider"></span></span></h2>
        <div class="row">
            @for($i=0;$i<2;$i++)
                @foreach($missed_news as $post)
                    <div class="col-md-4">
                        <article class="post post--category grid-post grid-post--medium">
                            {{ featured_image($post, 'small', null, true) }}
                            <div class="post-entry">
                                <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                                {{ post_meta($post) }}
                            </div>
                        </article>
                    </div>
                @endforeach
            @endfor
        </div>
    </div>
</section>
