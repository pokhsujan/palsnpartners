<section class="billboard-news-module">
    <div class="container">
        <div class="row">
            @php $k = 0; @endphp
            @foreach($posts as $post)
                @if($k == 0 || $k == 1)
                    <div class="col-md-6">
                        @endif
                        @if($k == 1)
                            <div class="row">
                                <div class="col-md-12">
                                    @endif
                                    @if($k == 2 || $k == 3)
                                        <div class="col-md-6 col-sm-6">
                                            @endif
                                            @if($k <4)
                                            <article class="post overlay-post @if($k ==0)large @elseif($k == 1)medium @endif">
                                                <a href="{{ route('posts.show', $post->slug) }}">
                                                    <div class="post-media"
                                                         style="background: url({{ $post->media?route('images.show', ['large',$post->media->filename]):$post->video_image }}) no-repeat center center / cover ;">
                                                        @if($post->format == 'video')
                                                            <span class="media-item"><i class="icon-play"></i></span>
                                                        @endif
                                                    </div>
                                                </a>
                                                <div class="post-entry">
                                                    @if($post->categories)
                                                        <div class="post-cat">
                                                            @foreach($post->categories as $cat)
                                                                <a href="{{ route('categories.show', $cat->slug) }}">{{ $cat->name }}</a>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                    <h2 class="post-title">
                                                        <a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                                                    </h2>
                                                        {{  post_meta($post) }}
                                                </div>
                                            </article>
                                            @endif
                                            @if($k == 0 || $k == 3)
                                        </div>
                                    @endif
                                    @if($k == 1)
                                </div>
                                @endif
                                @if($k == 2 || $k == 3)
                            </div>
                        @endif
                        @if($k == 3)
                    </div>
                @endif
                @php $k++; @endphp
            @endforeach
        </div>
    </div>
    </div>
</section>