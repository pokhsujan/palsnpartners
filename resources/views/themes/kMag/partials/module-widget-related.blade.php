<section class="news-module related-module">
    <div class="news-sec-heading">
      <div class="tag"><a href="javascript:void(0);">Related</a></div>
    </div>
    <div class="row">
      @foreach($posts as $post)
            <div class="col-md-6">
                <article class="post post--category grid-post grid-post--medium">
                    {{ featured_image($post, 'medium', null, true) }}
                    <div class="post-entry">
                        <h2 class="post-title"><a
                                    href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                        </h2>
                        {{ post_meta($post) }}
                    </div>
                </article>
            </div>
      @endforeach
    </div>
</section>
