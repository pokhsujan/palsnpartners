<div class="message-box">
    @if(count($errors)>0)
        <div class="alert-icon alert alert-danger alert-dismissible" role="alert">
            Something went wrong. Please check below
        </div>
    @endif
    @if(session('success'))
        <div class="alert-icon alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    {!! Form::open(array('route' => ['messages.create'], 'method' => 'POST', 'role' => 'form')) !!}
    <div class="form-group  {{$errors->has('full_name') ? 'is-invalid':''}}">
        {!! Form::text('full_name',\Auth::user()?auth()->user()->profile->full_name:old('full_name'),['class'=>'form-control','placeholder'=>'your full name','required']) !!}
        @if ($errors->has('full_name'))
            <span class="text-danger">
                    {{ $errors->first('full_name') }}
                </span>
        @endif
    </div>
    <div class="form-group {{$errors->has('email') ? 'is-invalid':''}}">
        {!! Form::email('email',\Auth::user()?auth()->user()->email:old('email'),['class'=>'form-control','placeholder'=>'your email','required']) !!}
        @if ($errors->has('email'))
            <span class="text-danger">
                    {{ $errors->first('email') }}
                </span>
        @endif
    </div>
    <div class="form-group {{$errors->has('phone') ? 'is-invalid':''}}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'your phone number']) !!}
        @if ($errors->has('phone'))
            <span class="text-danger">
                    {{ $errors->first('phone') }}
                </span>
        @endif
    </div>
    <div class="form-group {{$errors->has('mobile') ? 'is-invalid':''}}">
        {!! Form::email('mobile',old('mobile'),['class'=>'form-control','placeholder'=>'your mobile number']) !!}
        @if ($errors->has('mobile'))
            <span class="text-danger">
                    {{ $errors->first('mobile') }}
                </span>
        @endif
    </div>
    <div class="form-group {{$errors->has('message') ? 'is-invalid':''}}">
        {!! Form::textarea('message',old('message'),['class'=>'form-control','placeholder'=>'your message','required']) !!}
        @if ($errors->has('message'))
            <span class="text-danger">
                    {{ $errors->first('message') }}
                </span>
        @endif
    </div>
    <button type="submit" class="btn primary-btn">Send Message</button>
    {!! Form::close() !!}
</div>
