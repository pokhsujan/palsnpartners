    <meta name="robots" content="all">
    <meta name="description" content="{{ !empty($description)? $description : '' }}">
    <meta name="keywords" content="{{ !empty($keywords)? $keywords : '' }}">

    <!-- Facebook OpenGraph -->
    <meta property="og:locale" content="{{  config('app.locale') }}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:title" content="{{ !empty($title)? $title : '' }}">
    <meta property="og:description" content="{{ !empty($description)? $description : '' }}">
    <meta property="og:image" content="{{ !empty($image)? $image : setting('site_logo') }}">
    <meta property="og:site_name" content="{{  config('app.name') }}">

    <meta name="twitter:title" content="{{ !empty($title)? $title : '' }}">
    <meta name="twitter:description" content="{{ !empty($description)? $description : '' }}">
    <meta name="twitter:image" content="{{ !empty($image)? $image : setting('site_logo') }}">
    <!-- Google Plus Full-Bleed Image -->
    <meta itemprop="og:headline" content="{{ !empty($image)? $image : setting('site_logo') }}" />
