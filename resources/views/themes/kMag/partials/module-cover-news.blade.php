<section class="top-stories-module">
    <div class="container">
        <div class="row">
            @foreach($posts as $post)
                <article class="col-6 col-md-4 col-lg post-wrap">
                    <div class="post">
                        <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                    </div>
                </article>
            @endforeach
        </div>
    </div>
</section>
