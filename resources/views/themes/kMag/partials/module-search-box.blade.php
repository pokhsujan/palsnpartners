<div class="search-overlay overlay-module">
    <div class="nav-close close-trigger">
            <span class="burger">
            </span>
    </div>
    <div class="overlay-main">
        <div class="overlay-entry  d-flex justify-content-center flex-column">
            {!! Form::open(array('route' => 'posts.search',  'method' => 'GET', 'class' => 'search-form')) !!}
                {!! Form::text('s',old('s'), array('class' => 'search-field', 'placeholder' => 'Search...', 'required'=>'required')) !!}
                <div class="submit">
                    <input type="submit" class="search-submit" value="Search..."><i class="fa fa-search"></i>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
