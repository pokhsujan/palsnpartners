@if(count($posts) > 3)
<section class="billboard-news-module billboard-cat-news-module module module--billboard-cat style-2">
    <div class="container">
        @php $k = 0; @endphp
        @foreach($posts as $post)
            @if($k==0)
                <article class="post overlay-post full row no-gutters"
                         style="background: url({{ $post->media?url($post->media->media_url):$post->video_image }}) no-repeat center center / cover ;">
                    <a class="col-md-7 col-lg-8" href="{{ route('posts.show', $post->slug) }}">
                        <div class="post-media row justify-content-center no-gutters">
                            <div class="col-md-10">
                                @if($post->format == 'video')
                                    <span class="media-item"><i class="icon-play"></i></span>
                                @endif
                            </div>
                        </div>
                    </a>
                    <div class="post-entry col-md-7 col-lg-8">
                        <div class="row justify-content-center no-gutters">
                            <div class="col-11 col-md-10">
                                @if($post->categories)
                                    <div class="post-cat">
                                        @foreach($post->categories as $cat)
                                            <a href="{{ route('categories.show', $cat->slug) }}">{{ $cat->name }}</a>
                                        @endforeach
                                    </div>
                                @endif
                                <h2 class="post-title">
                                    <a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                                </h2>
                                    {{ post_meta($post) }}
                            </div>
                        </div>
                    </div>
                </article>
            @endif
            @if($k==1)
                <div class="row justify-content-end no-gutters">
                    <div class="col-md-5 col-lg-4 news-list">
                        <div class="row align-items-center module__header">
                            <h2 class="module__title mb-0 col-auto">Top {{ isset($name)?$name:'' }} Stories</h2>
                            <div class="col pl-0">
                                <div class="line"></div>
                            </div>
                        </div>
                        @endif
                        @if($k!=0)
                            <article class="post list-post">
                                <div class="post-entry">
                                    <h2 class="post-title"><a
                                                href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                                    </h2>
                                    {{ post_meta($post) }}
                                </div>
                            </article>
                        @endif
                        @if($k==5)
                    </div>
                </div>
            @endif
            @php $k++; @endphp
        @endforeach
    </div>
</section>
@endif