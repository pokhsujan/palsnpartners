@if(setting('enable_comment') == 'enable')
    <div class="news-sec-heading">
        <div class="tag"><a href="javascript:void(0);">Comments</a></div>
    </div>
    <div class="comment-module">
        @if(setting('comment')== 'facebook')
            <div class="fb-comments" data-href="{{url()->current()}}" data-numposts="5" data-width="100%"></div>
        @elseif(setting('comment')=='disqus')
            <div id="disqus_thread"></div>
        @else
            @auth
                @if($model->comments->count() < 1)
                    <p class="lead">There are no comments yet.</p>
                @endif

                <ul class="list-unstyled">
                    @foreach($model->comments->where('parent', null) as $comment)
                        @include('themes.kMag.partials.comments._comment')
                    @endforeach
                </ul>
                @include('themes.kMag.partials.comments._form')
            @else
                @if($model->comments->count() < 1)
                    <p class="lead">There are no comments yet.</p>
                @endif

                <ul class="list-unstyled">
                    @foreach($model->comments->where('parent', null) as $comment)
                        @include('themes.kMag.partials.comments._comment')
                    @endforeach
                </ul>
                <div class="card">
                    <div class="card-body">
                        <p>You must <a href="{{ route('login',['redirect' => url()]) }}">log in</a> to comment this post.
                        </p>
                    </div>
                </div>
            @endauth
        @endif
    </div>
@endif
