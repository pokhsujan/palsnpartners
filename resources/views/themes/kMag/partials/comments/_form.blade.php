<div class="news-sec-heading">
    <div class="tag"><a href="javascript:void(0);">Leave a Reply</a></div>
</div>
<form method="POST" action="{{ route('comment.store') }}">
    @csrf
    <input type="hidden" name="commentable_type" value="\{{ get_class($model) }}"/>
    <input type="hidden" name="commentable_id" value="{{ $model->id }}"/>
    <div class="form-group">
        <label for="message" hidden>Enter your comment here:</label>
        <textarea class="form-control @if($errors->has('message')) is-invalid @endif" name="message" rows="3"
                  placeholder="comment"></textarea>
        <div class="invalid-feedback">
            Your message is required.
        </div>
        <small class="form-text text-muted"><a target="_blank"
                                               href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a>
            cheatsheet.
        </small>
    </div>
    <button type="submit" class="btn btn-sm primary-btn text-uppercase">Post Comment</button>
</form>
