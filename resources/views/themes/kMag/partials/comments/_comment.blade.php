@inject('markdown', 'Parsedown')
@if(isset($reply) && $reply === true)
    <div id="comment-{{ $comment->id }}" class="media">
        @else
            <li id="comment-{{ $comment->id }}" class="media row no-gutters">
                @endif
                @if(!is_null($comment->commenter->profile) && $comment->commenter->profile->avatar)
                    <div class="col-auto">
                        <img class="img-fluid rounded-circle commenter-dp" src="{{ $comment->commenter->profile->avatar  }}"
                             alt="{{ $comment->commenter->name }} Avatar">
                    </div>
                @else
                    <div class="col-auto">
                        <img class="img-fluid rounded-circle commenter-dp" src="{{ Gravatar::src($comment->commenter->email) }}"
                             alt="{{ $comment->commenter->name }} Avatar">
                    </div>
                @endif
                <div class="col">
                    <div class="media-body">
                        <div class="row no-gutters">
                            <div class="col">
                                <div class="comment-meta">
                                    <h5 class="author">{{ $comment->commenter->full_name }}
                                    </h5>
                                    <span class="comment-date">{{ $comment->created_at->diffForHumans() }}</small>
                                </div>
                                <div class="comment">{!! $markdown->line($comment->comment) !!}</div>
                            </div>
                            <div class="col-12 col-lg-auto">

                                <div class="comment-action">
                                    @can('reply-to-comment', $comment)
                                        <button data-toggle="modal" data-target="#reply-modal-{{ $comment->id }}"
                                                class="btn btn-sm btn-link text-uppercase"><i class="fa fa-reply"></i>
                                        </button>
                                    @endcan
                                    @can('edit-comment', $comment)
                                        <button data-toggle="modal" data-target="#comment-modal-{{ $comment->id }}"
                                                class="btn btn-sm btn-link text-uppercase"><i class="fa fa-edit"></i>
                                        </button>
                                    @endcan
                                    @can('delete-comment', $comment)
                                        <a href="{{ url('comments/' . $comment->id) }}"
                                           onclick="event.preventDefault();document.getElementById('comment-delete-form-{{ $comment->id }}').submit();"
                                           class="btn btn-sm btn-link"><i class="fa fa-trash"></i></a>
                                        <form id="comment-delete-form-{{ $comment->id }}"
                                              action="{{ route('comment.destroy', $comment->id) }}" method="POST" style="display: none;">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    @endcan

                                </div>
                            </div>
                        </div>
                    </div>

                    @can('edit-comment', $comment)
                        <div class="modal fade" id="comment-modal-{{ $comment->id }}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="POST" action="{{ route('comment.update' , $comment->id) }}">
                                        @method('PUT')
                                        @csrf
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Comment</h5>
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="message">Update your message here:</label>
                                                <textarea required class="form-control" name="message"
                                                          rows="3">{{ $comment->comment }}</textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-block primary-btn">
                                                Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endcan

                    @can('reply-to-comment', $comment)
                        <div class="modal fade" id="reply-modal-{{ $comment->id }}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="POST" action="{{ route('comment.reply' , $comment->id) }}">
                                        @csrf
                                        <div class="modal-header">
                                            <h5 class="modal-title">Reply to Comment</h5>
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="message">Enter your message here:</label>
                                                <textarea required class="form-control" name="message"
                                                          rows="3"></textarea>
                                                <small class="form-text text-muted"><a target="_blank"
                                                                                       href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a>
                                                    cheatsheet.
                                                </small>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button"
                                                    class="btn btn-sm btn-outline-secondary text-uppercase"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">
                                                Reply
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endcan

                    <br/>{{-- Margin bottom --}}

                    @foreach($comment->children as $child)
                        @include('themes.kMag.partials.comments._comment', [
                            'comment' => $child,
                            'reply' => true
                        ])
                    @endforeach
                </div>
            @if(isset($reply) && $reply === true)
    </div>
    @else
    </li>
@endif
