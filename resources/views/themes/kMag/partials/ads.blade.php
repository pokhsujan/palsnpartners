@foreach($adspaces as $adspace)
    @if($adspace->ads)
    @push($adspace->ad_position)
        @php $ad = $adspace->ads(function ($q){
            $q->runningAds();
        })->first(); @endphp
        <div class="adv-holder spacer">
            @if($ad->ad_type == 'image_ad')
                <div class="text-center">
                    <a href="{{ $ad->url }}" target="_blank">
                        <img src="{{ url($ad->media->media_url) }}" class="img-responsive" alt="{{ $ad->title }}"/>
                    </a>
                </div>
            @else
                {!! $ad->script !!}
            @endif
        </div>
    @endpush
    @endif
@endforeach
