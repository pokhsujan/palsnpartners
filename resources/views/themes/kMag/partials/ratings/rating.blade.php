@if(setting('enable_rating')  == 'enable')
<div class="rating-module">
    <div class="news-sec-heading">
        <div class="tag"><a href="javascript:void(0);">Rating</a></div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <span class="heading">User Rating</span>
            <span class="fa fa-star {{ $model->avgRating >= 1 ? 'checked' : '' }}"></span>
            <span class="fa fa-star {{ $model->avgRating >= 2 ? 'checked' : '' }}"></span>
            <span class="fa fa-star {{ $model->avgRating >= 3 ? 'checked' : '' }}"></span>
            <span class="fa fa-star {{ $model->avgRating >= 4 ? 'checked' : '' }}"></span>
            <span class="fa fa-star {{ $model->avgRating == 5 ? 'checked' : '' }}"></span>
            <p> {{ $model->avgRating }} average based on {{ count($model->ratings) }} reviews.</p>
        </div>
        <div class="col-md-6">
            @auth
                {!! Form::open(array('route' => ['rating.store'], 'method' => 'POST', 'role' => 'form', 'id'=>'postRating')) !!}
                <input type="hidden" name="rateable_type" value="\{{ get_class($model) }}" />
                <input type="hidden" name="rateable_id" value="{{ $model->id }}" />
                <span class="heading">Your Rating</span>
                <div class="rating">
                    <span class="fa fa-star" data-rating="1"></span>
                    <span class="fa fa-star" data-rating="2"></span>
                    <span class="fa fa-star" data-rating="3"></span>
                    <span class="fa fa-star" data-rating="4"></span>
                    <span class="fa fa-star" data-rating="5"></span>
                    <input type="hidden" name="rating" class="rating-value" value="{{$model->authUserRating}}">
                    {{--@if(null($model->authUserRating))--}}
                        {{--<p>{{ __('You haven\'\t rated yet! ) }}</p>--}}
                    {{--@endif--}}
                </div>
                {!! Form::close() !!}
                <p class="rating-success text-success"></p>
            @else
                <p>You must <a href="{{ route('login',['redirect' => url()]) }}">log in</a> to rate this post.</p>
            @endauth
        </div>
    </div>
</div>
@endif
