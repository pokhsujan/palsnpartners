<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title><![CDATA[{{ setting('site_name') }}]]></title>
    <link> {{ url('/') }}/ </link>
    <language>en-us</language>
    <lastBuildDate>{{ \Carbon\Carbon::now()->format('D, d M y H:i:s O') }}</lastBuildDate>
    @foreach($posts as $post)
      <item>
        <title>{{ $post->title}}</title>
        <link> {{ $post->url }} </link>
        <guid isPermaLink="true">{{ $post->url }}</guid>
        <pubDate>{{ $post->created_at->format('D, d M y H:i:s O') }}</pubDate>
        <description>{{ $post->excerpt }}</description>
      </item>
    @endforeach
  </channel>
</rss>