@extends('themes.kMag.layout')
@include('themes.kMag.partials.ads')
@section('title')
{{ $page->title }}
@endsection
@push('meta-tags')
    {!!
        Share::generateMeta(
            $page->url,
            'article',
            $page->title,
            $page->featured_image?($page->media?url($post->media->media_url):null):null,
            setting('site_name'),
            $page->getMeta('meta_description')
        )
    !!}
@endpush
@push('styles')
@endpush
@section('content')
<section class="news-module page news-column sidebar-module">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="page-content">
                    <h1 class="page-title">{{ $page->title }}</h1>
                    <h2>{{ $page->subtitle }}</h2>
                    @if($page->featured_image)
                    <figure class="post-media">
                        <img src="{{ url($page->media->media_url) }}" alt="{{ $page->title }}"/>
                    </figure>
                    @endif
                    <div class="post-entry">
                        {!!  $page->content !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                @include('themes.kMag.sidebar')
            </div>
        </div>
    </div>
</section>
@endsection
