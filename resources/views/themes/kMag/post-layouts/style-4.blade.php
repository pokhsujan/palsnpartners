<div class="module module--highlights">
    <div class="row align-items-center module__header">
        <h2 class="module__title mb-0 col-auto">{{ $title }}</h2>
        <div class="col pl-0">
            <div class="line"></div>
        </div>
        <div class="col-auto">
            <a class="module__link"
               href="{{ $url }}"> View
                All <i class="icon-arrow-right"></i></a>
        </div>
    </div>
    <div class="row">
        @php $k = 0; @endphp
        @foreach($recent_news as $post)
            @php $k++ @endphp
            <div class="col-md-6">
                <article class="post hero-post">
                    {{ featured_image($post, 'medium', null, true) }}
                    <div class="post-entry">
                        <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                        {{ post_meta($post) }}
                    </div>
                </article>
            </div>
            @if($k>=$no_of_posts)
                @break;
            @endif
        @endforeach
    </div>
</div>