<div class="module module--category">
    <div class="row align-items-center module__header">
        <h2 class="module__title mb-0 col-auto">{{ $title  }}</h2>
        <div class="col pl-0 pr-md-0">
            <div class="line"></div>
        </div>
        <div class="col-auto">
            <a class="module__link" href="{{ $url }}"> View All <i class="icon-arrow-right"></i></a>
        </div>
    </div>
    <div class="row">
        @php $p_p = 0 @endphp
        @foreach($posts as $post)
            @php $p_p++;  @endphp
            <div class="col-md-6">
                <article class="post post--category grid-post grid-post--medium">
                    {{ featured_image($post, 'medium', null, true) }}
                    <div class="post-entry">
                        <h2 class="post-title"><a
                                    href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                        </h2>
                        {{  post_meta($post) }}
                    </div>
                </article>
            </div>
            @if($p_p >= $no_of_posts)
                @break
            @endif
        @endforeach
    </div>
</div>