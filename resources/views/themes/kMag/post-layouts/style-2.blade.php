<div class="module module--category">
    <div class="row align-items-center module__header">
        <h2 class="module__title mb-0 col-auto">{{ $title  }}</h2>
        <div class="col pl-0 pr-md-0">
            <div class="line"></div>
        </div>
        <div class="col-auto">
            <a class="module__link" href="{{ $url }}">View All <i class="icon-arrow-right"></i></a>
        </div>
    </div>
    @php $k = 0; @endphp
    @foreach($posts as $post)
        <article class="post post--category post--category-single row">
            {{ featured_image($post, 'medium', 'post-media col-md-6', true) }}
            <div class="post-entry col-md-6">
                <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                {{ post_meta($post) }}
                <p>{{ $post->excerpt }}</p>
                <a href="{{ $post->url }}" class="post__btn-link">Read More<i class="icon-arrow-right"></i></a>
            </div>
        </article>
        @php $k++; @endphp
        @if($k >= $no_of_posts)
            @break
        @endif
    @endforeach
</div>