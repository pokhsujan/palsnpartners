<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @if($post)
        <sitemap>
            <loc>{{ route('sitemap.show', 'post') }}</loc>
            <lastmod>{{ $post->publish_on->tz('UTC')->toAtomString() }}</lastmod>
        </sitemap>
    @endif
    @if($page)
        <sitemap>
            <loc>{{ route('sitemap.show', 'page') }}</loc>
            <lastmod>{{ $page->created_at->tz('UTC')->toAtomString() }}</lastmod>
        </sitemap>
    @endif
    @if($category)
        <sitemap>
            <loc>{{ route('sitemap.show', 'category') }}</loc>
            <lastmod>{{ $category->created_at->tz('UTC')->toAtomString() }}</lastmod>
        </sitemap>
    @endif @if($tag)
        <sitemap>
            <loc>{{ route('sitemap.show', 'category') }}</loc>
            <lastmod>{{ $tag->created_at->tz('UTC')->toAtomString() }}</lastmod>
        </sitemap>
    @endif
</sitemapindex>
