<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8"/>
  <title>{{ setting('site_name', config('site.site_name')) }} | @yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  @stack('meta-tags')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="{{ setting('favicon', '/images/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
  @if(setting('fb_page_id'))
    <meta property="fb:pages" content="{{ setting('fb_page_id') }}" />
  @endif
  {!! setting('head_tags') !!}
</head>
<body>
<header id="header">
    @if(setting('enable_top_bar') == 'enable')
    <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          @if(setting('top_date') == 'enable')
          <div class="time">
            <span class="date"><i class="icon-calendar"></i>{{ \Carbon\Carbon::now()->format('l j F Y') }}</span>
          </div>
          @endif
        </div>
        <div class="col-md-6">
          <div class="header-top-right">
            @include('themes.kMag.menus.top-menu')
            <ul class="top-menu social-link">
              @if(setting('facebook'))
              <li><a target="_blank" href="{{ setting('facebook') }}"><i class="icon-facebook"></i></a></li>
              @endif
              @if(setting('twitter'))
              <li><a target="_blank" href="{{ setting('twitter') }}"><i class="icon-twitter"></i></a></li>
              @endif
              @if(setting('youtube'))
              <li><a target="_blank" href="{{ setting('youtube') }}"><i class="fa fa-youtube"></i></a></li>
              @endif
              @if(setting('instagram'))
              <li><a target="_blank" href="{{ setting('instagram') }}"><i class="icon-instagram"></i></a></li>
                @endif
              @if(setting('gplus'))
              <li><a target="_blank" href="{{ setting('gplus') }}"><i class="fa fa-google-plus"></i></a></li>
               @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
    @endif
  <!--.header-top end-->
  <div class="header-bottom">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-3">
          <a href="{{ url('/') }}" class="logo">
{{--              <img src="{{ setting('logo', url('images/logo-gold.png')) }}" alt="{{ setting('site_name') }}"/>--}}
              <img src="{{ setting('', '/images/pnp-pic.png') }}" alt="{{ setting('site_name') }}"/>
          </a>
        </div>
        <div class="col-md-9">
            <div class="pull-right">
                @stack('ad-header')
            </div>
        </div>
      </div>

    </div>
  </div>
  <!--.header-bottom end-->
  <div class="mobile-nav">
    <div class="container">
      <div class="nav-holder">
        <div class="row">
          <div class="col-6">
            <div class="nav-burger">
              <div class="nav-trigger">
                <span class="burger">
                </span>
              </div>
            </div>
          </div>
          <div class="col-6">
            <ul class="main-menu">
              <li class="search-trigger pull-right"><a href="javascript:void(0);"><i class="icon-search"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--.mobile-nav end-->
</header>
