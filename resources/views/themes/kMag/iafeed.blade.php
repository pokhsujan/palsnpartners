<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>{{ setting('site_name') }}</title>
        <link>{{ url('/') }}</link>
        <description></description>
        <language>en-us</language>
        <lastBuildDate>{{ \Carbon\Carbon::now()->format('j-M-Y G:i:s') }}</lastBuildDate>
        @foreach($posts as $post)
            <item>
                <title>{{ $post->title}}</title>
                <link>{{ $post->url }}</link>
                <guid isPermaLink="true">{{ $post->url }}</guid>
                <pubDate>{{ $post->created_at->format('j-M-Y G:i:s') }}</pubDate>
                <author>{{ $post->user->full_name }}</author>
                <description>{{ $post->excerpt }}</description>
                <content:encoded>
                    <![CDATA[
                    <!doctype html>
                    <html lang="en" prefix="op: http://media.facebook.com/op#">
                    <head>
                        <meta charset="utf-8">
                        <link rel="canonical" href="{{ $post->url }}">
                        <meta property="op:markup_version" content="v1.0">
                        <meta property="fb:use_automatic_ad_placement" content="enable=true ad_density=default">
                    </head>
                    <body>
                    <article>
                        @if(setting('ia_ad_1'))
                            <figure class="op-ad">
                                <iframe width="320" height="50" style="border:0; margin:0;"
                                        src="https://www.facebook.com/adnw_request?placement={{ setting('ia_ad_1') }}&adtype=banner320x50"></iframe>
                            </figure>
                        @endif
                        <header>
                            <!-- The cover image shown inside your article -->
                            @if($post->featured_image || $post->format == 'video')
                                <figure data-feedback="fb:likes, fb:comments">
                                    @if($post->format == 'standard' && $post->featured_image)
                                        <img src="{{ url($post->media->media_url) }}" alt="{{ $post->title }}"/>
                                        <figcaption>{{ $post->media->description }}</figcaption>
                                    @elseif($post->format == 'video')
                                        <img src="{{ url($post->video_image) }}" alt="{{ $post->title }}"/>
                                    @else
                                    @endif
                                </figure>
                            @endif
                            <h1> {{ $post->title }} </h1>
                            <time class="op-published"
                                  dateTime="{{ $post->created_at->format('j-M-Y G:i:s') }}">{{ $post->created_at->format('j-M-Y G:i:s') }}</time>
                            <time class="op-modified"
                                  dateTime="{{ $post->updated_at->format('j-M-Y G:i:s') }}">{{ $post->updated_at->format('j-M-Y G:i:s') }}</time>
                            <address>
                                {{ setting('site_name') }}
                            </address>
                            @if(!$post->categories->isEmpty())
                                @foreach($post->categories as $category )
                                    <h3 class="op-kicker">{{$category->name}}</h3>
                                @endforeach
                            @endif
                        </header>
                        {!!  $post->ia_content  !!}
                        @if(setting('ia_ad_2'))
                            <figure class="op-ad">
                                <iframe width="300" height="250" style="border:0; margin:0;"
                                        src="https://www.facebook.com/adnw_request?placement={{ setting('ia_ad_2') }}&adtype=banner300x250"></iframe>
                            </figure>
                        @endif
                        @if($post->related_posts)
                            <ul class="op-related-articles">
                                @foreach($post->related_posts as $article)
                                    <li><a href="{{ $article->url }}"></a></li>
                                @endforeach
                            </ul>
                        @endif
                        <footer>
                            <aside>
                                <b>{{ setting('site_name') }},<br>
                                    {{ setting('tagline') }}</b>
                                <i><a href="http://psjtv.com">{{ url('/') }}</a></i>
                            </aside>
                        </footer>
                    </article>
                    </body>
                    </html>
                    ]]>
                </content:encoded>
            </item>
        @endforeach
    </channel>
</rss>
