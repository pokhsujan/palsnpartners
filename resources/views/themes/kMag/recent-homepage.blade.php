@extends('themes.kMag.layout')

@section('title')
    {{ \setting('blog_title') }}
@endsection
@push('meta-tags')

@endpush
@push('styles')

@endpush
@include('themes.kMag.partials.ads')
@section('content')
    <section class="page-head-module">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <h1 class="page-title">{{ \setting('blog_title') }}</h1>
                </div>
                <div class="col-md-8">
                    @stack('archive-content-top')
                </div>
            </div>
        </div>
    </section>
    <section class="news-module sidebar-module">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    @stack('ad-header-middle')
                    <div class="module module--highlights">
                        <div class="row">
                            @php $k = 0; @endphp
                            @foreach($recent_news as $post)
                                @php $k++ @endphp
                                <div class="col-md-6">
                                    <article class="post hero-post">
                                        {{ featured_image($post, 'medium', null, true) }}
                                        <div class="post-entry">
                                            <h2 class="post-title"><a
                                                        href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">{{ $post->published_date }}</span>
                                                <a href="{{ route('user.posts', $post->user->username) }}"
                                                   class="author">{{ $post->user->profile->full_name }}</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                @if($k>=($page->getMeta('recent_posts_per_page')?$page->getMeta('recent_posts_per_page'):6))
                                    @break;
                                @endif
                            @endforeach
                        </div>
                    </div>
                    @stack('ad-header-bottom')
                </div>
                <aside class="col-md-4 sidebar">
                    @include('themes.kMag.sidebar', ['sidebar_type' => 'home_page'])
                </aside>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
