@include('themes.kMag.header')
<!--.header end-->
@include('themes.kMag.menus.main-menu')
<!--.primary-nav end-->
@stack('page-head')
<main class="main-content">
  @yield('content')
</main>
@include('themes.kMag.footer')
