@extends('themes.kMag.layout')
@include('themes.kMag.partials.ads')
@section('title')
    {{ $page->title }}
@endsection
@push('styles')
@endpush
@section('content')
    <section class="news-module page news-column sidebar-module">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-content">
                        <h1 class="page-title">{{ $page->title }}</h1>
                        @if($page->featured_image)
                            <figure class="post-media">
                                <img src="{{ url($page->media->media_url) }}" alt="{{ $page->title }}"/>
                            </figure>
                        @endif
                        <div class="post-entry">
                            {!!  $page->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
