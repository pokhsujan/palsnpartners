<div class="category-module">
    <div class="post-list">
        @foreach($posts as $post)
            <article class="post">
                {{ featured_image($post, 'medium', null, true) }}
                <div class="post-entry">
                    <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                    {{  post_meta($post) }}
                    <p>{{ $post->excerpt }}</p>
                </div>
            </article>
        @endforeach
    </div>
</div>