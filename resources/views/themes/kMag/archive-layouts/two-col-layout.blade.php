<div class="module module--highlights">
    <div class="row">
        @foreach($posts as $post)
            <div class="col-md-6">
                <article class="post hero-post">
                    {{ featured_image($post, 'medium', null, true) }}
                    <div class="post-entry">
                        <h2 class="post-title"><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a></h2>
                        {{  post_meta($post) }}
                    </div>
                </article>
            </div>
        @endforeach
    </div>
</div>