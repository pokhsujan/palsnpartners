<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'sign_in' => 'Log In',
    'register' => 'Register',
    'username_or_email' => 'Username / E-Mail Address',
    'password' => 'Password',
    'remember_me' => 'Remember Me',
    'reset_from_here' => 'Reset from here',
    'sign_in_with' => 'Log In with',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'google' => 'Google',
    'or' => 'OR',
    'already_registered' => 'Already Registered?',
    'reset_password' => 'Reset Password',
    'send_password_reset_link' => 'Send Password Reset Link',
    'verify_email' => 'Verify Your Email Address',
    'fresh_verification_sent' => 'A fresh verification link has been sent to your email address.',
    'check_email_inbox' => 'Before proceeding, please check your email for a verification link.',
    'did_not_receive_email' => 'If you did not receive the email',
    'make_another_request' => 'click here to request another ',

    'dont_have_account' => 'Don\'t have an account?',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
