<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Installer'], function () {
    Route::group(['prefix' => 'install', 'as' => 'Installer::', 'middleware' => ['install']], function () {
        Route::get('/', ['as' => 'welcome', 'uses' => 'HomeController@index']);
        Route::get('environment/classic', ['as' => 'environmentClassic', 'uses' => 'HomeController@index']);
        Route::get('environment/classic', ['as' => 'environmentClassic', 'uses' => 'HomeController@index']);
        //Route::post('environment/saveClassic', ['as' => 'environmentSaveClassic', 'uses' => 'InstallerController@saveClassic']);
        Route::post('environment/saveClassic', ['as' => 'environmentClassic', 'uses' => 'HomeController@index']);
//        Route::get('requirements', ['as' => 'requirements', 'uses' => 'InstallerController@requirements']);
//        Route::get('permissions', ['as' => 'permissions', 'uses' => 'InstallerController@permissions']);
//        Route::get('database', ['as' => 'database', 'uses' => 'InstallerController@database']);
//        Route::get('final', ['as' => 'final', 'uses' => 'InstallerController@finish']);
    });
    Route::group(['prefix' => 'update', 'as' => 'Updater::', 'middleware' => ['update', 'role:admin|developer']], function () {
        Route::get('/', ['as' => 'welcome', 'uses' => 'UpdateController@welcome']);
        Route::get('overview', ['as' => 'overview', 'uses' => 'UpdateController@overview']);
        Route::get('database', ['as' => 'database', 'uses' => 'UpdateController@database']);
    });
});

Auth::routes(['verify' => true]);
// LARAVEL SOCIALITE AUTHENTICATION ROUTES
Route::get('social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialAuthController@redirectToProvider']);
Route::get('social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialAuthController@handleProviderCallback']);


Route::get('thumbnail/{size}/{filename}', ['as' => 'images.show', 'uses' => 'Frontend\MediasController@imageSizes']);
Route::get('/video-thumbnail/{size}', ['as'=>'images.video', 'uses' => 'Frontend\MediasController@videoImageSizes']);


Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'verified', 'permission:access-admin']], function () {
    Route::get('dashboard', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);
    Route::get('my-profile', ['as' => 'users.profile', 'uses' => 'UsersController@myProfile']);
    Route::post('my-profile', ['as' => 'users.profile.update', 'uses' => 'UsersController@updateMyProfile']);
    Route::get('my-profile/logs', ['as' => 'users.mylogs', 'uses' => 'UsersController@myLogs']);
    Route::get('my-profile/change-password', ['as' => 'users.profile.change.password', 'uses' => 'UsersController@changePassword']);
    Route::post('my-profile/update-password', ['as' => 'users.profile.update.password', 'uses' => 'UsersController@updateMyPassword']);
});

Route::group(['prefix' => 'admin', 'namespace' => 'Backend', 'middleware' => ['auth', 'verified', 'permission:access-admin']], function () {
    Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'admin']);
    // middleware who can manage users
    Route::group(['middleware' => ['permission:manage-media']], function () {
        Route::get('media/list', ['as' => 'media.lists', 'uses' => 'MediasController@lists']);
        Route::get('media/browse/images', ['as' => 'media.browse', 'uses' => 'MediasController@browseImage']);
        Route::get('media/browse/files', ['as' => 'media.browse.files', 'uses' => 'MediasController@browseFiles']);
        Route::get('media/generate/thumb', ['as' => 'media.thumb.generate', 'uses' => 'MediasController@generateThumbnails']);
        Route::resource('media', 'MediasController');
    });
    // middleware who can manage users
    Route::group(['middleware' => ['permission:manage-users']], function () {
        //users
        Route::resource('users', 'UsersController', ['except' => ['show']]);
        Route::get('users/password/{user_id}', ['uses' => 'UsersController@changePassword', 'as' => 'change.password']);
        Route::put('users/password/{user_id}', ['uses' => 'UsersController@updatePassword', 'as' => 'update.password']);
        Route::get('users/logs/{id}', ['uses' => 'UsersController@userLog', 'as' => 'users.logs']);
        Route::get('users/online', ['uses' => 'UsersController@onlineUsers', 'as' => 'users.online']);
        Route::get('users/suspend/{id}', ['uses' => 'UsersController@suspendUser', 'as' => 'users.suspend']);
        Route::get('users/active/{id}', ['uses' => 'UsersController@activateUser', 'as' => 'users.active']);

    });
    // middleware for developer only
    Route::group(['middleware' => ['role:developer']], function () {
        //roles and permissions
        Route::resource('roles', 'RolesController', ['except' => ['show']]);
        Route::resource('permissions', 'PermissionsController', ['except' => ['show']]);
    });

    // middleware for admin and developer only
    Route::group(['middleware' => ['role:admin|developer']], function () {
        //settings
        Route::resource('adspaces', 'AdspacesController', ['except' => ['show']]);
        Route::get('settings', ['as' => 'settings.index', 'uses' => 'SettingsController@index']);
        Route::post('settings', ['as' => 'settings.store', 'uses' => 'SettingsController@store']);
        Route::resource('messages', 'MessagesController', ['only' => ['index', 'show']]);
        Route::resource('subscribers', 'SubscribersController', ['only' => ['index', 'destroy']]);
        Route::get('subscribers/export/{type}', ['as' => 'subscribers.export', 'uses' => 'SubscribersController@export']);
        Route::get('subscribers/sync', ['as' => 'subscribers.sync', 'uses' => 'SubscribersController@sync']);
    });
    // middleware who can manage posts
    Route::group(['middleware' => ['permission:manage-posts']], function () {
        //posts
        Route::resource('posts', 'PostsController', ['except' => ['show']]);
        Route::get('posts/search', ['as' => 'posts.search.admin', 'uses' => 'PostsController@search']);
        Route::get('posts/draft', ['as' => 'posts.draft', 'uses' => 'PostsController@draft']);
        Route::get('posts/pending', ['as' => 'posts.pending', 'uses' => 'PostsController@pending']);
        Route::get('posts/trash', ['as' => 'posts.trash', 'uses' => 'PostsController@trash']);
        Route::get('posts/trash/empty', ['as' => 'posts.trash.empty', 'uses' => 'PostsController@emptyTrash']);
        Route::get('posts/trash/{id}/restore', ['as' => 'posts.trash.restore', 'uses' => 'PostsController@restoreTrash']);
        Route::delete('posts/trash/{id}/delete', ['as' => 'posts.trash.delete', 'uses' => 'PostsController@deleteTrash']);
        Route::get('posts/check', ['as' => 'posts.check', 'uses' => 'PostsController@checkAllPosts']);
        //categories
        Route::resource('categories', 'CategoriesController', ['except' => ['show']]);
        //tags
        Route::resource('tags', 'TagsController', ['except' => ['show']]);
        Route::get('tags/lists', ['as' => 'tags.lists', 'uses' => 'TagsController@tagslist']);
        Route::resource('comments', 'CommentsController', ['only' => ['index', 'show', 'destroy']]);
        Route::get('comments/suspend/{id}', ['uses' => 'CommentsController@approveComment', 'as' => 'comments.approve']);
        Route::get('comments/active/{id}', ['uses' => 'CommentsController@suspendComment', 'as' => 'comments.suspend']);

    });
    // middleware who can manage menus
    Route::group(['middleware' => ['permission:manage-menus']], function () {
        //menus
        Route::get('menus', ['as' => 'menus.index', 'uses' => 'MenusController@index']);
        Route::get('menus/edit', ['as' => 'menus.edit', 'uses' => 'MenusController@edit']);
        Route::post('menus/store', ['as' => 'menus.store', 'uses' => 'MenusController@store']);
    });
    // middleware who can manage ads
    Route::group(['middleware' => ['permission:manage-ads']], function () {
        //ads controller
        Route::resource('ads', 'AdsController', ['except' => 'show']);
        Route::get('ads/upcoming', ['uses' => 'AdsController@upcoming', 'as' => 'ads.upcoming']);
        Route::get('ads/expired', ['uses' => 'AdsController@expired', 'as' => 'ads.expired']);
    });
    // middleware who can manage pages
    Route::group(['middleware' => ['permission:manage-pages']], function () {
        //pages
        Route::resource('pages', 'PagesController', ['except' => ['show']]);
        Route::post('pages/homepage', ['as' => 'pages.homepage.store', 'uses' => 'PagesController@storeHomepage']);
        Route::get('pages/section', ['as' => 'pages.section', 'uses' => 'PagesController@pageSections']);
    });

    // middleware who can manage widgets
    Route::group(['middleware' => ['permission:manage-widgets']], function () {
        //widgets
        Route::get('widgets', ['as' => 'widgets.index', 'uses' => 'WidgetsController@index']);
        Route::get('widgets/edit', ['as' => 'widgets.edit', 'uses' => 'WidgetsController@edit']);
        Route::post('widgets/store', ['as' => 'widgets.store', 'uses' => 'WidgetsController@store']);
    });
    Route::get('posts/info', ['as' => 'posts.info', 'uses' => 'PostsController@info']);
});

// Frontend Routes


Route::group(['namespace' => 'Frontend', 'middleware' => 'installed'], function () {
    Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);
    Route::get('/posts/search', ['uses' => 'PostsController@search', 'as' => 'posts.search']);
    Route::get('/posts/{slug}', ['as' => 'posts.show', 'uses' => 'PostsController@show']);

    Route::group(['prefix' => 'posts', 'middleware' => ['auth', 'verified']], function () {
        Route::post('comments', ['as' => 'comment.store', 'uses' => 'CommentsController@store']);
        Route::delete('comments/{comment}', ['as' => 'comment.destroy', 'uses' => 'CommentsController@destroy']);
        Route::put('comments/{comment}', ['as' => 'comment.update', 'uses' => 'CommentsController@update']);
        Route::post('comments/{comment}', ['as' => 'comment.reply', 'uses' => 'CommentsController@reply']);
        Route::post('ratings', ['as' => 'rating.store', 'uses' => 'PostsController@ratingStore']);
    });
    Route::group(['prefix' => 'feeds'], function () {
        Route::get('/', ['as' => 'feed.rss', 'uses' => 'PostsController@feeds']);
        Route::get('ias', ['as' => 'feed.instantarticles', 'uses' => 'PostsController@iafeeds']);
    });
    Route::get('/categories/{slug}', ['uses' => 'CategoriesController@show', 'as' => 'categories.show']);
    Route::get('/tags/{slug}', ['uses' => 'TagsController@show', 'as' => 'tags.show']);
    Route::get('/users/{user_id}', ['uses' => 'UsersController@show', 'as' => 'user.posts']);
    Route::get('page/{slug}', ['as' => 'pages.show', 'uses' => 'PagesController@show']);
    Route::post('messages', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('subscriber', ['as' => 'subscriber.create', 'uses' => 'SubscribersController@create']);
    Route::get('sitemap.xml', ['as' => 'sitemap.index', 'uses' => 'SitemapController@index']);
    Route::get('sitemap/{slug}.xml', ['as' => 'sitemap.show', 'uses' => 'SitemapController@show']);
});
